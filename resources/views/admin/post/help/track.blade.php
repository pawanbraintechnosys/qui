@extends('admin.layouts.app')
@section('breadcrumbs')
@include('includes.breadcrumb', ['breadcrumbs' => [

'Listing' => route('admin.help'),
$users[0]['tittle']
]])
@endsection
@section('content')

<section class="page-content container-fluid">
	<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<div class="container"> 
				      <div id="map-canvas" style="width:1000px;height:400px"></div>
				    </div>
				</div>
        </div>
	</div>
	</div>
</section>
    <script>
    
    window.lat = {{$users[0]['latitude']}};
    window.lng = {{$users[0]['longitude']}};

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(updatePosition);
        }
      
        return null;
    };
    console.log(getLocation());
 
    function updatePosition(position) {

      if (position) {
        window.lat = position.coords.latitude;
        window.lng = position.coords.longitude;
      }
    }
    
    setInterval(function(){updatePosition(getLocation());}, 10000);
      
    function currentLocation() {
      return {lat:window.lat, lng:window.lng};
    };

    var map;
    var mark;

    var initialize = function() {
      map  = new google.maps.Map(document.getElementById('map-canvas'), {center:{lat:lat,lng:lng},zoom:12});
      mark = new google.maps.Marker({position:{lat:lat, lng:lng}, map:map});
    };

    window.initialize = initialize;

    var redraw = function(payload) {
      lat = payload.message.Functions.lat;
      lng = payload.message.Functions.lng;
      //console.log(parseFloat(lat));
      //console.log(lng);
      map.setCenter({lat:parseFloat(lat), lng:parseFloat(lng), alt:0});
      mark.setPosition({lat:parseFloat(lat), lng:parseFloat(lng), alt:0});
    };

    var pnChannel = "{{$users[0]['id']}}_{{$users[0]['tittle']}}";

    var pubnub = new PubNub({
      publishKey:   'pub-c-942d0fcb-dd3f-4ee4-8053-d42e9ab51830',
      subscribeKey: 'sub-c-a2832180-c734-11ea-8a84-d2f276f1c283'
    });

    pubnub.subscribe({channels: [pnChannel]});
    pubnub.addListener({message:redraw});
   //console.log(currentLocation());
    setInterval(function() {
      pubnub.publish({channel:pnChannel, message:currentLocation()});
    }, 5000000);
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCfaLWLOOJzGnXan4NM8-sk6OSr53b_W4k&callback=initialize"></script>
 

@endsection