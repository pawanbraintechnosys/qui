@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		'Listing' => route('admin.subadmin_list'),
		'Change Password'
						]])
	@endsection
@section('content')
<section class="page-content container-fluid">
	<div class="card">
		<div class="card-body">
			
			<form method="post" autocomplete="off" action="{{route('admin.update_password_subadmin')}}">
					  @csrf
					  	<input type="hidden" name="id" value="{{ $user['id'] }}">
					  	
						<div class="card">
							<div class="card-body">
								<div class="row">
								<div class="col-md-4">
										<div class="form-group">
											<label>Email*</label>
											<input type="text" name="email" class="form-control" placeholder="Viewer Name" id="email" value="{{ old('email', $user['email']) }}" readonly>
											@error('email')
												<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
											@enderror
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Password*</label>
											<input type="password" name="password" class="form-control" placeholder="password" id="password" value="">
											@error('password')
												<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
											@enderror
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Confirm Password*</label>
											<input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" id="confirm_password" value="">
											@error('password_confirmation')
												<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
											@enderror
										</div>
									</div>
                               </div>
								<div class="row pull-right">
									<div class="card-footer row">
									<div class="input submit">
										<div class="submit">
											<input class="btn btn-primary updateUser" type="submit" value="Update">
										</div>
									</div>
									</div>
								</div>
							</div>
						</div>
					</form>	
				
	</div>
</section>

@endsection
