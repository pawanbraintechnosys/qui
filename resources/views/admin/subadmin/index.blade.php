@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		    					'Listing'
						]])
	@endsection
@section('content')

<section class="page-content container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<div class="card-header">
					<div class="row">
									<div class="col-lg-6">	
											<form action="{{ route('admin.subadmin_list') }}" method="GET">
												<div class="form-group">
													<div class="input-group mb-3">
															<input type="text" class="form-control" placeholder="Search by name" name="search" value="{{ Request::get('search') }}">
															<div class="input-group-append">
																<input type="submit" class="form-control" value="Search">
															</div>
													</div>
												</div>
											</form>
									</div>
								
						<div  style="margin-left: 365px;"><a href="{{url('admin/register')}}" class="btn btn-danger text-white">Add More</a>
					</div>
					</div>
					</div>
					
													<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
								   <th><strong>@sortablelink('name','Name')</strong></th>
									<th><strong>@sortablelink('email','Email')</strong></th>
									
									<th><strong>Action</strong></th>
								</tr>
							</thead>
							<tbody>
								@if(!$users->isEmpty())
								@foreach($users as $key => $value)
								<tr>
								   
									<td>{{$value['name']}}</td>
									<td>{{$value['email']}}</td>
									
									<td>
										<form class="deleteblogForm" action="{{ route('admin.delete_subadmin',$value['id']) }}" method="POST">
											@csrf
											@method('DELETE')
											<a href="{{ route('admin.edit_subadmin',$value['id']) }}">
												<i class="zmdi zmdi-edit zmdi-hc-fw font-size-16"></i>	
											</a>
											<a data-id="{{$value['id']}}" href="javascript:void(0);" class="deleteblog">
												<i class="zmdi zmdi-delete zmdi-hc-fw font-size-16"></i>  
											</a>
											 <a href="{{ route('admin.updatePassword_subadmin',$value['id']) }}">
												  <i  class="zmdi zmdi-key zmdi-hc-fw font-size-18"></i>
											 </a>
										</form>	
										
													
									</td>
								</tr>
								@endforeach
								@else
									<tr><td colspan="4" class="text-center">No record found</td></tr>
								@endif
							</tbody>
						</table>
						@if(!empty($users->links()))
								<div class="pull-right">
										<div class="dataTables_paginate paging_simple_numbers" id="bs4-table_paginate">
											<ul class="pagination">
												<li class="paginate_button page-item">{{ $users->links() }}</li>
											</ul>
										</div>
								</div>
								@endif
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$("body").on("click",".deleteblog",(function(){
		var t=this;
		Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, delete it!"}).then((function(e){if(e.value){var r=$(t).attr("data-id");
		$(t).parents("form:first").serialize();
		var urlw = '{{route("admin.delete_subadmin")}}';
		$.ajax({url:urlw,type:"delete",data:{id:r},success:function(t){
			t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
			})}}))}));
	</script>
@endsection