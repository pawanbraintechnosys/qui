@extends('layouts.admin.default')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		    				'Cms Pages' => route('admin.cms'),
		    				'Add New'
						]])
	@endsection
@section('content')
<section class="page-content container">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<form method="post" action="{{ route('admin.storeCms') }}" enctype="multipart/form-data" autocomplete="off">
					  @csrf
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Usage</label>
									<input type="text" name="page_uses" class="form-control" placeholder="Page Name" value="{{ old('page_uses') }}">
									
									@error('page_uses')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Page Title*</label>
									<input type="text" name="title" class="form-control" placeholder="Page Title" value="{{ old('title') }}">
									
									@error('title')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Meta Title*</label>
									<input type="text" name="meta_title" class="form-control" placeholder="Meta Title" value="{{ old('meta_title') }}">
									
									@error('meta_title')
									<span class="text-danger font-size-14" role="alert">{{ $message }}</span>@enderror
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Meta Description*</label>
									<textarea type="text" name="meta_description" class="form-control" placeholder="Meta Description" rows="5" cols="10">{{ old('meta_description') }}</textarea>
									
									@error('meta_description')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label>Image Required</label>
									<input type="checkbox">
								</div>
							</div>
							<div class="col-md-6" id ="image">
								<div class="form-group">
									<label>Image</label>
									<input type="file" name="image" class="form-control" placeholder="Image" value="">
									@error('image')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Page Content*</label>
									<textarea type="text" name="content" class="form-control" placeholder="Page Content" rows="10" cols="50">{{ old('content') }}</textarea>
									
									@error('content')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<!--<div class="col-md-6">
								<div class="form-group">
									<label>Featured Image*</label>
									<input type="file" name="featureImage" class="form-control">
									
									@error('featureImage')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>-->
							<div class="col-md-4">
								<div class="form-group">
									<label>Status</label>
									<select class="form-control" name="status">
										<option value="1" {{ old('status') == 1 ? 'selected' : '' }}>Enable</option>
										<option value="0" {{ old('status') == 0 ? 'selected' : '' }}>Disable</option>
									</select>
								</div>
							</div>
							
						</div>
						<div class="card-footer row">
							<div class="input submit">
								<div class="submit">
									<input class="btn btn-primary" type="submit" value="Save">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
   $( document ).ready(function() {
    $("#image").hide();
	  $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
              $("#image").show();
			}
            else if($(this).prop("checked") == false){
                 $("#image").hide();
            }
        });
    });	

 CKEDITOR.replace('content', {
        filebrowserUploadUrl: "{{route('admin.ckeditor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
@endsection