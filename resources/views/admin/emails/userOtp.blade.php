@component('mail::message')
Hi {{$name}}.<br>

<p>Your OTP is  <b>{{$message}}</b></p></br>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
