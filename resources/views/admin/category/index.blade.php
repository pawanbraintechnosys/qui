@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [

		    					'Listing'
						]])
	@endsection
@section('content')

<section class="page-content container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<div class="card-header">
					<div class="row">
									<!--<div class="col-lg-6">
											<form action="{{ route('admin.category') }}" method="GET">
												<div class="form-group">
													<div class="input-group mb-3">
															<input type="text" class="form-control" placeholder="Search by name" name="search" value="{{ Request::get('search') }}">
															<div class="input-group-append">
																<input type="submit" class="form-control" value="Search">
															</div>
													</div>
												</div>
											</form>
									</div>-->


					</div>
					</div>
					</div>


					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
								    <th><strong>Name</strong></th>
								    <th><strong>Image</strong></th>
								     <th><strong>Action</strong></th>

								</tr>
							</thead>
							<tbody>
								@if(!empty($category))
								@foreach($category as $key => $value)
								<tr>

									<td>{{$value['name']}}</td>
                                    <td><img src="{{asset('admin')}}/images/add-more-icons/img/{{$value['image']}}" ></td>
                                    <td>

										<a href="{{ route('admin.edit.category',$value['id']) }}">

											<i class="zmdi zmdi-edit zmdi-hc-fw font-size-16"></i>	

										</a>

										<a data-status='1' data-toggle='tooltip' data-placement='top' data-original-title='Delete' href="javascript:void(0)" data-id="{{$value['id']}}" class="disableStatuscategory">
												<i class="zmdi zmdi-delete zmdi-hc-fw font-size-20"></i>
											</a>

									</td>

								</tr>

								@endforeach
								@else
									<tr><td colspan="4" class="text-center">No record found</td></tr>
								@endif
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>




$("body").on("click",".disableStatuscategory",(function(){
	var t=this;

	Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, delete it!"}).then((function(e){if(e.value){var r=$(t).attr("data-id");
	$(t).parents("form:first").serialize();
	var urlw = '{{route("admin.categoryDelete")}}';
	$.ajax({url:urlw,type:"post",data:{id:r},success:function(t){
	t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
	})}}))}));
</script>


@endsection
