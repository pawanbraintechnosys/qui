@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		    					'Listing' => route('admin.warning'),
		    					 $user->title
						]])
	@endsection
@section('content')
<section class="page-content container">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
				
					<form method="post" action="{{ route('admin.feedbackUpdate', $user->id) }}" autocomplete="off" enctype="multipart/form-data">
					  @csrf
					  @method('PUT')
						<div class="row">
						
							<div class="col-md-6">
								<div class="form-group">
									<label>Rating*</label>
									<input type="text" name="rating" class="form-control" placeholder="Rating" value="{{ old('rating', $user->rating) }}">
									
									@error('rating')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Comment*</label>
									<textarea type="text" name="comment" class="form-control" placeholder="Comment" rows="5" cols="10">{{ old('comment', $user->comment) }}</textarea>
									
									@error('comment')
										<span class="text-danger font-size-14" role="alert">{{ $message }}</span>
									@enderror
								</div>
							</div>
								<!--div class="col-md-6">
								<div class="form-group">
									<label>Post Type</label>
									<select class="form-control" name="post_type">
										<option value= 0 <?php //if($post->post_type == 0) { //echo "selected"; }?>>Public</option>
										<option value=1 <?php //if($post->post_type == 1) { //echo "selected"; }?>>Private</option>
									</select>
								</div>
							</div-->
							
						</div>
							
						<div class="card-footer row">
							<div class="input submit">
								<div class="submit">
									<input class="btn btn-primary" type="submit" value="Update">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection