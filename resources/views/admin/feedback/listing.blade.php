@extends('admin.layouts.app')
@section('breadcrumbs')
@include('includes.breadcrumb', ['breadcrumbs' => [

'Listing'
]])
@endsection
@section('content')

<section class="page-content container-fluid">
	<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<div class="card-header">
				<div class="row">
				<div class="col-lg-6">	
				<form action="{{ route('admin.feedback') }}" method="GET">
					<div class="form-group">
						<div class="input-group mb-3">
								<input type="text" class="form-control" placeholder="Search by name" name="search" value="{{ Request::get('search') }}">
								<div class="input-group-append">
									<input type="submit" class="form-control" value="Search">
								</div>
						</div>
					</div>
				</form>
				</div>


				</div>
				</div>
				</div>


				<div class="table-responsive">
					<table class="table table-striped">
					<thead>
						<tr>
							<th><strong>#</strong></th>
							<!--th><strong>@sortablelink('sender_name','Sender Name')</strong></th>
							<th><strong>@sortablelink('sender_email','Sender Email')</strong></th-->
							<th><strong>@sortablelink('receiver_name','Name')</strong></th>
							<th><strong>@sortablelink('receiver_email','Email')</strong></th>
							
							<th><strong>@sortablelink('rating','Rating')</strong></th>
							<th><strong>@sortablelink('comment','Comment')</strong></th>
							
							<th><strong>@sortablelink('created_at','Date')</strong></th>
							<th><strong>Actions</strong></th>
						</tr>
					</thead>
						<tbody>
							@if(!$users->isEmpty())
							@php $i=1;@endphp
							@foreach($users as $key => $value)
							
							<tr>
								<td>{{$i}}</td>
								<!--<td>{{$value['post']}}</td>-->
								<!--td>{{$value['sender_name']}}</td>
								<td>{{$value['sender_email']}}</td-->
								<td>{{$value['receiver_name']}}</td>
								<td>{{$value['receiver_email']}}</td>
								<td>{{$value['rating']}}/5</td>
								<!--td>{{$value['needed_price']}}</td-->
						<td style="
								height: 150px; width:150px;
								">{{ substr($value['comment'],-100)}}</td>

								
								<td>@php $date=strtotime($value['created_at']); @endphp {{ date('d-M-Y',$date)}}</td>
								</td>
								<td>
									<!--a href="{{ route('admin.Postview_warning',$value['id']) }}">
											<i class="zmdi zmdi-eye zmdi-hc-fw font-size-20"></i>	
										</a-->
										<a href="{{ route('admin.feedback.view',$value['id']) }}">
											<i class="zmdi zmdi-eye zmdi-hc-fw font-size-20"></i>	
										</a>
									<a href="{{ route('admin.feedback_edit',$value['id']) }}">
												<i class="zmdi zmdi-edit zmdi-hc-fw font-size-16"></i>
											</a>
										<a data-status='1' data-toggle='tooltip' data-placement='top' data-original-title='Delete' href="javascript:void(0)" data-id="{{$value['id']}}" class="disableStatus">
												<i class="zmdi zmdi-delete zmdi-hc-fw font-size-20"></i>
											</a>
								</td>

							</tr>
						
							@php $i++; @endphp
							@endforeach

							@else
							<tr>
								<td colspan="7" class="text-center">No record found</td>
							</tr>
							@endif
						</tbody>
					</table>
				@if(!empty($users->links()))
				<div class="pull-right">
				<div class="dataTables_paginate paging_simple_numbers" id="bs4-table_paginate">
				<ul class="pagination">
					<li class="paginate_button page-item">{{ $users->links() }}</li>
				</ul>
				</div>
				</div>
				@endif
				</div>
			</div>
		</div>
	</div>
	</div>
</section>



<script>




$("body").on("click",".disableStatus",(function(){
	var t=this;

	Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, delete it!"}).then((function(e){if(e.value){var r=$(t).attr("data-id");
	$(t).parents("form:first").serialize();
	var urlw = '{{route("admin.feedbackDelete")}}';
	$.ajax({url:urlw,type:"post",data:{id:r},success:function(t){
	t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
	})}}))}));
</script>

@endsection