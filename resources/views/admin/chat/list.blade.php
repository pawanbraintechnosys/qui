@extends('admin.layouts.app')
	@section('breadcrumbs')
		@include('includes.breadcrumb', ['breadcrumbs' => [
		                    
		    					'Listing'
						]])
	@endsection
@section('content')



<section class="page-content container-fluid">

	<div class="row">

		<div class="col-lg-12">

			<div class="card">

				   <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form action="{{ route('admin.chatList',Request::segment(3)) }}" method="GET">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" placeholder="Search by name"
                                                       name="search" value="{{ Request::get('search') }}">
                                                <div class="input-group-append">
                                                    <input type="submit" class="form-control" value="Search">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                               
                            </div>
                        </div>
                   <div class="table-responsive">

						<table class="table table-striped">

							<thead>

								<tr>

									<th><strong><b>User Name</b></strong></th>
									<th><strong><b>Chat</b></strong></th>
									<th><strong><b>Date</b></strong></th>
									<th><strong><b>Time</b></strong></th>
									<th><strong><b>Action</b></strong></th>

								</tr>

							</thead>

							<tbody>
                             @if(!$dataReverse->isEmpty())
								

								@foreach($dataReverse as $key => $value)
                               
								<tr>

									<td>{{$value['user']['name']}}</td>
									<td>{{$value['text']}}</td>
									<?php $dotTime= explode(".",$value['createdAt']);
									$Time= explode("T",$dotTime[0]);  
									//print_r($Time); die;
										?>
									<td>@php $date=strtotime($Time[0]); @endphp {{ date('d-M-Y',$date)}}
									</td>
									<td>{{$Time[1]}}</td>
									<td>
										<a data-status="{{$value['channel']}}" data-toggle='tooltip' data-placement='top' data-original-title='Delete' href="javascript:void(0)" data-id="{{$value['timetoken']}}" data-startTimetoken="{{$value['start_timeToken']}}" class="deleteChat">
										<i class="zmdi zmdi-delete zmdi-hc-fw font-size-20"></i>
										</a>

										

									</td>
									
                                     

								</tr>

								@endforeach

								@else

									<tr><td colspan="5" class="text-center">No record found</td></tr>

								@endif

							</tbody>

						</table>
                     @if(!empty($dataReverse->links()))

								<div class="pull-right">

										<div class="dataTables_paginate paging_simple_numbers" id="bs4-table_paginate">

											<ul class="pagination">

												<li class="paginate_button page-item">{{ $dataReverse->links() }}</li>

											</ul>

										</div>

								</div>

								@endif
						
					</div>

				

			</div>

		</div>

	</div>

</section>
<script>
$("body").on("click",".deleteChat",(function(){
	var t=this;
	var channel=$(t).attr("data-status");
	var startTimetoken=$(t).attr("data-startTimetoken");
	
	Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, delete it!"}).then((function(e){if(e.value){var r=$(t).attr("data-id");
	$(t).parents("form:first").serialize();
	var urlw = '{{route("admin.chat_delete")}}';
	$.ajax({url:urlw,type:"post",data:{end_timetoken:r,channel:channel,start_timeToken:startTimetoken},success:function(t){
	t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
	})}}))}));
</script>



@endsection