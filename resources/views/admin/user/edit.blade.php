@extends('admin.layouts.app')
@section('breadcrumbs')
    @include('includes.breadcrumb', ['breadcrumbs' => [
    'Listing' => route('admin.category'),
    'Add New'
    ]])
@endsection
@section('content')
    <section class="page-content container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{ route('admin.user.update',$record->id) }}" enctype="multipart/form-data"
                              autocomplete="off">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Name</label>
                                        <input type="text" class="form-control" name="name" placeholder="Enter name" value='{{ old('name', $record->name) }}'>
                                        <input type="hidden"  name="id"  value='{{ old('id', $record->id) }}'>
                                        @if ($errors->has('name'))
                                            <span class="error-message">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">User Name</label>
                                        <input type="text" class="form-control" name="username" placeholder="Enter username" value='{{ old('username', $record->username) }}'>
                                        @if ($errors->has('username'))
                                            <span class="error-message">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="text" class="form-control" name="email" placeholder="Enter email address" value='{{ old('email', $record->email) }}'>
                                        @if ($errors->has('email'))
                                            <span class="error-message">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" class="form-control"  id="sublocality_level_2" name="location" placeholder="Type the location" value='{{ old('location', $record->location) }}'>
                                        <input type="hidden" class="form-control" id="latitude" name="latitude"  value="{{ old('latitude', $record->latitude) }}">
                                        <input type="hidden" class="form-control" id="longitude" name="longitude"  value="{{ old('longitude', $record->longitude) }}">
                                        @if ($errors->has('location'))
                                            <span class="error-message">
                                                <strong>{{ $errors->first('location') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Password</label>
                                        <input type="password" class="form-control" name="password" placeholder="Enter Password" value='{{ old('password', $record->password) }}'>
                                        @if ($errors->has('password'))
                                            <span class="error-message">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Confirm Password</label>
                                        <input type="password" class="form-control" name="password_confirmation" placeholder="Please enter confirm password" value='{{ old('password_confirmation', $record->password_confirmation) }}'>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="error-message">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                    <div id="map" style="width:100%;height:400px;">
                                    </div>
                                    <div id="infowindow-content">
                                        <img src="" width="16" height="16" id="place-icon">
                                        <span id="place-name"  class="title"></span><br>
                                        <span id="place-address"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer row">
                                <div class="input submit">
                                    <div class="submit">
                                        <input class="btn btn-primary" type="submit" value="Save">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        var placeSearch, autocomplete;
        var componentForm = {

            //sublocality_level_2: 'long_name',
            //sublocality_level_1: 'long_name',
            //locality : 'long_name',
            //administrative_area_level_2: 'short_name',
            //administrative_area_level_1: 'long_name',
            //country: 'long_name',
            //postal_code: 'short_name',
        };
        var map;

        function initAutocomplete() {
            var geocoder = new google.maps.Geocoder();
                @if($record->latitude && $record->longitude)
            var myLatlng = new google.maps.LatLng({{$record->latitude}},{{$record->longitude}});
                @else
            var myLatlng = new google.maps.LatLng(20.5937,78.9629);
                @endif
            var marker;
            var map = new google.maps.Map(document.getElementById('map'), {
                center: myLatlng,
                zoom: 4,
                mapTypeId: 'roadmap'
            });


            var marker = new google.maps.Marker({
                map: map,
                position: myLatlng,
                anchorPoint: new google.maps.Point(25.3548, 51.1839),
                draggable: true
            });
            map.setCenter(myLatlng);
            map.setZoom(8);  // Why 17? Because it looks good.

            //infowindow.setContent(iwContent);
            // opening the infowindow in the current map and at the current marker location
            //infowindow.open(map, marker);

            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('sublocality_level_2')),
                {types: []});


            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', function(){
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                //console.log(place);
                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(15);  // Why 15? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                // alert(place);
                document.getElementById('sublocality_level_2').value = place.formatted_address;
                document.getElementById('latitude').value = place.geometry.location.lat();
                document.getElementById('longitude').value = place.geometry.location.lng();
                //console.log(componentForm);
                for (var component in componentForm) {
                    document.getElementById(component).value = '';
                    document.getElementById(component).disabled = false;
                }

                // Get each component of the address from the place details
                // and fill the corresponding field on the form.
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        document.getElementById(addressType).value = val;
                    }
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }
                infowindowContent.children['place-icon'].src = place.icon;
                infowindowContent.children['place-name'].textContent = place.name;
                infowindowContent.children['place-address'].textContent = address;
                infowindow.open(map, marker);
            });

            google.maps.event.addListener(marker, 'dragend', function(event) {
                document.getElementById('latitude').value = event.latLng.lat();
                document.getElementById('longitude').value = event.latLng.lng();
                //console.log(marker.getPosition());
                geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                    document.getElementById('sublocality_level_2').value = results[0].formatted_address;
                    //console.log(results[0]);
                    if (status == google.maps.GeocoderStatus.OK) {
                        for (var component in componentForm) {
                            //document.getElementById(component).value = '';
                            //document.getElementById(component).disabled = false;
                        }

                        for (var i = 0; i < results[0].address_components.length; i++) {

                            for (var j = 0; j < results[0].address_components[i].types.length; j++) {
                                var addressType = results[0].address_components[i].types[j];
                                if (componentForm[addressType]) {
                                    var val = results[0].address_components[i][componentForm[addressType]];
                                    document.getElementById(addressType).value = val;
                                }
                            }
                        }
                    }
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                });
            });
        }



        function setMarkers(map){

        }

        function toggleBounce() {
            if (marker.getAnimation() !== null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }

        function fillInAddress(map) {

        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env("GOOGLE_KEY")}}&libraries=places&callback=initAutocomplete" async defer></script>
@endsection
