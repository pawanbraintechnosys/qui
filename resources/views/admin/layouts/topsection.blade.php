<!-- START LOGO WRAPPER -->
<nav class="top-toolbar navbar navbar-mobile navbar-tablet">
	<ul class="navbar-nav nav-left">
		<li class="nav-item">
			<a href="javascript:void(0)" data-toggle-state="aside-left-open">
				<i class="icon dripicons-align-left"></i>
			</a>
		</li>
	</ul>
	<ul class="navbar-nav nav-center site-logo">
		<li>
			<a href="{{ url('/admin') }}">
				<div class="logo_mobile">
					<span>{{ config('app.name', 'Laravel') }}</span>
				</div>
			</a>
		</li>
	</ul>
	<ul class="navbar-nav nav-right">
		<li class="nav-item">
			<a href="javascript:void(0)" data-toggle-state="mobile-topbar-toggle">
				<i class="icon dripicons-dots-3 rotate-90"></i>
			</a>
		</li>
	</ul>
</nav>
<!-- END LOGO WRAPPER -->

<!-- START TOP TOOLBAR WRAPPER -->
<nav class="top-toolbar navbar navbar-desktop flex-nowrap">

	
	
	<!-- START LEFT DROPDOWN MENUS -->
	<header class="page-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h1 class="separator">@if(!empty($pageTitle)) {{$pageTitle}} @endif</h1>
					@yield('breadcrumbs')			
			</div>
		</div>

	</header>
	
	<!-- END LEFT DROPDOWN MENUS -->
	
	
	<!-- START RIGHT TOOLBAR ICON MENUS -->
	<ul class="navbar-nav nav-right">
		<li class="nav-item">
			<!-- Default switch -->
			 
			<!--div class="custom-control custom-switch">
			  <input type="checkbox" class="custom-control-input" id="notification" data-id="1" {{ App\Model\Admin::where(['id' =>1])->pluck('status')->first() == 0 ? 'checked' : '' }}>
			  <label class="custom-control-label notification" for="notification" style="
			    margin-top: 24px;
			">Help Notification</label>
			</div-->
		</li>
		<li class="nav-item dropdown">
			<a data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
				<i class="icon dripicons-user"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-right dropdown-menu-accout">
				<div class="dropdown-header pb-3">
					<a class="dropdown-item" href="{{ route('admin.auth.changepassword') }}"><i class="zmdi zmdi-key"></i> Change Password</a>
					<a class="dropdown-item" href="{{ route('admin.auth.logout') }}"  onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">
					<i class="icon dripicons-lock-open"></i> {{ __('Logout') }}</a>
					<form id="logout-form" action="{{ route('admin.auth.logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
				</div>
			</div>	
		</li>
	</ul>
	<!-- END RIGHT TOOLBAR ICON MENUS -->
	
</nav>
<!-- END TOP TOOLBAR WRAPPER -->
<script>
	$("body").on("click",".notification",(function(){
		
		var x = document.getElementById("notification").checked;
		if(x == true){
           var t=this;
		Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, turn on notification"}).then((function(e){if(e.value){var r=1;
		$(t).parents("form:first").serialize();
		var urlw = '{{route("admin.notificationStatus")}}';
		$.ajax({url:urlw,type:"post",data:{id:r},success:function(t){
			t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
			})}}));
		   }else{
		   	
		  var t=this;
		Swal.fire({title:"Are you sure?",text:"You won't be able to revert this!",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes, turn on notification"}).then((function(e){if(e.value){var r=0;
		$(t).parents("form:first").serialize();
		var urlw = '{{route("admin.notificationStatus")}}';
		$.ajax({url:urlw,type:"post",data:{id:r},success:function(t){
			t.status?(toastr.success(t.msg,"Success"),setTimeout((function(){location.reload()}),3e3)):toastr.error(t.msg,"Success")}
			})}}));
		   }
		}));
	</script>