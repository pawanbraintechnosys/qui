<?php $seg=Request::segment(2);

     if($seg == 'News'){ 
      $newestNews= 'admin.newest';
      $newsOffered= '';
      $newsNeeded= '';
      $newsHelp= '';
      $newsWarning= '';
      $popularHelp= '';
      $popularWarning= '';
      $popularNews= 'admin.popular';
      $popularOffered= '';
      $popularNeeded= '';

     }elseif($seg == 'Offered'){
      $newsOffered= 'admin.newest';
      $newsNeeded= '';
      $newestNews= '';
      $newsHelp= '';
      $newsWarning= '';
      $popularHelp= '';
      $popularWarning= '';
       $popularNews= '';
      $popularOffered= 'admin.popular';
      $popularNeeded= '';
     }elseif($seg == 'Needed'){
      $newsNeeded= 'admin.newest';
      $newestNews= '';
      $newsOffered= '';
      $newsHelp= '';
      $newsWarning= '';
      $popularHelp= '';
      $popularWarning= '';
      $popularNews= '';
      $popularOffered= '';
      $popularNeeded= 'admin.popular';
     }elseif($seg == 'Help'){
      $newsNeeded= '';
      $newestNews= '';
      $newsOffered= '';
      $newsHelp= 'admin.newest';
      $newsWarning= '';
      $popularHelp= 'admin.newest';
      $popularWarning= '';
      $popularNews= '';
      $popularOffered= '';
      $popularNeeded= 'admin.popular';
     }elseif($seg == 'warning'){
      $newsNeeded= '';
      $newestNews= '';
      $newsOffered= '';
      $newsHelp= '';
      $newsWarning= 'admin.newest';
      $popularHelp= '';
      $popularWarning= 'admin.popular';
      $popularNews= '';
      $popularOffered= '';
      $popularNeeded= '';
     }else{
     	$newsNeeded= '';
      $newestNews= '';
      $newsOffered= '';
      $newsHelp= '';
      $newsWarning= '';
      $popularHelp= '';
      $popularWarning= '';
      $popularNews= '';
      $popularOffered= '';
      $popularNeeded= '';
     }
?>

<?php  $allowed_routes=[];  
	$task= explode(',',Auth::user()['task']);
	if(!empty($task)){
		foreach($task as $route){
	        $allowed_routes = array_merge($allowed_routes,@explode('|',$route));
	}
	//print_r($allowed_routes); die;
	}			
			
	
	?>
<aside class="sidebar sidebar-left">
	<div class="sidebar-content">
		<div class="aside-toolbar" style="border-bottom: 1px solid #4f13c5;">
			
			<a target="_blank" href="{{ url('/') }}">
					<img src="{{asset('admin')}}/img/logo/qui.jpg"  class="logo_size"/>
					
				</a>
		</div>

	
		<nav class="main-menu">
			<ul class="nav metismenu">
				<li class="nav-dropdown {{ request()->routeIs('admin.dashboard') ? 'active' : '' }}">
					<a href="{{route('admin.dashboard')}}" aria-expanded="false"><i class="icon dripicons-meter"></i><span>Dashboard</span></a>
				</li>
				@if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.user", $allowed_routes))
					   <li class="nav-dropdown {{ request()->routeIs('admin.user','admin.disabled','admin.user.create') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-user"></i><span>Manage Users</span></a>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.user','admin.user.create') ? 'active' : 'admin.user' }}"><a href="{{route('admin.user')}}"><span>Users</span></a></li>
							
					</ul>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.disabled') ? 'active' : 'admin.disabled' }}"><a href="{{route('admin.disabled')}}"><span>Disabled</span></a></li>
							
					</ul>
				</li>
					   @else
					    @endif		
					   @else
			<li class="nav-dropdown {{ request()->routeIs('admin.user','admin.disabled','admin.user.create') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-user"></i><span>Manage Users</span></a>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.user','admin.user.create') ? 'active' : 'admin.user' }}"><a href="{{route('admin.user')}}"><span>Users</span></a></li>
							
					</ul>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.disabled') ? 'active' : 'admin.disabled' }}"><a href="{{route('admin.disabled')}}"><span>Disabled</span></a></li>
							
					</ul>
				</li>
				 @endif
				 @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.category", $allowed_routes))
					   <li class="nav-dropdown {{ request()->routeIs('admin.category','admin.edit.category') ? 'active' : '' }}">
					<a href="{{route('admin.category')}}" aria-expanded="false"><i class="la la-gear"></i><span>Category</span></a>
				</li>
					   @else
					   @endif
					   @else
					  
				<li class="nav-dropdown {{ request()->routeIs('admin.category','admin.edit.category') ? 'active' : '' }}">
					<a href="{{route('admin.category')}}" aria-expanded="false"><i class="la la-gear"></i><span>Category</span></a>
				</li>
				 @endif
				  @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.news", $allowed_routes))
					   <li class="nav-dropdown {{ request()->routeIs('admin.post_userDetails_needed','admin.post_userDetails_offered','admin.post_userDetails_news','admin.needed','admin.news','admin.offered','admin.editPost_news','admin.Postview_news','admin.editPost_offered','admin.Postview_offered','admin.Postview_needed','admin.editPost_needed','admin.warning','admin.help','admin.Postview_warning','admin.Postview_help','admin.editPost_help','admin.editPost_warning','admin.track','admin.newest','admin.popular','admin.recommend_more','admin.recommend') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="icon dripicons-view-thumb"></i><span>Manage Post</span></a>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.post_userDetails_news','admin.news','admin.editPost_news','admin.Postview_news',$newestNews,$popularNews) ? 'active' : 'admin.news' }}"><a href="{{route('admin.news')}}"><span>News</span></a></li>
							
					</ul>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.post_userDetails_offered','admin.offered','admin.editPost_offered','admin.Postview_offered') ? 'active' : 'admin.offered',$newsOffered,$popularOffered }}"><a href="{{route('admin.offered')}}"><span>Offered</span></a></li>
							
					</ul>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.post_userDetails_needed','admin.needed','admin.Postview_needed','admin.editPost_needed',$popularNeeded,$newsNeeded) ? 'active' : 'admin.needed' }}"><a href="{{route('admin.needed')}}"><span>Needed</span></a></li>
							
					</ul>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.post_userDetails_help','admin.help','admin.Postview_help','admin.editPost_help',$newsHelp,$popularHelp,'admin.track') ? 'active' : 'admin.help' }}"><a href="{{route('admin.help')}}"><span>Help</span></a></li>
							
					</ul>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.post_userDetails_warning','admin.warning','admin.Postview_warning',$newsWarning,$popularWarning,'admin.editPost_warning') ? 'active' : 'admin.warning' }}"><a href="{{route('admin.warning')}}"><span>Warning</span></a></li>
							
					</ul>
				</li>
					   @else
					   @endif
                      @else
				<li class="nav-dropdown {{ request()->routeIs('admin.post_userDetails_needed','admin.post_userDetails_offered','admin.post_userDetails_news','admin.needed','admin.news','admin.offered','admin.editPost_news','admin.Postview_news','admin.editPost_offered','admin.Postview_offered','admin.Postview_needed','admin.editPost_needed','admin.warning','admin.help','admin.Postview_warning','admin.Postview_help','admin.editPost_help','admin.editPost_warning','admin.track','admin.newest','admin.popular','admin.recommend_more','admin.recommend') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="icon dripicons-view-thumb"></i><span>Manage Post</span></a>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.post_userDetails_news','admin.news','admin.editPost_news','admin.Postview_news',$newestNews,$popularNews) ? 'active' : 'admin.news' }}"><a href="{{route('admin.news')}}"><span>News</span></a></li>
							
					</ul>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.post_userDetails_offered','admin.offered','admin.editPost_offered','admin.Postview_offered') ? 'active' : 'admin.offered',$newsOffered,$popularOffered }}"><a href="{{route('admin.offered')}}"><span>Offered</span></a></li>
							
					</ul>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.post_userDetails_needed','admin.needed','admin.Postview_needed','admin.editPost_needed',$popularNeeded,$newsNeeded) ? 'active' : 'admin.needed' }}"><a href="{{route('admin.needed')}}"><span>Needed</span></a></li>
							
					</ul>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.post_userDetails_help','admin.help','admin.Postview_help','admin.editPost_help',$newsHelp,$popularHelp,'admin.track') ? 'active' : 'admin.help' }}"><a href="{{route('admin.help')}}"><span>Help</span></a></li>
							
					</ul>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.post_userDetails_warning','admin.warning','admin.Postview_warning',$newsWarning,$popularWarning,'admin.editPost_warning') ? 'active' : 'admin.warning' }}"><a href="{{route('admin.warning')}}"><span>Warning</span></a></li>
							
					</ul>
				</li>
				@endif
				  @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.cms", $allowed_routes))
					   <li class="nav-dropdown {{ request()->routeIs('admin.cms','admin.editCms') ? 'active' : '' }}">
					<a href="{{route('admin.cms')}}" aria-expanded="false"><i class="la la-gear"></i><span>CMS</span></a>
				</li>
					   @else
					   @endif
					   @else
				<li class="nav-dropdown {{ request()->routeIs('admin.cms','admin.editCms') ? 'active' : '' }}">
					<a href="{{route('admin.cms')}}" aria-expanded="false"><i class="la la-gear"></i><span>CMS</span></a>
				</li>
				@endif
				 @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.feedback", $allowed_routes))
					   <li class="nav-dropdown {{ request()->routeIs('admin.feedback','admin.feedback_edit') ? 'active' : '' }}">
					<a href="{{route('admin.feedback')}}" aria-expanded="false"><i class="la la-gear"></i><span>Feedback</span></a>
				</li>
				@else
				@endif
				@else
				<li class="nav-dropdown {{ request()->routeIs('admin.feedback','admin.feedback_edit') ? 'active' : '' }}">
					<a href="{{route('admin.feedback')}}" aria-expanded="false"><i class="la la-gear"></i><span>Feedback</span></a>
				</li>
				@endif
				 @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.chat", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.chat','admin.chat_edit') ? 'active' : '' }}">
					<a href="{{route('admin.chat')}}" aria-expanded="false"><i class="la la-gear"></i><span>Chat</span></a>
				</li>
				@else
				@endif
				@else
				<li class="nav-dropdown {{ request()->routeIs('admin.chat','admin.chat_edit') ? 'active' : '' }}">
					<a href="{{route('admin.chat')}}" aria-expanded="false"><i class="la la-gear"></i><span>Chat</span></a>
				</li>
				@endif
				 @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.chat", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.pushnotification','admin.pushnotificationAdd') ? 'active' : '' }}">
					<a href="{{route('admin.pushnotification')}}" aria-expanded="false"><i class="la la-gear"></i><span>Notification</span></a>
				</li>
				@else
				@endif
				@else
				<li class="nav-dropdown {{ request()->routeIs('admin.pushnotification','admin.pushnotificationAdd') ? 'active' : '' }}">
					<a href="{{route('admin.pushnotification')}}" aria-expanded="false"><i class="la la-gear"></i><span>Notification</span></a>
				</li>
				@endif
                  @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.report", $allowed_routes))
					    <li class="nav-dropdown {{ request()->routeIs('admin.report','admin.spam','admin.report') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-user"></i><span>Report & Issues</span></a>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.user','admin.spam') ? 'active' : 'admin.spam' }}"><a href="{{route('admin.spam')}}"><span>Spam</span></a></li>
							
					</ul>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.report') ? 'active' : 'admin.report' }}"><a href="{{route('admin.report')}}"><span>Inappropriate</span></a></li>
							
					</ul>
				</li>
					   @else
					   @endif
					   @else
				  <li class="nav-dropdown {{ request()->routeIs('admin.report','admin.spam','admin.report') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-user"></i><span>Report & Issues</span></a>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.user','admin.spam') ? 'active' : 'admin.spam' }}"><a href="{{route('admin.spam')}}"><span>Spam</span></a></li>
							
					</ul>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.report') ? 'active' : 'admin.report' }}"><a href="{{route('admin.report')}}"><span>Inappropriate</span></a></li>
							
					</ul>
				</li>
				
				@endif

				 @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.chat", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.pushnotification','admin.pushnotificationAdd') ? 'active' : '' }}">
					<a href="{{route('admin.pushnotification')}}" aria-expanded="false"><i class="la la-gear"></i><span>Sub Admin</span></a>
				</li>
				@else
				@endif
				@else
				<li class="nav-dropdown {{ request()->routeIs('admin.subadmin_list','admin.edit_subadmin','admin.register') ? 'active' : '' }}">
					<a href="{{route('admin.subadmin_list')}}" aria-expanded="false"><i class="la la-gear"></i><span>Sub Admin</span></a>
				</li>
				@endif

				 @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.chat", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.pushnotification','admin.pushnotificationAdd') ? 'active' : '' }}">
					<a href="{{route('admin.pushnotification')}}" aria-expanded="false"><i class="la la-gear"></i><span>Sub Admin</span></a>
				</li>
				@else
				@endif
				@else
				<li class="nav-dropdown {{ request()->routeIs('admin.support','admin.support_chat') ? 'active' : '' }}">
					<a href="{{route('admin.support')}}" aria-expanded="false"><i class="la la-gear"></i><span>Support</span></a>
				</li>
				@endif
				
				
				
				
		
		</nav>
	</div>
</aside>