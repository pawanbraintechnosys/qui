@extends('layouts.app')

@section('content')

<header>
	<div class="qui-head">
		<div class="container sml-container">
			<div class="row">
				<div class="col-lg-12">
					<div class="head-box text-center">
						<a class="navbar-brand" href="#"><img src="{{ asset('image/logo.png') }}" alt="logo"></a>
						<h2>{!!$cms[0]['title']!!}</h2>
						<!--<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- header end -->

<!-- page content -->

<section class="p-0">
	<div class="page-content">
		{!!$cms[0]['content']!!}
	</div>
</section>
<!-- page content end -->

<!-- Optional JavaScript -->


@endsection