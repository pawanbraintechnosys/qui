<aside class="sidebar sidebar-left">
	<div class="sidebar-content">
		<div class="aside-toolbar">
			<ul class="site-logo">
				<a target="_blank" href="{{ url('/') }}">
					<img src="{{asset('admin')}}/img/logo/outer.png" class="logo_size"/>
					
				</a>
				<p style="    text-align: center;
    margin-top: -8px; color: #fff;">A Bridge Not Too Far.</p>
			</ul>
		</div>
		<?php  $allowed_routes=[];  
	$task= explode(',',Auth::user()['task']);
	if(!empty($task)){
		foreach($task as $route){
	        $allowed_routes = array_merge($allowed_routes,@explode('|',$route));
	}
	//print_r($allowed_routes); die;
	}			
			
	
	?>
	
		<nav class="main-menu">
			<ul class="nav metismenu">
				<li class="nav-dropdown {{ request()->routeIs('admin.dashboard') ? 'active' : '' }}">
					<a href="{{route('admin.dashboard')}}" aria-expanded="false"><i class="icon dripicons-meter"></i><span>Dashboard</span></a>
				</li>
				@if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.sliders", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.galleryadd','admin.changePasswordGet','admin.bannerImage','admin.sliders','admin.addSliders','', 'admin.editSlider', 'admin.testimonials', 'admin.addTestimonial', 'admin.editTestimonial', 'admin.testimonials.category', 'admin.addTestimonial.category', 'admin.editTestimonial.category', 'admin.partners', 'admin.addPartner', 'admin.editPartner', 'admin.gallery', 'admin.addgallery', 'admin.editgallery','admin.team', 'admin.addteam', 'admin.editteam' ,'admin.faq', 'admin.addfaq', 'admin.editfaq','admin.setting', 'admin.addsetting', 'admin.editsetting','admin.galleryCategory','admin.addgalleryCategory','admin.GalleryDestroyCategory','admin.editgalleryCategory','admin.galleryUpdateCategory','admin.edit_AboutMission','admin.galleryAlbum') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="icon dripicons-view-thumb"></i><span>Website</span></a>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.sliders','admin.addSliders','', 'admin.editSlider') ? 'active' : '' }}"><a href="{{ route('admin.sliders')}}"><span>Slider</span></a></li>
						
						<li class="{{ request()->routeIs('admin.testimonials', 'admin.addTestimonial', 'admin.editTestimonial', 'admin.testimonials.category', 'admin.addTestimonial.category', 'admin.editTestimonial.category') ? 'active' : '' }}"><a href="{{ route('admin.testimonials')}}"><span>Testimonials</span></a></li>
						
						<li class="{{ request()->routeIs('admin.partners', 'admin.addPartner', 'admin.editPartner') ? 'active' : '' }}"><a href="{{ route('admin.partners')}}"><span>Partners</span></a></li>
						
						<li class="{{ request()->routeIs('admin.galleryadd','admin.gallery','admin.galleryAlbum', 'admin.addgallery', 'admin.editgallery','admin.galleryCategory','admin.addgalleryCategory','admin.GalleryDestroyCategory','admin.editgalleryCategory','admin.galleryUpdateCategory') ? 'active' : '' }}"><a href="{{ route('admin.galleryCategory')}}"><span>Manage Gallery</span></a></li>
						<!--<li class="{{ request()->routeIs('admin.blog', 'admin.addblog', 'admin.editblog') ? 'active' : '' }}"><a href="{{ route('admin.blog')}}"><span>Blog</span></a></li>-->
						<li class="{{ request()->routeIs('admin.team', 'admin.addteam', 'admin.editteam') ? 'active' : '' }}"><a href="{{ route('admin.team')}}"><span>Our Team</span></a></li>
						<li class="{{ request()->routeIs('admin.faq', 'admin.addfaq', 'admin.editfaq') ? 'active' : '' }}"><a href="{{ route('admin.faq')}}"><span>FAQ</span></a></li>
						<li class="{{ request()->routeIs('admin.setting', 'admin.addsetting', 'admin.editsetting') ? 'active' : '' }}"><a href="{{ route('admin.editsetting',1) }}"><span>Setting</span></a></li>
						<li class="{{ request()->routeIs('admin.edit_AboutMission') ? 'active' : '' }}"><a href="{{ route('admin.edit_AboutMission',1) }}"><span> About Mission & Vision </span></a></li>
					<!--	<li class="{{ request()->routeIs('admin.bannerImage') ? 'active' : '' }}"><a href="{{ route('admin.bannerImage') }}"><span>Banner Image</span></a></li>-->
					
						
					</ul>
				</li>
				 @else
                       @endif						   
					  
					   @else
						   <li class="nav-dropdown {{ request()->routeIs('admin.galleryadd','admin.changePasswordGet','admin.bannerImage','admin.sliders','admin.addSliders','', 'admin.editSlider', 'admin.testimonials', 'admin.addTestimonial', 'admin.editTestimonial', 'admin.testimonials.category', 'admin.addTestimonial.category', 'admin.editTestimonial.category', 'admin.partners', 'admin.addPartner', 'admin.editPartner', 'admin.gallery', 'admin.addgallery', 'admin.editgallery','admin.team', 'admin.addteam', 'admin.editteam' ,'admin.faq', 'admin.addfaq', 'admin.editfaq','admin.setting', 'admin.addsetting', 'admin.editsetting','admin.galleryCategory','admin.addgalleryCategory','admin.GalleryDestroyCategory','admin.editgalleryCategory','admin.galleryUpdateCategory','admin.edit_AboutMission','admin.galleryAlbum') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="icon dripicons-view-thumb"></i><span>Website</span></a>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.sliders','admin.addSliders','', 'admin.editSlider') ? 'active' : '' }}"><a href="{{ route('admin.sliders')}}"><span>Slider</span></a></li>
						
						<li class="{{ request()->routeIs('admin.testimonials', 'admin.addTestimonial', 'admin.editTestimonial', 'admin.testimonials.category', 'admin.addTestimonial.category', 'admin.editTestimonial.category') ? 'active' : '' }}"><a href="{{ route('admin.testimonials')}}"><span>Testimonials</span></a></li>
						
						<li class="{{ request()->routeIs('admin.partners', 'admin.addPartner', 'admin.editPartner') ? 'active' : '' }}"><a href="{{ route('admin.partners')}}"><span>Partners</span></a></li>
						
						<li class="{{ request()->routeIs('admin.galleryadd','admin.gallery','admin.galleryAlbum', 'admin.addgallery', 'admin.editgallery','admin.galleryCategory','admin.addgalleryCategory','admin.GalleryDestroyCategory','admin.editgalleryCategory','admin.galleryUpdateCategory') ? 'active' : '' }}"><a href="{{ route('admin.galleryCategory')}}"><span>Manage Gallery</span></a></li>
						<!--<li class="{{ request()->routeIs('admin.blog', 'admin.addblog', 'admin.editblog') ? 'active' : '' }}"><a href="{{ route('admin.blog')}}"><span>Blog</span></a></li>-->
						<li class="{{ request()->routeIs('admin.team', 'admin.addteam', 'admin.editteam') ? 'active' : '' }}"><a href="{{ route('admin.team')}}"><span>Our Team</span></a></li>
						<li class="{{ request()->routeIs('admin.faq', 'admin.addfaq', 'admin.editfaq') ? 'active' : '' }}"><a href="{{ route('admin.faq')}}"><span>FAQ</span></a></li>
						<li class="{{ request()->routeIs('admin.setting', 'admin.addsetting', 'admin.editsetting') ? 'active' : '' }}"><a href="{{ route('admin.editsetting',1) }}"><span>Setting</span></a></li>
						<!--<li class="{{ request()->routeIs('admin.bannerImage') ? 'active' : '' }}"><a href="{{ route('admin.bannerImage') }}"><span>Banner Image</span></a></li>-->
						<li class="{{ request()->routeIs('admin.edit_AboutMission') ? 'active' : '' }}"><a href="{{ route('admin.edit_AboutMission',1) }}"><span> About Mission & Vision </span></a></li>
				
					</ul>
				</li>
						   @endif
						   @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.cms", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.cms','admin.editCms') ? 'active' : '' }}">
					<a href="{{route('admin.cms')}}" aria-expanded="false"><i class="la la-gear"></i><span>CMS</span></a>
				</li>
				@else
                       @endif						   
					  
					   @else
						   <li class="nav-dropdown {{ request()->routeIs('admin.cms','admin.editCms') ? 'active' : '' }}">
					<a href="{{route('admin.cms')}}" aria-expanded="false"><i class="la la-gear"></i><span>CMS</span></a>
				</li>
						   @endif
						    @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.calendar", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.calendar') ? 'active' : '' }}">
					<a href="{{route('admin.calendar')}}" aria-expanded="false"><i class="la la-calendar"></i><span>Events</span></a>
				</li>
				@else
				@endif						   
			    @else
						   <li class="nav-dropdown {{ request()->routeIs('admin.calendar') ? 'active' : '' }}">
					<a href="{{route('admin.calendar')}}" aria-expanded="false"><i class="la la-calendar"></i><span>Events</span></a>
				</li>
						   @endif
						   @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.blog", $allowed_routes))
				
				<li class="nav-dropdown {{ request()->routeIs('admin.blog', 'admin.addblog', 'admin.editblog','admin.blogCategory','admin.addblogCategory','admin.editblogCategory') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-bold"></i><span>Blogs</span></a>
					
					<ul class="collapse nav-sub" aria-expanded="true">
						
						<li class="{{ request()->routeIs('admin.blogCategory','admin.addblogCategory','admin.editblogCategory') ? 'active' : '' }}"><a href="{{ route('admin.blogCategory')}}"><span>Blog Category</span></a></li>
						
						<li class="{{ request()->routeIs('admin.blog','admin.addblog', 'admin.editblog') ? 'active' : '' }}"><a href="{{ route('admin.blog')}}"><span>Blog</span></a></li>
						
					</ul>
				</li>
				@else
				@endif						   
			    @else
					<li class="nav-dropdown {{ request()->routeIs('admin.blog', 'admin.addblog', 'admin.editblog','admin.blogCategory','admin.addblogCategory','admin.editblogCategory') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-bold"></i><span>Blogs</span></a>
					
					<ul class="collapse nav-sub" aria-expanded="true">
						
						<li class="{{ request()->routeIs('admin.blogCategory','admin.addblogCategory','admin.editblogCategory') ? 'active' : '' }}"><a href="{{ route('admin.blogCategory')}}"><span>Blog Category</span></a></li>
						
						<li class="{{ request()->routeIs('admin.blog','admin.addblog', 'admin.editblog') ? 'active' : '' }}"><a href="{{ route('admin.blog')}}"><span>Blog</span></a></li>
						
					</ul>
				</li>
					@endif
					
						   @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.volunteer", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.showVolunteer','admin.volunteer','admin.volunteerGallery','admin.addvolunteerGallery','admin.storevolunteerGallery','admin.volunteerGalleryDestroy','admin.volunteerGalleryedit','admin.volunteerGalleryupdate','admin.volunteerTestimonials','admin.vTestimonialsDelete','admin.volunteerTestimonialsUpdate','admin.editvolunteerTestimonials','admin.addvolunteerTestimonials','admin.volunteerTestimonials.category','admin.addvolunteerTestimonials.category','admin.editvolunteerTestimonials.category','admin.volunteerTestimonials_Destroy.category','admin.volunteer_updatePassword') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-gear"></i><span>Volunteer</span></a>
					
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.showVolunteer','admin.volunteer','admin.volunteer_updatePassword') ? 'active' : '' }}"><a href="{{route('admin.volunteer')}}"><span>Volunteer</span></a></li>
						<li class="{{ request()->routeIs('admin.volunteerGallery','admin.volunteerGalleryedit','admin.volunteerGalleryupdate','admin.addvolunteerGallery') ? 'active' : '' }}"><a href="{{ route('admin.volunteerGallery')}}"><span>Volunteer Gallery</span></a></li>
						
						<li class="{{ request()->routeIs('admin.volunteerTestimonials','admin.vTestimonialsDelete','admin.volunteerTestimonialsUpdate','admin.editvolunteerTestimonials','admin.addvolunteerTestimonials','admin.volunteerTestimonials.category','admin.addvolunteerTestimonials.category','admin.editvolunteerTestimonials.category','admin.volunteerTestimonials_Destroy.category') ? 'active' : '' }}"><a href="{{ route('admin.volunteerTestimonials')}}"><span>Volunteer
						Testimonials</span></a></li>
						
					</ul>
				</li>
				@else
				@endif						   
			    @else
					<li class="nav-dropdown {{ request()->routeIs('admin.showVolunteer','admin.volunteer','admin.volunteerGallery','admin.addvolunteerGallery','admin.storevolunteerGallery','admin.volunteerGalleryDestroy','admin.volunteerGalleryedit','admin.volunteerGalleryupdate','admin.volunteerTestimonials','admin.vTestimonialsDelete','admin.volunteerTestimonialsUpdate','admin.editvolunteerTestimonials','admin.addvolunteerTestimonials','admin.volunteerTestimonials.category','admin.addvolunteerTestimonials.category','admin.editvolunteerTestimonials.category','admin.volunteerTestimonials_Destroy.category','admin.volunteer_updatePassword') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-gear"></i><span>Volunteer</span></a>
					
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.showVolunteer','admin.volunteer','admin.volunteer_updatePassword') ? 'active' : '' }}"><a href="{{route('admin.volunteer')}}"><span>Volunteer</span></a></li>
						<li class="{{ request()->routeIs('admin.volunteerGallery','admin.volunteerGalleryedit','admin.volunteerGalleryupdate','admin.addvolunteerGallery') ? 'active' : '' }}"><a href="{{ route('admin.volunteerGallery')}}"><span>Volunteer Gallery</span></a></li>
						
						<li class="{{ request()->routeIs('admin.volunteerTestimonials','admin.vTestimonialsDelete','admin.volunteerTestimonialsUpdate','admin.editvolunteerTestimonials','admin.addvolunteerTestimonials','admin.volunteerTestimonials.category','admin.addvolunteerTestimonials.category','admin.editvolunteerTestimonials.category','admin.volunteerTestimonials_Destroy.category') ? 'active' : '' }}"><a href="{{ route('admin.volunteerTestimonials')}}"><span>Volunteer
						Testimonials</span></a></li>
						
					</ul>
				</li>
					@endif
				
				
				
				 @if(Auth::user()['type'] == "sub_admin")
					   @if(in_array("admin.vlog", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.vlog', 'admin.addvlog', 'admin.editvlog','admin.vlogCategory','admin.addvlogCategory','admin.editvlogCategory') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-file-video-o"></i><span>Vlogs</span></a>
					
					<ul class="collapse nav-sub" aria-expanded="true">
						
						<li class="{{ request()->routeIs('admin.vlogCategory','admin.addvlogCategory','admin.editvlogCategory') ? 'active' : '' }}"><a href="{{ route('admin.vlogCategory')}}"><span>Vlog Category</span></a></li>
						
						<li class="{{ request()->routeIs('admin.vlog','admin.addvlog', 'admin.editvlog') ? 'active' : '' }}"><a href="{{ route('admin.vlog')}}"><span>Vlog</span></a></li>
						
					</ul>
				</li>
				@else
				@endif						   
			    @else
					<li class="nav-dropdown {{ request()->routeIs('admin.vlog', 'admin.addvlog', 'admin.editvlog','admin.vlogCategory','admin.addvlogCategory','admin.editvlogCategory') ? 'active' : '' }}">
						<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-file-video-o"></i><span>Vlogs</span></a>
					
					<ul class="collapse nav-sub" aria-expanded="true">
						
						<li class="{{ request()->routeIs('admin.vlogCategory','admin.addvlogCategory','admin.editvlogCategory') ? 'active' : '' }}"><a href="{{ route('admin.vlogCategory')}}"><span>Vlog Category</span></a></li>
						
						<li class="{{ request()->routeIs('admin.vlog','admin.addvlog', 'admin.editvlog') ? 'active' : '' }}"><a href="{{ route('admin.vlog')}}"><span>Vlog</span></a></li>
						
					</ul>
				</li>
				@endif
				<!--<li class="nav-dropdown {{ request()->routeIs('admin.Mission&Vission') ? 'active' : '' }}">
					<a href="{{route('admin.Mission&Vission')}}" aria-expanded="false"><i class="la la-pencil-square-o"></i><span>Mission & vission</span></a>
				</li>-->

				@if(!empty($user['user_type']))
					@php $type=$user['user_type']; @endphp
				@else
						@php $type= " "; @endphp
				@endif
				
			@if($type == "Mentor")
				@php $typeMentor = "admin.updatePassword"; 
			          $typeFighter ="";
					  $typeFightercare ="";
					  $typeMentorcare="";
			@endphp
			
				@elseif($type == "Fighter")
				@php 
				$typeFighter= "admin.updatePassword"; 
				$typeFightercare ="";
				$typeMentorcare="";
				$typeMentor ="";
				@endphp
				@elseif($type == "Fighter-Caregiver")
				@php $typeFightercare= "admin.updatePassword"; 
				       $typeMentor="";
						 $typeFighter ="";
						
						 $typeMentorcare="";
				@endphp
				@elseif($type == "Mentor-Caregiver")
				@php $typeMentorcare= "admin.updatePassword"; 
				          $typeMentor="";
						 $typeFighter ="";
						 $typeFightercare ="";
						
				@endphp
				@else
					@php 
				         $typeMentor="";
						 $typeFighter ="";
						 $typeFightercare ="";
						 $typeMentorcare="";
				@endphp
			@endif
				
				
				@if(Auth::user()['type'] == "sub_admin")
			   @if(in_array("admin.users", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.users','admin.fighter', 'admin.mentor','admin.mentorview','admin.fighterview','admin.fightersearch','admin.mentoredit','admin.deletedUsers','admin.mentor-cargiver','admin.mentor-caregiverview','admin.mentor-cargiversearch','admin.fighter-cargiver','admin.fighter-caregiverview','admin.fighter-cargiversearch') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-user"></i><span>User</span></a>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.mentor','admin.mentorview','admin.fighterview','admin.fightersearch','admin.mentoredit') ? 'active' : '' }}"><a href="{{ route('admin.mentor')}}"><span>Mentors</span></a></li>
						<li class="{{ request()->routeIs('admin.mentor-cargiver','admin.mentor-caregiverview','admin.mentor-cargiversearch',$typeMentorcare) ? 'active' : '' }}"><a href="{{ route('admin.mentor-cargiver')}}"><span>Mentor CareGiver</span></a></li>
						
						<li class="{{ request()->routeIs('admin.fighter','admin.fighterview','admin.fightersearch',$typeFighter) ? 'active' : '' }}"><a href="{{ route('admin.fighter')}}"><span>Fighters</span></a></li>
						
						<li class="{{ request()->routeIs('admin.fighter-cargiver','admin.fighter-caregiverview','admin.fighter-cargiversearch',$typeFightercare) ? 'active' : '' }}"><a href="{{ route('admin.fighter-cargiver')}}"><span>Fighter CareGiver</span></a></li>
						
						<li class="{{ request()->routeIs('admin.deletedUsers') ? 'active' : '' }}"><a href="{{ route('admin.deletedUsers')}}"><span>Trashed</span></a></li>
					</ul>
				</li>
				@else
				@endif						   
			    @else
					<li class="nav-dropdown {{ request()->routeIs('admin.users','admin.fighter', 'admin.mentor','admin.mentorview','admin.fighterview','admin.fightersearch','admin.mentoredit','admin.deletedUsers','admin.mentor-cargiver','admin.mentor-caregiverview','admin.mentor-cargiversearch','admin.fighter-cargiver','admin.fighter-caregiverview','admin.fighter-cargiversearch','admin.assignMentor','admin.updatePassword') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-user"></i><span>User</span></a>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.mentor','admin.mentorview','admin.fighterview','admin.fightersearch','admin.mentoredit',$typeMentor) ? 'active' : '' }}"><a href="{{ route('admin.mentor')}}"><span>Mentors</span></a></li>
						<li class="{{ request()->routeIs('admin.mentor-cargiver','admin.mentor-caregiverview','admin.mentor-cargiversearch',$typeMentorcare) ? 'active' : '' }}"><a href="{{ route('admin.mentor-cargiver')}}"><span>Mentor CareGiver</span></a></li>
						
						<li class="{{ request()->routeIs('admin.fighter','admin.fighterview','admin.fightersearch','admin.assignMentor',$typeFighter) ? 'active' : '' }}"><a href="{{ route('admin.fighter')}}"><span>Fighters</span></a></li>
						
						<li class="{{ request()->routeIs('admin.fighter-cargiver','admin.fighter-caregiverview','admin.fighter-cargiversearch',$typeFightercare) ? 'active' : '' }}"><a href="{{ route('admin.fighter-cargiver')}}"><span>Fighter CareGiver</span></a></li>
						
						<li class="{{ request()->routeIs('admin.deletedUsers') ? 'active' : '' }}"><a href="{{ route('admin.deletedUsers')}}"><span>Trashed</span></a></li>
					</ul>
				</li>
					@endif
 				<!--<li class="nav-dropdown {{ request()->routeIs('admin.volunteer') ? 'active' : '' }}">
					<a href="{{route('admin.volunteer')}}" aria-expanded="false"><i class="la la-gear"></i><span>Volunteer</span></a>
				</li>-->
				@if(Auth::user()['type'] == "sub_admin")
			   @if(in_array("admin.paypalDetails", $allowed_routes))
				  
				<li class="nav-dropdown {{ request()->routeIs('admin.paypalDetails') ? 'active' : '' }}">
					<a href="{{route('admin.paypalDetails')}}" aria-expanded="false"><i class="la la-paypal"></i><span>Payment Management</span></a>
				</li>
				
				@else
					@endif
				@else
					<li class="nav-dropdown {{ request()->routeIs('admin.paypalDetails') ? 'active' : '' }}">
					<a href="{{route('admin.paypalDetails')}}" aria-expanded="false"><i class="la la-paypal"></i><span>Payment Management</span></a>
				</li>
					@endif
				@if(Auth::user()['type'] == "sub_admin")
			   @if(in_array("admin.recording", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.recording') ? 'active' : '' }}">
					<a href="{{route('admin.recording')}}" aria-expanded="false"><i class="la la-camera"></i><span>Video Details</span></a>
				</li>
				@else
					@endif
				@else
					<li class="nav-dropdown {{ request()->routeIs('admin.recording','admin.recordingData') ? 'active' : '' }}">
					<a href="{{route('admin.recording')}}" aria-expanded="false"><i class="la la-camera"></i><span>Video Details</span></a>
				</li>
					@endif
				
					@if(Auth::user()['type'] == "sub_admin")
						 @if(in_array("admin.subadmin_list", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.subadmin_list','admin.edit_subadmin','admin.updatePassword_subadmin') ? 'active' : '' }}">
					<a href="{{route('admin.subadmin_list')}}" aria-expanded="false"><i class="la la-user"></i><span>Admin Users</span></a>
				</li>
				@else
					@endif
				@else
					<li class="nav-dropdown {{ request()->routeIs('admin.subadmin_list','admin.edit_subadmin','admin.updatePassword_subadmin') ? 'active' : '' }}">
					<a href="{{route('admin.subadmin_list')}}" aria-expanded="false"><i class="la la-user"></i><span>Admin Users</span></a>
				</li>
			@endif

            	@if(Auth::user()['type'] == "sub_admin")
					  @if(in_array("admin.select_users_communication", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.post_select_users_communication','admin.select_users_communication','admin.users_communication','admin.Post_users_communication','admin.mentor_communication','admin.mentor_caregiver_communication','admin.fighter_communication','admin.fighter_caregiver_communication','admin.Post_fighter_caregiver_communication','admin.Post_mentor_caregiver_communication','admin.Post_mentor_communication','admin.Post_fighter_communication') ? 'active' : ''}}">
					<a href="{{route('admin.select_users_communication')}}" aria-expanded="false"><i class="la la-user"></i><span>Broadcast  Communication</span></a>
				</li>
				@else
					@endif
				@else
					<li class="nav-dropdown {{ request()->routeIs('admin.post_select_users_communication','admin.select_users_communication','admin.users_communication','admin.Post_users_communication','admin.mentor_communication','admin.mentor_caregiver_communication','admin.fighter_communication','admin.fighter_caregiver_communication','admin.Post_fighter_caregiver_communication','admin.Post_mentor_caregiver_communication','admin.Post_mentor_communication','admin.Post_fighter_communication') ? 'active' : ''}}">
					<a href="{{route('admin.select_users_communication')}}" aria-expanded="false"><i class="la la-user"></i><span>Broadcast  Communication</span></a>
				</li>
			@endif

			
					@if(Auth::user()['type'] == "sub_admin")
						 @if(in_array("admin.request_mentor", $allowed_routes))
		<li class="nav-dropdown {{ request()->routeIs('admin.RedirectMentor','admin.request_mentor','admin.Become_Caregiver','admin.request_Caregiver'
		,'admin.become','admin.Form_Selection','admin.Country','admin.Primary_Language','admin.hear_about_Heroes',
		'admin.Gender','admin.Ethnicity','admin.Maritial_Status','admin.Sexual_Orientataion',
		'admin.Religion','admin.Employment_Status','admin.Fluent_Spoken_Languages','admin.side_effects',
		'admin.communicate_with_the_person','admin.speak_with_a_peer_a_Mentor',
		'admin.communicate_with_your_personalized','Form_Selection','admin.Form_Selection_listing',
		'admin.editForm','admin.Country','admin.Country_listing','admin.editCountry',
		'admin.Primary_Language','admin.Primary_Language_listing','admin.editPrimary','admin.hear_about_Heroes',
		'admin.hear_about_Heroes_listing','admin.editHearabout','admin.editMaritial_Status','admin.Gender',
		'admin.Gender_listing','admin.editGender','admin.Ethnicity','admin.Ethnicity_listing',
		'admin.editEthnicity','admin.Maritial_Status','admin.Maritial_Status_listing',
		'admin.Sexual_Orientataion','admin.Sexual_Orientataion_listing',
		'admin.editSexual_Orientataion','admin.Religion','admin.Religion_listing',
		'admin.editReligion','admin.Employment_Status','admin.Employment_Status_listing','admin.editEmployment_Status',
		'admin.Fluent_Spoken_Languages','admin.Fluent_Spoken_Languages_listing',
		'admin.editFluent_listSpoken_Languages','admin.side_effects','admin.side_effects_listing','admin.editside_effects',
		'admin.communicate_with_the_person','admin.communicate_with_the_person_listing',
		'admin.editcommunicate_with_the_person','admin.speak_with_a_peer_a_Mentor',
		'admin.speak_with_a_peer_a_Mentor_listing','admin.editspeak_with_a_peer_a_Mentor',
		'admin.communicate_with_your_personalized','admin.communicate_with_your_personalized_listing',
		'admin.editcommunicate_with_your_personalized','admin.cancer_treatment_status',
		 'admin.cancer_treatment_status_listing',
		 'admin.editcancer_treatment_status',
		 'admin.cancer_stage_or_grade',
		 'admin.cancer_stage_or_grade_listing',
		 'admin.editcancer_stage_or_grade',
		 'admin.Did_this_cancer_metastasize',
		 'admin.Did_this_cancer_metastasize_listing',
		 'admin.editDid_this_cancer_metastasize',
		  'admin.High_Risk',
		 'admin.High_Risk_listing',
		 'admin.editHigh_Risk',
		 'admin.Mutation_Type',
		 'admin.Mutation_Type_listing',
		 'admin.editMutation_Type') ? 'active' : '' }}">
		<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-question "></i><span>Questions</span></a>
		
		<ul class="collapse nav-sub" aria-expanded="true">
		@php $url = Request::segment(3); @endphp
	@if($url == "Become A Mentor CareGiver")
		@foreach(CommonHelper::requestData() as $data)
	       @php $request[] = $data;
		        $request1 ="";
		        $request2 ="";
		        $request3 ="";
				$Mentor3 = "";
				$Mentor ="";
				$Mentor2 ="";
				$Mentor1 ="";
				$Mentor1edit = "";
				$Mentoredit = "";
			    $Mentor2edit = "";
				$Mentor3edit = "";
		  @endphp
		 
		 @endforeach
		 @elseif($url == "Request A Mentor")
					@foreach(CommonHelper::requestData() as $data)
					
				   @php $request1[] = $data;
						$request ="";
						$request2 ="";
						$request3 ="";
						$Mentor3 = "";
						$Mentor ="";
						$Mentor2 ="";
						$Mentor1 ="";
						 $Mentor1edit = "";
							  $Mentoredit = "";
							  $Mentor2edit = "";
							  $Mentor3edit = "";
						
					 @endphp
			@endforeach
				
		   @elseif($url == "Become A Mentor")
				   @foreach(CommonHelper::requestData() as $data)
				   @php $request2[] = $data; 
						$request ="";
						$request1 ="";
						$request3 ="";
						$Mentor3 = "";
						$Mentor ="";
						$Mentor2 ="";
						$Mentor1 ="";
						$Mentor1edit = "";
						$Mentoredit = "";
					    $Mentor2edit = "";
					    $Mentor3edit = "";
						
				   @endphp
				@endforeach
		   @elseif($url == "Request A Mentor CareGiver")
					@foreach(CommonHelper::requestData() as $data)
				   @php $request3[] = $data ;
						$request ="";
						$request2 ="";
						$request1 ="";
						$Mentor3 = "";
						$Mentor ="";
						$Mentor2 ="";
						$Mentor1 ="";
						$Mentor3edit = "";
						$Mentoredit = "";
					    $Mentor1edit = "";
					    $Mentor2edit = "";
						
				   @endphp
				@endforeach	
				@else
					@php
					       $request3 = "";
							$request ="";
							$request2 ="";
							$request1 ="";
							$Mentor3 = "";
							$Mentor ="";
							$Mentor2 ="";
							$Mentor1 ="";
							$Mentor3edit[] = "";
						$Mentoredit[] = "";
					    $Mentor1edit[] = "";
					    $Mentor2edit[] = "";
							
							@endphp
		@endif
		
		@if(Request::segment(2)=="edit")
			 @php
		 $id=Request::segment(4);
			  $url1 = App\Model\AddForm::find($id); @endphp
	      
			@if($url1['form_name']=="Become A Mentor CareGiver")
				     @foreach(CommonHelper::requestData() as $data)
					   @php $Mentor = $data;
							$Mentor1 ="";
							$Mentor2 ="";
							$Mentor3 ="";
							$request3 = "";
							$request ="";
							$request2 ="";
							$request1 ="";
							$Mentoredit[] = $Mentor;
							$Mentor1edit = "";
							$Mentor2edit = "";
						    $Mentor3edit = "";
							
							
					   @endphp
					 @endforeach
				 @elseif($url1['form_name']=="Request A Mentor")
				      @foreach(CommonHelper::requestData() as $data)
					  @php $Mentor1 = $data;
							$Mentor ="";
							$Mentor2 ="";
							$Mentor3 ="";
							$request3 = "";
							$request ="";
							$request2 ="";
							$request1 ="";
							$Mentor1edit[] = $Mentor1;
							$Mentoredit = "";
							$Mentor2edit = "";
						    $Mentor3edit = "";
						 @endphp 
						  
					@endforeach
					
					
			     @elseif($url1['form_name']=="Become A Mentor")
						  @foreach(CommonHelper::requestData() as $data)
						   @php $Mentor2 = $data; 
								$Mentor ="";
								$Mentor1 ="";
								$Mentor3 ="";
								$request3 = "";
								$request ="";
								$request2 ="";
								$request1 ="";
							  $Mentor2edit[] = $Mentor2;
							  $Mentoredit = "";
							  $Mentor1edit = "";
							  $Mentor3edit = "";
								
						   @endphp
						@endforeach
			     @elseif($url1['form_name']=="Request A Mentor CareGiver")
							
						  @foreach(CommonHelper::requestData() as $data)
							   @php $Mentor3 = $data ;
									$Mentor ="";
									$Mentor2 ="";
									$Mentor1 ="";
									$request3 = "";
									$request ="";
									$request2 ="";
									$request1 ="";
									$Mentor3edit[] = $Mentor3;
							        $Mentoredit = "";
							        $Mentor1edit = "";
							        $Mentor2edit = "";
									
							   @endphp
							@endforeach
							@else
					@php
					       $request3 = "";
							$request ="";
							$request2 ="";
							$request1 ="";
							$Mentor3 = "";
							$Mentor ="";
							$Mentor2 ="";
							$Mentor1 ="";
							$Mentor2edit = "";
						    $Mentoredit = "";
							$Mentor1edit = "";
							$Mentor3edit = "";
							
							@endphp
							
			  @endif
			  @endif
			
			<li class="{{ request()->routeIs('admin.request_mentor',$request1,$Mentor1edit) ? 'active' : '' }}"><a href="{{ route('admin.request_mentor')}}"><span>Request A Mentor</span></a></li>
			<li class="{{ request()->routeIs('admin.request_Caregiver',$request3,$Mentor3edit) ? 'active' : '' }}"><a href="{{ route('admin.request_Caregiver')}}"><span>Request A Mentor Caregiver</span></a></li>
			<li class="{{ request()->routeIs('admin.become',$request2,$Mentor2edit) ? 'active' : '' }}"><a href="{{ route('admin.become')}}"><span>Become A Mentor</span></a></li>
			<li class="{{ request()->routeIs('admin.Become_Caregiver',$request,$Mentoredit) ? 'active' : '' }}"><a href="{{ route('admin.Become_Caregiver')}}"><span>Become A Mentor Caregiver</span></a></li>
			
		</ul>
		@else
					@endif
				@else
					<li class="nav-dropdown {{ request()->routeIs('admin.request_mentor','admin.Become_Caregiver','admin.request_Caregiver'
		,'admin.become','admin.Form_Selection','admin.Country','admin.Primary_Language','admin.hear_about_Heroes',
		'admin.Gender','admin.Ethnicity','admin.Maritial_Status','admin.Sexual_Orientataion',
		'admin.Religion','admin.Employment_Status','admin.Fluent_Spoken_Languages','admin.side_effects',
		'admin.communicate_with_the_person','admin.speak_with_a_peer_a_Mentor',
		'admin.communicate_with_your_personalized','Form_Selection','admin.Form_Selection_listing',
		'admin.editForm','admin.Country','admin.Country_listing','admin.editCountry',
		'admin.Primary_Language','admin.Primary_Language_listing','admin.editPrimary','admin.hear_about_Heroes',
		'admin.hear_about_Heroes_listing','admin.editHearabout','admin.editMaritial_Status','admin.Gender',
		'admin.Gender_listing','admin.editGender','admin.Ethnicity','admin.Ethnicity_listing',
		'admin.editEthnicity','admin.Maritial_Status','admin.Maritial_Status_listing',
		'admin.Sexual_Orientataion','admin.Sexual_Orientataion_listing',
		'admin.editSexual_Orientataion','admin.Religion','admin.Religion_listing',
		'admin.editReligion','admin.Employment_Status','admin.Employment_Status_listing','admin.editEmployment_Status',
		'admin.Fluent_Spoken_Languages','admin.Fluent_Spoken_Languages_listing',
		'admin.editFluent_listSpoken_Languages','admin.side_effects','admin.side_effects_listing','admin.editside_effects',
		'admin.communicate_with_the_person','admin.communicate_with_the_person_listing',
		'admin.editcommunicate_with_the_person','admin.speak_with_a_peer_a_Mentor',
		'admin.speak_with_a_peer_a_Mentor_listing','admin.editspeak_with_a_peer_a_Mentor',
		'admin.communicate_with_your_personalized','admin.communicate_with_your_personalized_listing',
		'admin.editcommunicate_with_your_personalized','admin.cancer_treatment_status',
		 'admin.cancer_treatment_status_listing',
		 'admin.editcancer_treatment_status',
		 'admin.cancer_stage_or_grade',
		 'admin.cancer_stage_or_grade_listing',
		 'admin.editcancer_stage_or_grade',
		 'admin.Did_this_cancer_metastasize',
		 'admin.Did_this_cancer_metastasize_listing',
		 'admin.editDid_this_cancer_metastasize',
		  'admin.High_Risk',
		 'admin.High_Risk_listing',
		 'admin.editHigh_Risk',
		 'admin.Mutation_Type',
		 'admin.Mutation_Type_listing',
		 'admin.editMutation_Type','admin.Diagnosis_Type',
		 'admin.Diagnosis_Type_listing',
		 'admin.editDiagnosis_Type') ? 'active' : '' }}">
		<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-question "></i><span>Questions</span></a>
		
		<ul class="collapse nav-sub" aria-expanded="true">
		@php $url = Request::segment(3); @endphp
	@if($url == "Become A Mentor CareGiver")
		@foreach(CommonHelper::requestData() as $data)
		<?php //print_r($data); die;?>
	       @php $request[] = $data;
		        $request1 ="";
		        $request2 ="";
		        $request3 ="";
				$Mentor3 = "";
				$Mentor ="";
				$Mentor2 ="";
				$Mentor1 ="";
				$Mentor1edit = "";
				$Mentoredit = "";
			    $Mentor2edit = "";
				$Mentor3edit = "";
		  @endphp
		 
		 @endforeach
		 @elseif($url == "Request A Mentor")
					@foreach(CommonHelper::requestData() as $data)
					
				   @php $request1[] = $data;
						$request ="";
						$request2 ="";
						$request3 ="";
						$Mentor3 = "";
						$Mentor ="";
						$Mentor2 ="";
						$Mentor1 ="";
						 $Mentor1edit = "";
							  $Mentoredit = "";
							  $Mentor2edit = "";
							  $Mentor3edit = "";
						
					 @endphp
			@endforeach
				
		   @elseif($url == "Become A Mentor")
				   @foreach(CommonHelper::requestData() as $data)
				   @php $request2[] = $data; 
						$request ="";
						$request1 ="";
						$request3 ="";
						$Mentor3 = "";
						$Mentor ="";
						$Mentor2 ="";
						$Mentor1 ="";
						$Mentor1edit = "";
						$Mentoredit = "";
					    $Mentor2edit = "";
					    $Mentor3edit = "";
						
				   @endphp
				@endforeach
		   @elseif($url == "Request A Mentor CareGiver")
					@foreach(CommonHelper::requestData() as $data)
				   @php $request3[] = $data ;
						$request ="";
						$request2 ="";
						$request1 ="";
						$Mentor3 = "";
						$Mentor ="";
						$Mentor2 ="";
						$Mentor1 ="";
						$Mentor3edit = "";
						$Mentoredit = "";
					    $Mentor1edit = "";
					    $Mentor2edit = "";
						
				   @endphp
				@endforeach	
				@else
					@php
					       $request3 = "";
							$request ="";
							$request2 ="";
							$request1 ="";
							$Mentor3 = "";
							$Mentor ="";
							$Mentor2 ="";
							$Mentor1 ="";
							$Mentor3edit[] = "";
						$Mentoredit[] = "";
					    $Mentor1edit[] = "";
					    $Mentor2edit[] = "";
							
							@endphp
		@endif
		
		@if(Request::segment(2)=="edit")
			 @php
		 $id=Request::segment(4);
			  $url1 = App\Model\AddForm::find($id); @endphp
	      
			@if($url1['form_name']=="Become A Mentor CareGiver")
				     @foreach(CommonHelper::requestData() as $data)
					   @php $Mentor = $data;
							$Mentor1 ="";
							$Mentor2 ="";
							$Mentor3 ="";
							$request3 = "";
							$request ="";
							$request2 ="";
							$request1 ="";
							$Mentoredit[] = $Mentor;
							$Mentor1edit = "";
							$Mentor2edit = "";
						    $Mentor3edit = "";
							
							
					   @endphp
					 @endforeach
				 @elseif($url1['form_name']=="Request A Mentor")
				      @foreach(CommonHelper::requestData() as $data)
					  @php $Mentor1 = $data;
							$Mentor ="";
							$Mentor2 ="";
							$Mentor3 ="";
							$request3 = "";
							$request ="";
							$request2 ="";
							$request1 ="";
							$Mentor1edit[] = $Mentor1;
							$Mentoredit = "";
							$Mentor2edit = "";
						    $Mentor3edit = "";
						 @endphp 
						  
					@endforeach
					
					
			     @elseif($url1['form_name']=="Become A Mentor")
						  @foreach(CommonHelper::requestData() as $data)
						   @php $Mentor2 = $data; 
								$Mentor ="";
								$Mentor1 ="";
								$Mentor3 ="";
								$request3 = "";
								$request ="";
								$request2 ="";
								$request1 ="";
							  $Mentor2edit[] = $Mentor2;
							  $Mentoredit = "";
							  $Mentor1edit = "";
							  $Mentor3edit = "";
								
						   @endphp
						@endforeach
			     @elseif($url1['form_name']=="Request A Mentor CareGiver")
							
						  @foreach(CommonHelper::requestData() as $data)
							   @php $Mentor3 = $data ;
									$Mentor ="";
									$Mentor2 ="";
									$Mentor1 ="";
									$request3 = "";
									$request ="";
									$request2 ="";
									$request1 ="";
									$Mentor3edit[] = $Mentor3;
							        $Mentoredit = "";
							        $Mentor1edit = "";
							        $Mentor2edit = "";
									
							   @endphp
							@endforeach
							@else
					@php
					       $request3 = "";
							$request ="";
							$request2 ="";
							$request1 ="";
							$Mentor3 = "";
							$Mentor ="";
							$Mentor2 ="";
							$Mentor1 ="";
							$Mentor2edit = "";
						    $Mentoredit = "";
							$Mentor1edit = "";
							$Mentor3edit = "";
							
							@endphp
							
			  @endif
			  @endif
			
			<li class="{{ request()->routeIs('admin.request_mentor',$request1,$Mentor1edit) ? 'active' : '' }}"><a href="{{ route('admin.request_mentor')}}"><span>Request A Mentor</span></a></li>
			<li class="{{ request()->routeIs('admin.request_Caregiver',$request3,$Mentor3edit) ? 'active' : '' }}"><a href="{{ route('admin.request_Caregiver')}}"><span>Request A Mentor Caregiver</span></a></li>
			<li class="{{ request()->routeIs('admin.become',$request2,$Mentor2edit) ? 'active' : '' }}"><a href="{{ route('admin.become')}}"><span>Become A Mentor</span></a></li>
			<li class="{{ request()->routeIs('admin.Become_Caregiver',$request,$Mentoredit) ? 'active' : '' }}"><a href="{{ route('admin.Become_Caregiver')}}"><span>Become A Mentor Caregiver</span></a></li>
					@endif
				</li>
			
				
				<!-- <li class="nav-dropdown {{ request()->routeIs('admin.users') ? 'active' : '' }}">
					<a href="{{route('admin.users')}}" aria-expanded="false"><i class="la la-gear"></i><span>Users</span></a>
				</li> -->
				
			</ul>
			

			@if(Auth::user()['type'] == "sub_admin")
			   @if(in_array("admin.paypalThankyou", $allowed_routes))
				<li class="nav-dropdown {{ request()->routeIs('admin.paypalThankyou','admin.fighterThankyou','admin.mentorThankyou','admin.mentor-cargiverThankyou','admin.fighter-cargiverThankyou','admin.volunteerThankyouadmin') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-user"></i><span>Manage Thank You Page</span></a>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.paypalThankyou') ? 'active' : '' }}"><a href="{{ route('admin.paypalThankyou')}}"><span>Payment</span></a></li>
						<li class="{{ request()->routeIs('admin.mentorThankyou','admin.mentorview','admin.fighterview','admin.fightersearch','admin.mentoredit') ? 'active' : '' }}"><a href="{{ route('admin.mentorThankyou')}}"><span>Mentor</span></a></li>
						<li class="{{ request()->routeIs('admin.mentor-cargiverThankyou','admin.mentor-caregiverview','admin.mentor-cargiversearch') ? 'active' : '' }}"><a href="{{ route('admin.mentor-cargiverThankyou')}}"><span>Mentor CareGiver</span></a></li>
						
						<li class="{{ request()->routeIs('admin.fighterThankyou','admin.fighterview','admin.fightersearch','admin.assignMentor') ? 'active' : '' }}"><a href="{{ route('admin.fighterThankyou')}}"><span>Fighter</span></a></li>
						
						<li class="{{ request()->routeIs('admin.fighter-cargiverThankyou','admin.fighter-caregiverview','admin.fighter-cargiversearch') ? 'active' : '' }}"><a href="{{ route('admin.fighter-cargiverThankyou')}}"><span>Fighter CareGiver</span></a></li>
						<li class="{{ request()->routeIs('admin.volunteerThankyouadmin') ? 'active' : '' }}"><a href="{{ route('admin.volunteerThankyouadmin')}}"><span>Volunteer</span></a></li>
						
						</ul>
				</li>
				@else
				@endif						   
			    @else
					<li class="nav-dropdown {{ request()->routeIs('admin.paypalThankyou','admin.fighterThankyou','admin.mentorThankyou','admin.mentor-cargiverThankyou','admin.fighter-cargiverThankyou','admin.volunteerThankyouadmin') ? 'active' : '' }}">
					<a class="has-arrow" href="javascript:void(0);" aria-expanded="false"><i class="la la-user"></i><span>Manage Thank You Page</span></a>
					<ul class="collapse nav-sub" aria-expanded="true">
						<li class="{{ request()->routeIs('admin.paypalThankyou') ? 'active' : '' }}"><a href="{{ route('admin.paypalThankyou')}}"><span>Payment</span></a></li>
						<li class="{{ request()->routeIs('admin.mentorThankyou','admin.mentorview','admin.fighterview','admin.fightersearch','admin.mentoredit') ? 'active' : '' }}"><a href="{{ route('admin.mentorThankyou')}}"><span>Mentor</span></a></li>
						<li class="{{ request()->routeIs('admin.mentor-cargiverThankyou','admin.mentor-caregiverview','admin.mentor-cargiversearch') ? 'active' : '' }}"><a href="{{ route('admin.mentor-cargiverThankyou')}}"><span>Mentor CareGiver</span></a></li>
						
						<li class="{{ request()->routeIs('admin.fighterThankyou','admin.fighterview','admin.fightersearch','admin.assignMentor') ? 'active' : '' }}"><a href="{{ route('admin.fighterThankyou')}}"><span>Fighter</span></a></li>
						
						<li class="{{ request()->routeIs('admin.fighter-cargiverThankyou','admin.fighter-caregiverview','admin.fighter-cargiversearch') ? 'active' : '' }}"><a href="{{ route('admin.fighter-cargiverThankyou')}}"><span>Fighter CareGiver</span></a></li>
						<li class="{{ request()->routeIs('admin.volunteerThankyouadmin') ? 'active' : '' }}"><a href="{{ route('admin.volunteerThankyouadmin')}}"><span>Volunteer</span></a></li>
						
						</ul>
				</li>
					@endif

			
				




				
		</nav>
	</div>
</aside>