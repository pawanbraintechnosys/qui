<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Admin;
use App\User;
use App\Model\Post;
use App\Model\Report;
use App\Model\UserReaction;
use App\Model\Category;
use App\Model\Feedback;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use DB;


class AdminController extends Controller
{
     public function index()
    {
        $data = array();
        $data['pageTitle'] = 'Admin Panel';
        $data['total']=User::all()->count();
        $data['active']=User::where('status',0)->get()->count();
        $data['deactivate']=User::where('status',1)->get()->count();
          //print_R($data); die;
        return view('admin.dashboard',compact('data'));
    }






	 public function create()
    {
	$pageTitle = 'Manage Users';
        return view('admin.auth.register', compact('pageTitle'));
    }

	public function store(Request $request)
    {
        // validate the data
        if($request->isMethod('post')){
		$validator = Validator::make($request->all(), [
          'name'          => 'required|string|max:255',
          'email'         => 'required|string|email|max:255|unique:users',
          'password'      => 'required|string|min:8|confirmed',
          'module'      => 'required',
        ]);
		$msg = [
				'message' => 'Something wents wrong!!!',
				'alert-type' => 'error'
				];
				if($validator->fails()){
					return redirect()->intended(route('admin.register'))
					->withErrors($validator)
					->withInput()
					->with($msg);
					
				}
        $tas=$request->module;
		//print_r($tas); die;
		$task= implode(",",$tas);
        $admins = new Admin;
        $admins->name = $request->name;
        $admins->email = $request->email;
        $admins->task = $task;
        $admins->password=bcrypt($request->password);
        $admins->save();
		
		$notification = [
				'message' => 'User has been added successfully!!!',
				'alert-type' => 'success'
				];
        return redirect()->route('admin.subadmin_list')->with($notification);
    }
	}



	 public function categoryAdd()
    {
	$pageTitle = 'Add Category';
        return view('admin.category.add', compact('pageTitle'));
    }

	 public function Post_categoryAdd(Request $request)
    {

		$validator = Validator::make($request->all(), [
          'name'          => 'required|string|max:255',

        ]);
		$msg = [
				'message' => 'Something wents wrong!!!',
				'alert-type' => 'error'
				];
				if($validator->fails()){
					return redirect()->intended(route('admin.categoryAdd'))
					->withErrors($validator)
					->withInput()
					->with($msg);

				}

        $category = new Category;
        $category->name = $request->name;

        $category->save();

		$notification = [
				'message' => 'Category has been added successfully!!!',
				'alert-type' => 'success'
				];
        return redirect()->route('admin.categoryAdd')->with($notification);

	}

	public function category()
    {
        $pageTitle = 'Category';
        $category=Category::all()->where('status',0)->toArray();
        return view('admin.category.index', compact('pageTitle','category'));
    }

    public function help(Request $request)
    {
    	//$ok= $request->getIpaddressofClient();
			//print_r($ok); die;
       $pageTitle = 'Manage Help';
       //$users = User::sortable()->paginate(5);
      $records = Post::query()->orderBy('updated_at', 'DESC')->Where('post_category','help')->Where('post','News');
			if($request->query('search')){

			$records->where('tittle', 'LIKE', "%{$request->input('search')}%")->where('status','!=',3);
			$records->orWhere('description', 'LIKE', "%{$request->input('search')}%")->where('status','!=',3);
			$records->orWhere('user_name', 'LIKE', "%{$request->input('search')}%")->where('status','!=',3);
			}

			$users = $records->Where('post_category','help')->Where('post','News')->sortable("id")->paginate(env('PAGINATION_LIMIT'));
			//$data = Post::groupBy('user_id')->pluck('user_id')->toArray();
			$data = Post::where('updated_at', '>=', date('Y-m-d').' 00:00:00')->where(['post_category'=>'help','post'=>'News'])->where('status','!=',3)->orderBy('updated_at', 'DESC')->get()->unique('user_id')->toArray();
			$main=[];
			foreach($data as $value){
				$valueData=$value['id'];
				array_push($main,$valueData);
			}

        return view('admin.post.help.help',compact('pageTitle','users','main'));
    }

     public function news(Request $request)
    {
       $pageTitle = 'Manage News';
       if(!empty($request->search) && $request->search == 'newest'){
        	//$post_id=Post::Where('post','News')->latest()->first('id')->toArray();
        	
      $records = Post::query();
			
			$users = $records->Where('post','News')->latest()->paginate(env('PAGINATION_LIMIT'));
		}if(!empty($request->search) && $request->search == 'popular'){
            $count=UserReaction::select('post_id', DB::raw('count(*) as total'))
                 ->groupBy('post_id')
                 ->limit(1)
                 ->orderBy('total', 'desc')
                 ->get()->toArray();
                 $records = Post::query();
			
			$users = $records->where('id',$count[0]['post_id'])->Where('post','News')->latest()->sortable("updated_at")->paginate(env('PAGINATION_LIMIT'));
                
		}elseif(!empty($request->search) && $request->search == 'recommend'){
			$records = Post::query();
			
			$users = $records->Where('post','News')->where('recommend',0)->sortable("updated_at")->paginate(env('PAGINATION_LIMIT'));

		}else{
			$records = Post::query();
			if($request->query('search')){
				$records->where('tittle', 'LIKE', "%{$request->input('search')}%")->Where('post','News')->where('status','!=',3);
				$records->orWhere('description', 'LIKE', "%{$request->input('search')}%")->Where('post','News')->where('status','!=',3);
				$records->orWhere('user_name', 'LIKE', "%{$request->input('search')}%")->Where('post','News')->where('status','!=',3);
			}
			$users = $records->Where('post','News')->where('status','!=',3)->sortable("updated_at")->paginate(env('PAGINATION_LIMIT'));
		}

        return view('admin.post.news.news',compact('pageTitle','users'));
    }

     public function newest(Request $request)
    {
       $seg=$request->segment(2);
       //print_r($seg); die;
       $records = Post::query();
			if($request->query('search')){
				$records->where('tittle', 'LIKE', "%{$request->input('search')}%")->where('status','!=',3);
				$records->orWhere('description', 'LIKE', "%{$request->input('search')}%")->where('status','!=',3);
				$records->orWhere('user_name', 'LIKE', "%{$request->input('search')}%")->where('status','!=',3);
			}
       if($seg == 'Help'){ 
			
			$users = $records->Where('post','Needed')->Where('post_category',$seg)->latest()->sortable("updated_at")->paginate(env('PAGINATION_LIMIT'));
			$data = Post::where('updated_at', '>=', date('Y-m-d').' 00:00:00')->where(['post_category'=>'help','post'=>'Needed'])->where('status','!=',3)->orderBy('updated_at', 'DESC')->get()->unique('user_id')->toArray();
			$main=[];
			foreach($data as $value){
				$valueData=$value['id'];
				array_push($main,$valueData);
			}
			return view('admin.post.help.help',compact('pageTitle','users','main'));
		}if($seg == 'warning'){ 
			$users = $records->Where('post','News')->where('status','!=',3)->Where('post_category','warning')->latest()->sortable("updated_at")->paginate(env('PAGINATION_LIMIT'));
			return view('admin.post.warning.warning',compact('pageTitle','users'));

		}elseif($seg == 'Needed'){
			$users = $records->Where('post',$seg)->latest()->sortable("updated_at")->paginate(env('PAGINATION_LIMIT'));
			return view('admin.post.needed.needed',compact('pageTitle','users'));
		}elseif($seg == 'Offered'){
			$users = $records->Where('post',$seg)->latest()->sortable("updated_at")->paginate(env('PAGINATION_LIMIT'));
			return view('admin.post.offered.offered',compact('pageTitle','users'));
		}else{
			$users = $records->Where('post',$seg)->latest()->sortable("updated_at")->paginate(env('PAGINATION_LIMIT'));
			return view('admin.post.news.news',compact('pageTitle','users'));
		}

        
    }

    public function popular(Request $request)
    {
       $seg=$request->segment(2);
       $searchTerm = $request->search;
    
       if($seg == 'Help'){ 
			
			$records = DB::table('posts')
			    ->selectRaw("posts.*, COUNT('user_reactions.post_id') as postsCount")
			    ->where('posts.post','News')
			    ->where('posts.post_category','help ')
			    ->join('user_reactions', 'posts.id', '=', 'user_reactions.post_id')
			    ->groupBy('user_reactions.post_id')
			    ->orderBy('postsCount', 'desc')
                ->where(function($query) use ($searchTerm) {
							$query->Where('posts.user_name', 'LIKE', '%' . $searchTerm . '%')
							->orWhere('posts.tittle', 'LIKE', '%' . $searchTerm . '%')
							->orWhere('posts.description', 'LIKE', '%' . $searchTerm . '%')->where('posts.status','!=',3);
						});
            $users = $records->paginate(env('PAGINATION_LIMIT'));
 
    
			$data = Post::where('updated_at', '>=', date('Y-m-d').' 00:00:00')->where(['post_category'=>'help','post'=>'News'])->where('status','!=',3)->orderBy('updated_at', 'DESC')->get()->unique('user_id')->toArray();
			$main=[];
			foreach($data as $value){
				$valueData=$value['id'];
				array_push($main,$valueData);
			}
			return view('admin.post.help.popular',compact('pageTitle','users','main'));
		}if($seg == 'warning'){ 
			$records = DB::table('posts')
			    ->selectRaw("posts.*, COUNT('user_reactions.post_id') as postsCount")
			    ->where('posts.post','Needed')
			    ->where('posts.post_category','warning ')
			    ->where('posts.status','!=',3)
			    ->join('user_reactions', 'posts.id', '=', 'user_reactions.post_id')
			    ->groupBy('user_reactions.post_id')
			    ->orderBy('postsCount', 'desc')
			    ->where(function($query) use ($searchTerm) {
							$query->Where('posts.user_name', 'LIKE', '%' . $searchTerm . '%')
							->orWhere('posts.tittle', 'LIKE', '%' . $searchTerm . '%')
							->orWhere('posts.description', 'LIKE', '%' . $searchTerm . '%')->where('status','!=',3);
						});
               $users = $records->paginate(env('PAGINATION_LIMIT'));
			return view('admin.post.warning.popular',compact('pageTitle','users'));

		}elseif($seg == 'Needed'){
			$records = DB::table('posts')
			    ->selectRaw("posts.*, COUNT('user_reactions.post_id') as postsCount")
			    ->where('posts.post','Needed')
			    ->where('posts.post_category','!=','help ')
			    ->where('posts.status','!=',3)
			   ->join('user_reactions', 'posts.id', '=', 'user_reactions.post_id')
			    ->groupBy('user_reactions.post_id')
			    ->orderBy('postsCount', 'desc')
			    ->where(function($query) use ($searchTerm) {
							$query->Where('posts.user_name', 'LIKE', '%' . $searchTerm . '%')
							->orWhere('posts.tittle', 'LIKE', '%' . $searchTerm . '%')
							->orWhere('posts.description', 'LIKE', '%' . $searchTerm . '%')->where('status','!=',3);
						});
               $users = $records->paginate(env('PAGINATION_LIMIT'));
  
    return view('admin.post.needed.popular',compact('pageTitle','users'));
		}elseif($seg == 'Offered'){
			$records = DB::table('posts')
			    ->selectRaw("posts.*, COUNT('user_reactions.post_id') as postsCount")
			    ->where('posts.post','Offered')
			    ->where('posts.status','!=',3)
			   ->join('user_reactions', 'posts.id', '=', 'user_reactions.post_id')
			    ->groupBy('user_reactions.post_id')
			    ->orderBy('postsCount', 'desc')
			    ->where(function($query) use ($searchTerm) {
							$query->Where('posts.user_name', 'LIKE', '%' . $searchTerm . '%')
							->orWhere('posts.tittle', 'LIKE', '%' . $searchTerm . '%')
							->orWhere('posts.description', 'LIKE', '%' . $searchTerm . '%')->where('status','!=',3);
						});
   $users = $records->paginate(env('PAGINATION_LIMIT'));
 
    return view('admin.post.offered.popular',compact('pageTitle','users'));
			
		}else{
			$records = DB::table('posts')
    ->selectRaw("posts.*, COUNT('user_reactions.post_id') as postsCount")
    ->where('posts.post','News')
    ->where('posts.post_category','!=','warning ')
    ->where('posts.status','!=',3)
    ->join('user_reactions', 'posts.id', '=', 'user_reactions.post_id')
    ->groupBy('user_reactions.post_id')
    ->orderBy('postsCount', 'desc')
    ->where(function($query) use ($searchTerm) {
							$query->Where('posts.user_name', 'LIKE', '%' . $searchTerm . '%')
							->orWhere('posts.tittle', 'LIKE', '%' . $searchTerm . '%')
							->orWhere('posts.description', 'LIKE', '%' . $searchTerm . '%')->where('status','!=',3);
						});
   $users = $records->paginate(env('PAGINATION_LIMIT'));
 
    return view('admin.post.news.popular',compact('pageTitle','users'));
		}

        
    }

     public function recommend(Request $request)
    {
       $pageTitle = 'Manage Recommend';
       //$users = User::sortable()->paginate(5);
       //echo "hii"; die;
        $seg=$request->segment(2);
       $records = Post::query();
			if($request->query('search')){
				$records->where('tittle', 'LIKE', "%{$request->input('search')}%")->where('status','!=',3);
				$records->orWhere('description', 'LIKE', "%{$request->input('search')}%")->where('status','!=',3);
				$records->orWhere('user_name', 'LIKE', "%{$request->input('search')}%")->where('status','!=',3);
			}
			$users = $records->Where('post',$seg)->where('status','!=',3)->where('recommend',0)->sortable("updated_at")->paginate(env('PAGINATION_LIMIT'));

		
        if($seg=='News'){
        	return view('admin.post.news.recommend',compact('pageTitle','users'));
        }elseif($seg=='Offered'){
        	return view('admin.post.offered.recommend',compact('pageTitle','users'));
        }elseif($seg=='Needed'){
        	return view('admin.post.needed.recommend',compact('pageTitle','users'));
        }
        
    }

     public function recommend_more(Request $request)
    {
       $pageTitle = 'Manage Recommend';
       //$users = User::sortable()->paginate(5);
       //echo "hii"; die;
        $seg=$request->segment(2);
         $seg1=$request->segment(3);
       $records = Post::query();
			if($request->query('search')){
				$records->where('tittle', 'LIKE', "%{$request->input('search')}%")->where('status','!=',3);
				$records->orWhere('description', 'LIKE', "%{$request->input('search')}%")->where('status','!=',3);
				$records->orWhere('user_name', 'LIKE', "%{$request->input('search')}%")->where('status','!=',3);
			}
			$users = $records->Where('post',$seg)->Where('post_category',$seg1)->where('status','!=',3)->where('recommend',0)->sortable("updated_at")->paginate(env('PAGINATION_LIMIT'));

		
        if($seg=='News'){
           return view('admin.post.news.recommend',compact('pageTitle','users'));
        }elseif($seg=='Needed'){ 
            return view('admin.post.needed.recommend',compact('pageTitle','users'));
        }
    }

     public function offered(Request $request)
    {
       $pageTitle = 'Manage Offered';
       //$users = User::sortable()->paginate(5);
      $records = Post::query();
			if($request->query('search')){
				$records->where('tittle', 'LIKE', "%{$request->input('search')}%")->Where('post','Offered')->where('status','!=',3);
				$records->orWhere('description', 'LIKE', "%{$request->input('search')}%")->Where('post','Offered')->where('status','!=',3);
				$records->orWhere('user_name', 'LIKE', "%{$request->input('search')}%")->Where('post','Offered')->where('status','!=',3);
			}

			$users = $records->Where('post','Offered')->where('status','!=',3)->sortable("id")->paginate(env('PAGINATION_LIMIT'));

        return view('admin.post.offered.offered',compact('pageTitle','users'));
    }

    public function needed(Request $request)
    {
       $pageTitle = 'Manage Needed';
       //$users = User::sortable()->paginate(5);
      $records = Post::query();
			if($request->query('search')){
				$records->where('tittle', 'LIKE', "%{$request->input('search')}%")->Where('post','Needed')->where('status','!=',3);
				$records->orWhere('description', 'LIKE', "%{$request->input('search')}%")->Where('post','Needed')->where('status','!=',3);
				$records->orWhere('user_name', 'LIKE', "%{$request->input('search')}%")->Where('post','Needed')->where('status','!=',3);
			}

			$users = $records->Where('post','Needed')->where('status','!=',3)->sortable("id")->paginate(env('PAGINATION_LIMIT'));

        return view('admin.post.needed.needed',compact('pageTitle','users'));
    }

    public function warning(Request $request)
    {
       $pageTitle = 'Manage Warning';
       //$users = User::sortable()->paginate(5);
      $records = Post::query()->Where('post_category','warning')->Where('post','Needed');
			if($request->query('search')){
				$records->where('tittle', 'LIKE', "%{$request->input('search')}%")->Where('post_category','warning')->Where('post','Needed')->where('status','!=',3);
				$records->orWhere('description', 'LIKE', "%{$request->input('search')}%")->Where('post_category','warning')->Where('post','Needed')->where('status','!=',3);
				$records->orWhere('user_name', 'LIKE', "%{$request->input('search')}%")->Where('post_category','warning')->Where('post','Needed')->where('status','!=',3);
			}

			$users = $records->Where('post_category','warning')->Where('post','Needed')->where('status','!=',3)->sortable("id")->paginate(env('PAGINATION_LIMIT'));

        return view('admin.post.warning.warning',compact('pageTitle','users'));
    }

    public function postChange_status(Request $request){

		if($request->ajax()){

			//$user = User::find($request->seg);

			try{
						//User::where('id', $user->id)->update([]);$data=[AssignMentor];

						$assindata=Post::where('id',$request->id)->get()->toArray();

						if(!empty($assindata) && $assindata[0]['status'] == 0){
						$data=Report::where('post_id',$request->id)->get()->toArray();
						if(!empty($data)){
                         Report::where('post_id', $request->id)->update(['status'=>'1']);
						}

						 Post::where('id', $request->id)->update(['status'=>'1']);
						 $response = array('status'=>1,'msg'=>'Post deactivated successfully.');

						}if(!empty($assindata) && $assindata[0]['status'] == 1){
                            $data=Report::where('post_id',$request->id)->get()->toArray();
						if(!empty($data)){
                         Report::where('post_id', $request->id)->update(['status'=>'0']);
						}
                          Post::where('id', $request->id)->update(['status'=>'0']);
                          $response = array('status'=>1,'msg'=>'Post activated successfully.');
						}


					}catch(\Exception $e){
						$response = array('status'=>0,'msg'=>'something went wrong');

					}

		}
	 	return response()->json($response);
	}

	 public function recommendChange(Request $request){

		if($request->ajax()){

			//$user = User::find($request->seg);

			try{
						//User::where('id', $user->id)->update([]);$data=[AssignMentor];

						$assindata=Post::where('id',$request->id)->get()->toArray();

						if(!empty($assindata) && $assindata[0]['recommend'] == 0){

						 Post::where('id', $request->id)->update(['recommend'=>'1']);
						 $response = array('status'=>1,'msg'=>'Post recommendation rejected successfully.');

						}if(!empty($assindata) && $assindata[0]['recommend'] == 1){

                          Post::where('id', $request->id)->update(['recommend'=>'0']);
                          $response = array('status'=>1,'msg'=>'Post recommended successfully.');
						}


					}catch(\Exception $e){
						$response = array('status'=>0,'msg'=>'something went wrong');

					}

		}
	 	return response()->json($response);
	}

	public function postDelete(Request $request){

		if($request->ajax()){
             try{


                         Post::where('id', $request->id)->update(['status'=>3]);
                         $data=Report::where('post_id',$request->id)->get()->toArray();
						if(!empty($data)){
                         Report::where('post_id', $request->id)->update(['status'=>3]);
						}
                          $response = array('status'=>1,'msg'=>'Post has been deleted successfully.');



					}catch(\Exception $e){
						$response = array('status'=>0,'msg'=>'something went wrong');

					}

		}
	 	return response()->json($response);
	}

	public function editPost($id){
		$pageTitle = 'Edit Post';
		$post = Post::find($id);
		if($post->post == 'Needed' && $post->post_category == 'help'){
	return view('admin.post.help.edit',compact('post','pageTitle'));
	}elseif($post->post == 'News' && $post->post_category == 'warning'){
	return view('admin.post.warning.edit',compact('post','pageTitle'));
	}elseif($post->post == 'Offered'){
			return view('admin.post.offered.edit',compact('post','pageTitle'));
		   }elseif($post->post == 'News'){
			return view('admin.post.news.edit',compact('post','pageTitle'));
		   }elseif($post->post == 'Needed'){
			return view('admin.post.needed.edit',compact('post','pageTitle'));
		   }
	}


	public function postUpdate(Request $request, $id){
		if($request->isMethod('PUT')){
			$Post = Post::find($id);

			$validator = Validator::make($request->all(), [
				'tittle' => 'required',
				'description' => 'required',
				'needed_price' => 'nullable',
				'page_name' => 'required',
				'price_type' => 'nullable',
				//'price' => 'nullable|numeric',

			]);

			$msg = [
				'message' => 'Something wents wrong!!!',
				'alert-type' => 'error'
			];
			if($validator->fails()){
				return redirect(route('admin.editPost_'.$request->page_name, $Post->id))
				->withErrors($validator)
				->withInput()
				->with($msg);
			}else{

              if($request->price_type !='' && $request->price_type == 0){

              	$validator1 = Validator::make($request->all(), [
				'price' => 'required|numeric',

			]);

			$msg = [
				'message' => 'Something wents wrong!!!',
				'alert-type' => 'error'
			];
			if($validator1->fails()){
				return redirect(route('admin.editPost_'.$request->page_name, $Post->id))
				->withErrors($validator1)
				->withInput()
				->with($msg);
			}
              }

				/*
					validate image if present
				*/
					if($request->hasFile('image')){

						$validator = Validator::make($request->all(), [
							'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
						]);

						if($validator->fails()){
							return redirect(route('admin.editPost', $Post->id))
							->withErrors($validator)
							->withInput();
						}

						//unlink(public_path().'/upload/image/'.$Post->image);

						$logo = time().'.'.request()->image->getClientOriginalExtension();
				request()->image->move(public_path('upload/image'), $logo);

					$imageName=  $logo;


						$Post->image = $imageName;


					}

					$Post->update($request->post());

					$notification = [
						'message' => 'Post updated successfully!!!',
						'alert-type' => 'success'
					];

	if($Post->post == 'Needed' && $Post->post_category == 'help'){
	return redirect()->intended(route('admin.help'))->with($notification);
	}elseif($Post->post == 'News' && $Post->post_category == 'warning'){
	return redirect()->intended(route('admin.warning'))->with($notification);
	}elseif($Post->post == 'Offered'){
	return redirect()->intended(route('admin.offered'))->with($notification);
   }elseif($Post->post == 'News'){
	return redirect()->intended(route('admin.news'))->with($notification);
   }elseif($Post->post == 'Needed'){
	return redirect()->intended(route('admin.needed'))->with($notification);
   }

				}
			}
			}

	public function Postview($id){
	$pageTitle = 'Post View';
	$post = Post::find($id);
	if($post->post == 'Needed' && $post->post_category == 'help'){
	return view('admin.post.help.view',compact('post','pageTitle'));
	}elseif($post->post == 'News' && $post->post_category == 'warning'){
	return view('admin.post.warning.view',compact('post','pageTitle'));
	}
	elseif($post->post == 'Offered'){
	return view('admin.post.offered.view',compact('post','pageTitle'));
	}elseif($post->post == 'News'){
	return view('admin.post.news.view',compact('post','pageTitle'));
	}elseif($post->post == 'Needed'){
	return view('admin.post.needed.view',compact('post','pageTitle'));
	}
	}

	public function post_userDetails($id){
	$pageTitle = 'User Details';
	$post = User::find($id);
	return view('admin.post.user',compact('post','pageTitle'));
	}
	
	 public function feedback(Request $request)
    {   
       $pageTitle = 'Manage feedback';
       //$users = User::sortable()->paginate(5);
      $records = Feedback::query();
			if($request->query('search')){
				$records->where('rating', 'LIKE', "%{$request->input('search')}%")->where('status',0);
				$records->orwhere('comment', 'LIKE', "%{$request->input('search')}%")->where('status',0);
				//$records->orWhere('sender_name', 'LIKE', "%{$request->input('search')}%")->where('status',0); 
				//$records->orWhere('sender_email', 'LIKE', "%{$request->input('search')}%")->where('status',0); 
				$records->orWhere('receiver_name', 'LIKE', "%{$request->input('search')}%")->where('status',0); 
				$records->orWhere('receiver_email', 'LIKE', "%{$request->input('search')}%")->where('status',0); 
				
			
			}
			
			$users = $records->where('status',0)->sortable("id")->paginate(env('PAGINATION_LIMIT'));
        
        return view('admin.feedback.listing',compact('pageTitle','users'));
    }
	
	public function feedback_edit($id){
		$pageTitle = 'Edit feedback';
		$user = Feedback::find($id);
		//print_r($user); die;
			return view('admin.feedback.edit',compact('user','pageTitle'));
		 
	}
	
	public function feedbackUpdate(Request $request, $id){
		if($request->isMethod('PUT')){
			$Post = Feedback::find($id);

			$validator = Validator::make($request->all(), [
				'rating' => 'required',
				'comment' => 'required',
				//'price' => 'nullable|numeric',
				
			]);

			$msg = [
				'message' => 'Something wents wrong!!!',
				'alert-type' => 'error'
			];
			if($validator->fails()){
				return redirect(route('admin.feedback_edit', $Post->id))
				->withErrors($validator)
				->withInput()
				->with($msg);
			}else{ 
           

					$Post->update($request->post());

					$notification = [
						'message' => 'Review change successfully!!!',
						'alert-type' => 'success'
					];
					
	
	             return redirect()->intended(route('admin.feedback'))->with($notification);
   
					
				}
			}
			}
			
			public function feedbackDelete(Request $request){
		
		if($request->ajax()){
             try{
						
 
                         Feedback::where('id', $request->id)->update(['status'=>1]);
                          $response = array('status'=>1,'msg'=>'Feedback has been deleted successfully.');
					
					  
						
					}catch(\Exception $e){
						$response = array('status'=>0,'msg'=>'something went wrong');

					}

		}   	 
	 	return response()->json($response);
	}

	public function feedbackView($id){
	$pageTitle = 'Feedback View';
	$post = Feedback::find($id);
	return view('admin.feedback.view',compact('post','pageTitle'));
	}

	public function notificationStatus(Request $request){

		if($request->ajax()){
                try{
                      if($request->id == 0){ 
						$assindata=Admin::where('id',1)->update(['status'=>0]);
                         $response = array('status'=>1,'msg'=>'SOS notification turned on successfully.');
                     }
                     if($request->id == 1){
                     	$assindata=Admin::where('id',1)->update(['status'=>1]);
                         $response = array('status'=>1,'msg'=>'SOS notification turned off successfully');
                     }
						}catch(\Exception $e){
						$response = array('status'=>0,'msg'=>'something went wrong');

					}

		}
	 	return response()->json($response);
	}

}
