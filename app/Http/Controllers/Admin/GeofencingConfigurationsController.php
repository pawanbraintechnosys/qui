<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Notification;
use App\Model\User;
use App\Model\Post;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use PubNub\PubNub;
use PubNub\Enums\PNStatusCategory;
use PubNub\Callbacks\SubscribeCallback;
use PubNub\PNConfiguration;
use PubNub\Exceptions\PubNubException;
use Monolog\Handler\ErrorLogHandler;


class GeofencingConfigurationsController extends Controller
{

  protected $pubnub;
  protected $pnconf;


  public function __construct()
  {
      $this->pnconf = new PNConfiguration();
      $this->pnconf->setSubscribeKey(env('SubscribeKey'));
      $this->pnconf->setPublishKey(env('PublishKey'));
      $this->pnconf->setSecretKey(env('SecretKey'));
      $this->pnconf->setSecure(false);
      $this->pubnub = new PubNub($this->pnconf);
      //$this->pubnub->ssl => true;
  }

	public function index($id){
		$pageTitle = 'index';
		$post = Post::find($id);
		if($post->post == 'Needed' && $post->post_category == 'help'){
	return view('admin.post.GeofencingConfigurations.help',compact('post','pageTitle'));
	}elseif($post->post == 'News' && $post->post_category == 'warning'){
	return view('admin.post.GeofencingConfigurations.warning',compact('post','pageTitle'));
	}elseif($post->post == 'Offered'){
			return view('admin.post.GeofencingConfigurations.offered',compact('post','pageTitle'));
		   }elseif($post->post == 'News'){
			return view('admin.post.GeofencingConfigurations.news',compact('post','pageTitle'));
		   }elseif($post->post == 'Needed'){
			return view('admin.post.GeofencingConfigurations.needed',compact('post','pageTitle'));
		   }
	}

   	public function GeofencingConfigurations_Post(Request $request, $id){

		if($request->isMethod('post')){
			$Post = Post::find($id);
           
			$validator = Validator::make($request->all(), [
				'latitude' => 'required',
				'longitude' => 'required',
				'location' => 'required',
				'time' => 'required',
				

			]);

			$msg = [
				'message' => 'Something wents wrong!!!',
				'alert-type' => 'error'
			];
			if($validator->fails()){
				return redirect(route('admin.editPost_'.$request->page_name, $Post->id))
				->withErrors($validator)
				->withInput()
				->with($msg);
			}else{
				$data = $request->post();
				//$Post = Post::find($id);
				
                  $date = $Post->created_at;

                 $time = date("H:i:s", strtotime($request->time));
                $dateTime= $date.' '.$time;
             //  $DT = date_format($dateTime,"Y-m-d H:i:s");

               // $data['updated_at'] =  $DT;
					
					$update= $Post->update($data);
                   // print_r($update); die;
					$notification = [
						'message' => 'Post updated successfully!!!',
						'alert-type' => 'success'
					];

						if($Post->post == 'Needed' && $Post->post_category == 'help'){
						return redirect()->intended(route('admin.help'))->with($notification);
						}elseif($Post->post == 'News' && $Post->post_category == 'warning'){
						return redirect()->intended(route('admin.warning'))->with($notification);
						}elseif($Post->post == 'Offered'){
						return redirect()->intended(route('admin.offered'))->with($notification);
						}elseif($Post->post == 'News'){
						return redirect()->intended(route('admin.news'))->with($notification);
						}elseif($Post->post == 'Needed'){
						return redirect()->intended(route('admin.needed'))->with($notification);
						}

				}
			}
			}

    
	

   
	
}
