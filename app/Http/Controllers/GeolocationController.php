<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Favourite_geolocation;
use Monolog\Handler\ErrorLogHandler;



class GeolocationController extends Controller
{


      public function favourite_geolocations(Request $request)
    {
        $request->validate([
          'name' => 'required|string',
          'address' => 'required',
          'latitude' => 'required',
          'longitude' => 'required',
          'user_id' => 'required',
          'image' => 'required',
        ]);
        $data=$request->all();
        $getData= Favourite_geolocation::where(['latitude'=>$request->latitude,'longitude'=>$request->longitude,'user_id'=>$request->user_id])->get()->toArray();
          if(!empty($getData)){
          	if($request->header()['lang'][0] == true){
               return json_encode(array('msg'=>'Location already exists.','status'=>false));
               }else{
            	 return json_encode(array('msg'=>'Ubicación ya existente','status'=>false));
             }
           
          }else{ 
          Favourite_geolocation::create($data);
          if($request->header()['lang'][0] == true){
             return json_encode(array('msg'=>'Your location has been submitted','status'=>true));
             }else{
          	  return json_encode(array('msg'=>'La ubicación se ha registrado','status'=>true));
          }
         
        }
      
    }

    public function getFavourite_geolocations(Request $request)
    {
       $request->validate([
              'user_id' => 'required',
        ]);
      $category = Favourite_geolocation::where('user_id',$request->user_id)->get()->toArray();
      if(!empty($category)){
      	if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Data found.','data'=>$category,'status'=>true));
        }else{
        return json_encode(array('msg'=>'Información válida','data'=>$category,'status'=>true));
        }
        
      }else{
      	if($request->header()['lang'][0] == true){
        return json_encode(array('msg'=>'Data not found.','status'=>false));
        }else{
       return json_encode(array('msg'=>'Información no válida','status'=>false));
        }
         
      }
  }

     public function removeFavourite_geolocations(Request $request)
    {
       $request->validate([
              'id' => 'required',
      ]);
      $category = Favourite_geolocation::where('id',$request->id)->delete();
      if($category){
      	if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Location has been removed','status'=>true));
          }else{
        	 return json_encode(array('msg'=>'La ubicación se ha eliminado','status'=>true));
        }
        
        }else{
        	if($request->header()['lang'][0] == true){
              return json_encode(array('msg'=>'Location has not been removed','status'=>false));
              }else{
          	 return json_encode(array('msg'=>'La ubicación no se ha podido eliminar','status'=>false));
          }
        
      }
      
      }
  

   
}
