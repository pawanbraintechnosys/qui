<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Notifications\forgotPassword;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Model\passwordReset;
use App\User;
use App\Admin;
use App\Model\UserReaction;
use App\Model\Report;
use App\Model\Notification;
use App\Model\JoinLocation;

use Illuminate\Support\Facades\Validator;
use Session;
use App\Model\Category;
use App\Model\Feedback;
use App\Model\Favourite_geolocation;
use App\Model\Post;
use App\Model\ChatJoin;
use App\Model\SupportChat;
use App\Model\Chat;
use DB;
use App\Model\Help;
use App\Channel\GetPublichannel;
use PubNub\PubNub;
use PubNub\Enums\PNStatusCategory;
use PubNub\Callbacks\SubscribeCallback;
use PubNub\PNConfiguration;
use PubNub\Exceptions\PubNubException;
use Monolog\Handler\ErrorLogHandler;



class ApiController extends Controller
{

  protected $pubnub;
  protected $pnconf;


  public function __construct()
  {
    //$this->middleware('api_token');
      $this->pnconf = new PNConfiguration();
      $this->pnconf->setSubscribeKey(env('SubscribeKey'));
      $this->pnconf->setPublishKey(env('PublishKey'));
      $this->pnconf->setSecretKey(env('SecretKey'));
      $this->pnconf->setSecure(false);
      $this->pubnub = new PubNub($this->pnconf);
      //$this->pubnub->ssl => true;
  }

  public function signup(Request $request)
  {
    $validator = Validator::make($request->all(),[
        'email' => 'required',
        'password' => 'required|string|min:8|confirmed'
    ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
      }else{

        $userEmail=User::where('email',$request->email)->get()->toArray();
      if(!empty($userEmail)){
      if(empty($userEmail[0]['name'])){
        $user =  User::where('id',$userEmail[0]['id'])->update(['password'=>bcrypt($request->password)]);

        if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'User is created Successfully!','data'=>$userEmail[0]['id'],'status'=>true));
        }else{
        	 return json_encode(array('msg'=>'Usuario creado exitosamente','data'=>$userEmail[0]['id'],'status'=>true));
        }

      }else{ 
        $validator = Validator::make($request->all(),[
        'email' => 'required|string|email|unique:users',
        'password' => 'required|string|min:8|confirmed'

        ]);
        if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
           $user = new User([
            'email' => $request->email,
            'password' => bcrypt($request->password)
          ]);
          $user->save();
          return json_encode(array('msg'=>'User is created Successfully!','data'=>$user->id,'status'=>true));
        }
      }
     }else{
        $user = new User([
        'email' => $request->email,
        'password' => bcrypt($request->password)
        ]);
        $user->save();
        return json_encode(array('msg'=>'User is created Successfully!','data'=>$user->id,'status'=>true));
      }

    }
          
  }


     public function create_account(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'username' => 'required||unique:users,username',
            'location' => 'required',
            'longitude'   => 'nullable',
            'latitude'    => 'nullable',
           
        ]);
       $userData=User::where('username',$request->username)->get()->toArray();

        if(!empty($userData)){
        	if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'The usename has already been taken.','status'=>false));
        }else{
        	return json_encode(array('msg'=>'Nombre de usuario no disponible','status'=>false));
        	
        }
            
        }else{ 
      User::where('id',$request->id)->update(['name'=>$request->name,'username'=>$request->username,'location'=>$request->location,'longitude'=>$request->longitude,'latitude'=>$request->latitude]);
      if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'Your registration has been completed successfully.','status'=>true));
        }else{
        	 return json_encode(array('msg'=>'Tu registro se ha completado con éxito','status'=>true));
        	
        }
     
       }
    }

 
  
    public function updateProfile(Request $request){
      $validator = Validator::make($request->all(),[
      'name' => 'nullable|string|max:50',
      'username'  => 'nullable|max:50||unique:users,username',
      'location'     => 'nullable',
      'password'     => 'nullable',
      'longitude'   => 'nullable',
      'latitude'    => 'nullable',
      'distance'    => 'nullable',
      'id'          =>'required',
     ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
        if(!empty($request->image)){
          $validator = Validator::make($request->all(),[
          'image' => 'mimes:jpeg,jpg,png',
          ]);
          if($validator->fails()){
            return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
            }else{
            $host = $request->getHttpHost();
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('upload/profile'), $imageName);
           
          }
        }
        $data = User::find($request->id);
        if(!empty($data)){
          $User = User::where('id', $request->id)->update([
            'name' => ($request->name)? $request->name: $data['name'],
            'username' => ($request->username)? $request->username: $data['username'],
            'latitude' => ($request->latitude)? $request->latitude: $data['latitude'],
            'longitude' => ($request->longitude)? $request->longitude: $data['longitude'],
            'location' => ($request->location)? $request->location: $data['location'],
            'distance' => ($request->distance)? $request->distance: $data['distance'],
            'image' => ($request->image)? $imageName: $data['image'],
            'password' =>  ($request->password)? bcrypt($request->password): $data['password'],
          ]);
          $updatedata = User::find($request->id);

        
          if(!empty($updatedata['image'])) { 
             $image1 = explode("://",$updatedata['image']);
            
             if($image1[0] == 'https'|| $image1[0] == 'http') { 
                 $updatedata['image'] = $updatedata['image'];
             }else{
               $updatedata['image'] = asset('/upload/profile/'.$updatedata['image']);
             }
              
         
         }else{
           $updatedata['image'] = null;
         }
         if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Your profile has been updated','data'=>$updatedata,'status'=>true));
        }else{
        	  return json_encode(array('msg'=>'Tu perfil se ha actualizado','data'=>$updatedata,'status'=>true));
        }
         
          }else{
          	if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'User does not exist','status'=>false));
        }else{
        	  return json_encode(array('msg'=>'El usuario no existe','status'=>false));
        }
          
        }
      }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
         if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
          $getRating = Feedback::where('receiver_email',$request->email)->get()->count();
       $rat = ($getRating/ 15) * 5;
        $credentials1 = request(['email', 'password']);
        $credentials2 = [
        'username' => $request->email,
        'password' => $request->password
    ];
  
        if(!Auth::attempt($credentials1) && !Auth::attempt($credentials2))
        	//return json_encode(array('msg'=>'The username or password you entered is incorrect.','status'=>false));
        	if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'The username or password you entered is incorrect.','status'=>false));
        }else{
        	return json_encode(array('msg'=>'El usuario o la contraseña son incorrectos','status'=>false));
        }
           
          $user = $request->user();
          $user['rating'] = $rat;
          $user['url'] = asset('/user_detail/Suid='.$user->id);
           if($user->deleteStatus == 0){  
         if($user->status == 0){
        
          $tokenResult = $user->createToken('Personal Access Token');
          $token = $tokenResult->token;
          if ($request->remember_me)
              $token->expires_at = Carbon::now()->addWeeks(1);
          $token->save();
          return response()->json([
              'access_token' => $tokenResult->accessToken,
              'token_type' => 'Bearer',
              'msg'=>'Login successfully!',
              'data'=>$user,
              'status'=>true,
             'expires_at' => Carbon::parse(
                 $tokenResult->token->expires_at
              )->toDateTimeString()
          ]);
        
        }else{
        	//return json_encode(array('msg'=>'Your account has been deactivated','status'=>false));
        	if($request->header()['lang'][0] == true){
        return json_encode(array('msg'=>'Your account has been deactivated','status'=>false));
        }else{
        	return json_encode(array('msg'=>'Tu cuenta ha sido desactivada','status'=>false));
        }
           
        }
        }else{
        	 //return json_encode(array('msg'=>'Your account has been temporarily deactivated','status'=>false));
        	if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Your account has been temporarily deactivated','status'=>false));
        }else{
        	 return json_encode(array('msg'=>'Tu cuenta ha sido temporalmente desactivada','status'=>false));
        }
           
        }
        }
    }
  
    public function forgotPassword(Request $request){
            $validator = Validator::make($request->all(),[
            'email' => 'required',
            ]);
            if($validator->fails()){
                return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
                }else{
                $user1=User::where('email',$request->email)->get()->toArray();
                if(!empty($user1)){
                    $user = User::where('email',$request->email)->get();
                    $user1= $user[0];
                    $user1->notify(new forgotPassword($request->email,$user1[0]['first_name'],$user1[0]['last_name']));
					if($request->header()['lang'][0] == true){
					return json_encode(array('msg'=>"We've sent you an email to help you reset your password. Remember to check your spam inbox if you can't find it.",'status'=>true));
					}else{
					return json_encode(array('msg'=>"Te hemos enviado un email para restablecer tu contraseña. Revisa la bandeja Spam en caso que no te llegue al Inbox",'status'=>true));
					}
                   
                    }else{
                    	if($request->header()['lang'][0] == true){
					return json_encode(array('msg'=>'Email not found','status'=>false));
					}else{
					return json_encode(array('msg'=>'Email no válido','status'=>false));
					}
                   
                }
            }
            
        }
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'Successfully logged out!','status'=>true));
        }else{
        	  return json_encode(array('msg'=>'Cerraste sesión','status'=>true));
        }
        
        
    }

    public function socialLogin(Request $request){
      $validator = Validator::make($request->all(),[
          'email' => 'required',
          'social_id' => 'required',
          'name' => 'nullable|string',
          'username' => 'nullable',
          'location' => 'nullable',
          'social_type'=> 'nullable',
          'image' => 'nullable',
       ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
           $user = User::where('email',$request->email)->first();
           $getRating = Feedback::where('receiver_email',$request->email)->get()->count();
       $rat = ($getRating/ 15) * 5;

        if(!empty($user)){
          $data=$request->post();
          $User1 = User::where('email', $request->email)->update($data);
          $user['rating']= $rat;
           $user['url'] = asset('/user_detail/Suid='.$user['id']);
       if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'Login successfully!','data'=>$user,'status'=>true));
        }else{
            return json_encode(array('msg'=>'Iniciaste sesión','data'=>$user,'status'=>true));
        }
             

         }else{
          $data=[
            'social_id'=>$request->social_id,
            'social_type'=>$request->social_type,
            'location'=>$request->location,
            'username'=>$request->username,
            'name'=>$request->name,
            'image'=>$request->image,
            'email'=>$request->email,];
            $User1 = User::create($data);
           
            $userData = User::find($User1->id)->toArray();
            $user['rating']= $rat;
            $userData['url'] = asset('/user_detail/Suid='.$User1->id);
           // return json_encode(array('msg'=>'Login successfully!','data'=>$userData,'status'=>true));
            if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Login successfully!','data'=>$userData,'status'=>true));
        }else{
         return json_encode(array('msg'=>'Iniciaste sesión','data'=>$userData,'status'=>true));
        }
       
        }
      }
    }

         public function share_page(Request $request)
    {
      $validator = Validator::make($request->all(),[
      'id' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
         $url= str_replace("/api/auth/","/",\Request::fullUrl());
         
           $data=  $url.'/'.$request->id; 
          
      return json_encode(array('msg'=>'Data found','data'=>$data,'status'=>true));
       }
        
    }


       public function category(Request $request)
    {
       
       $data=Category::all()->toArray();
  
      if($data){
        $mainData=[];
         foreach($data as $value){
             $data1 = $value;
             $data1['image'] = asset('/admin/images/add-more-icons/img/').$value['image'];
             array_push($mainData,$data1);
         }
        return json_encode(array('msg'=>'Data found!','data'=>$mainData,'status'=>true));
      }else{
        
        return json_encode(array('msg'=>'data not found.','status'=>false));
      }
    }

     public function getFeedback(Request $request)
    {
       $request->validate([
              'user_id' => 'required',
                      
      ]);
  
     $data = Feedback::where('receiver_id',$request->user_id)->get()->toArray();
      if($data){
         
        return json_encode(array('msg'=>'Data found!','data'=>$data,'status'=>true));
      }else{
        
        return json_encode(array('msg'=>'Data not found.','status'=>false));
      }
 
      
    
  }

  public function getLast_helpPost(Request $request)
  {
       $request->validate([
              'user_id' => 'required',
                      
      ]);
  
      $data1 = Post::where(['user_id'=>$request->user_id,'post_category'=>'help','post'=>'News'])->orderBy('updated_at', 'desc')->get()->toArray();
     
      if($data1){
        $data = Post::where(['user_id'=>$request->user_id,'post_category'=>'help','post'=>'News'])->orderBy('updated_at', 'desc')->first()->toArray();
    
         $mainData=['post_id'=>$data['id'],'tittle'=>$data['tittle'],'lat'=>$data['latitude'],'long'=>$data['longitude']];
        return json_encode(array('msg'=>'Data found!','data'=>$mainData,'status'=>true));
      }else{
        
        return json_encode(array('msg'=>'Data not found.','status'=>false));
      }
  }

   public function getLatLong(Request $request)
  {
       $request->validate([
              'post_id' => 'required',
              
                      
      ]);
  
     $data = Post::where(['id'=>$request->post_id])->first();
    
      if($data){
         $mainData=['lat'=>$data['latitude'],'long'=>$data['longitude']];
        return json_encode(array('msg'=>'Data found!','data'=>$mainData,'status'=>true));
      }else{
        
        return json_encode(array('msg'=>'Data not found.','status'=>false));
      }
  }

  public function map_trackStore(Request $request)
  {
     $validator = Validator::make($request->all(),[
      'tittle' => 'required',
      'lat' => 'required',
      'long' => 'required',
      'post_id' => 'required',
      'user_id' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{

           $alertdata=['lat'=>$request->lat,
                    'long'=>$request->long,
                    'channel'=>$request->post_id.'map',
                    'user_id'=>$request->user_id,
                    'post_id'=>$request->post_id,
                    'status'=>3,
                  ];
        Post::where('id',$request->post_id)->update(['latitude'=>$request->lat,'longitude'=>$request->long]);
        Help::create($alertdata);
       $msgdata=[  'Functions'=>[
                 'lat'=>$request->lat,
                 'lng'=>$request->long,  
          ],
        ];
              
             
          try {

      $result = $this->pubnub->publish()
              ->channel($request->post_id.'_'.$request->tittle)
               ->message($msgdata)
              ->usePost(false)
              ->sync();
         // echo "Publish worked! Timetoken: " . $result->getTimetoken();
              if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Location saved','status'=>true));
        }else{
          return json_encode(array('msg'=>'Ubicación guardada','status'=>true));
        }
           
      }
      catch(\PubNub\Exceptions\PubNubServerException $e) {
        if($request->header()['lang'][0] == true){
        return json_encode(array('msg'=>'Location not save.','timetoken'=>$e->getMessage(),'status'=>false));
        }else{
        return json_encode(array('msg'=>'La ubicación no se pudo guardar','timetoken'=>$e->getMessage(),'status'=>false));
        }
        
         // echo "Error happened while publishing: " . $e->getMessage();
      }
  }
                  
 }

 public function pages(Request $request)
  {
      
     $data = Cms::where('page_uses','About The QUI')->get()->toArray();
    
      if($data){
         $mainData=['about_us'=>asset('/about_us/'),
         'privacy_policy'=>asset('/privacy-policy/')
       ];
        return json_encode(array('msg'=>'Data found!','data'=>$mainData,'status'=>true));
      }else{
        
        return json_encode(array('msg'=>'Data not found.','status'=>false));
      }
  }

          public function joinLocation(Request $request)
          {
            $validator = Validator::make($request->all(),[
            'user_id' => 'required',
            'location_name' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'join' => 'required',
            'image' => 'nullable',
            'type' => 'nullable',
            'post_id' => 'nullable',
            'check' => 'nullable',
            'location_type' => 'nullable',
            ]);
            if($validator->fails()){
              return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
              }else{
                try{ 
                   $joinCount=JoinLocation::where('location_name',$request->location_name)->where('latitude',$request->latitude)->where('longitude',$request->longitude)->where('status',0)->distinct('user_id')->count();
                   
                  if(!empty($request->type )){
                    if($request->check == true ){

                    $join=JoinLocation::where('user_id',$request->user_id)->where('location_name',$request->location_name)->where('latitude',$request->latitude)->where('longitude',$request->longitude)->where('status',0)->where('post_id',$request->post_id)->get()->toArray();
                    // print_r($join); die;
                      if(!empty($join)){
                        return json_encode(array('msg'=>'joined','userCount'=>$joinCount,'status'=>true));

                        }else{
                         return json_encode(array('msg'=>'Data not found','status'=>false));
                      }

                    }else{
                      $join=JoinLocation::where('user_id',$request->user_id)->where('location_name',$request->location_name)->where('latitude',$request->latitude)->where('longitude',$request->longitude)->where('status',0)->where('post_id',$request->post_id)->get()->toArray();
                     
                       if(empty($join)){
                      $data1=['user_id'=>$request->user_id,'location_name'=>$request->location_name,'latitude'=>$request->latitude,'longitude'=>$request->longitude,'join'=>$request->join,'image'=>$request->image,'type'=>1,'post_id'=>$request->post_id,'location_type'=>$request->location_type];
                      $storeData= JoinLocation::create($data1);

                      $id=$storeData->id;
                      $data=JoinLocation::where('id',$id)->get()->toArray();
                       }else{
                        $storeData = false;
                       }
                      if(!empty($storeData)){
                        
                        if($request->header()['lang'][0] == true){
                            return json_encode(array('msg'=>'Data inserted successfully','userCount'=>$joinCount,'data'=>$data,'url'=>asset('/share_Location/').$id,'status'=>true));
                            }else{
                            return json_encode(array('msg'=>'Información registrada con éxito','data'=>$data,'url'=>asset('/share_Location').$id,'status'=>true));
                         }
                       

                        }else{
                          if($request->header()['lang'][0] == true){
                            return json_encode(array('msg'=>'Data not inserted','status'=>false));
                            }else{
                            return json_encode(array('msg'=>'Información sin registrar','status'=>false));
                          }

                      }
                    }

                  }else{
           
                  $join=JoinLocation::where('user_id',$request->user_id)->where('location_name',$request->location_name)->where('latitude',$request->latitude)->where('longitude',$request->longitude)->where('status',0)->get()->toArray();
                  if(!empty($join)){
                      $data=JoinLocation::where('user_id',$request->user_id)->where('location_name',$request->location_name)->where('latitude',$request->latitude)->where('longitude',$request->longitude)->where('status',0)->get()->toArray();
                      //$data[ 'url']= 'https://onlineprojectprogress.com/laravel/qui/share_Location/'.$data[0]['id']; 
                      $locationData= JoinLocation::where(['location_name'=>$request->location_name,'longitude'=>$request->longitude,'latitude'=>$request->latitude])->where('status',0)->get()->toArray();
                      if(!empty($locationData)){
                          $userCount= JoinLocation::where(['location_name'=>$request->location_name,'longitude'=>$request->longitude,'latitude'=>$request->latitude])->where('status',0)->count();

                          }else{
                          $userCount= 0;
                      }
                      return json_encode(array('msg'=>'User alredy join','data'=>$data,'userCount'=>$userCount,'url'=>asset('/share_Location').$data[0]['id'],'status'=>true));
                      }else{
                        $data1=['user_id'=>$request->user_id,'location_name'=>$request->location_name,'latitude'=>$request->latitude,'longitude'=>$request->longitude,'join'=>$request->join,'image'=>$request->image,'location_type'=>$request->location_type];
                        $storeData= JoinLocation::create($data1);
                        $id=$storeData->id;
                        $data=JoinLocation::where('id',$id)->get()->toArray();

                        // $data[ 'url']= 'https://onlineprojectprogress.com/laravel/qui/share_Location/'.$id; 
                        if(!empty($storeData)){
                          $locationData= JoinLocation::where(['location_name'=>$request->location_name,'longitude'=>$request->longitude,'latitude'=>$request->latitude,'status'=>0])->get()->toArray();
                            if(!empty($locationData)){
                              $userCount= JoinLocation::where(['location_name'=>$request->location_name,'longitude'=>$request->longitude,'latitude'=>$request->latitude,'status'=>0])->count();

                              }else{
                              $userCount= 0;
                            }
                          if($request->header()['lang'][0] == true){
                            return json_encode(array('msg'=>'Data inserted successfully','data'=>$data,'userCount'=>$userCount,'url'=>asset('/share_Location/').$id,'status'=>true));
                            }else{
                            return json_encode(array('msg'=>'Información registrada con éxito','data'=>$data,'userCount'=>$userCount,'url'=>asset('/share_Location/').$id,'status'=>true));
                          }

                          }else{
                            if($request->header()['lang'][0] == true){
                              return json_encode(array('msg'=>'Data not inserted','status'=>false));
                              }else{
                              return json_encode(array('msg'=>'Información sin registrar','status'=>false));
                            }

                        }
                   }

                 }
                } catch (\Exception $e) {

                  return json_encode(array('msg'=>$e->getMessage(),'status'=>false));
                  //return $e->getMessage();
              }
            }
          }

   public function getjoinLocation(Request $request)
    {
      $validator = Validator::make($request->all(),[
      'user_id' => 'required',
      'location_name' => 'required',
      'latitude' => 'required',
      'longitude' => 'required',
      
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
          
             $storeData= JoinLocation::where('user_id',$request->user_id)->get()->toArray();
            if(!empty($storeData)){
              $locationData= JoinLocation::where(['location_name'=>$request->location_name,'longitude'=>$request->longitude,'latitude'=>$request->latitude])->get()->toArray();
              if(!empty($locationData)){
                 $userCount= JoinLocation::where(['location_name'=>$request->location_name,'longitude'=>$request->longitude,'latitude'=>$request->latitude])->count();

              }else{
                $userCount= 0;
              }
              return json_encode(array('msg'=>'Data found successfully','data'=>$storeData,'userCount'=>$userCount,'status'=>true));
            }else{
              return json_encode(array('msg'=>'Data not found','status'=>false));
            }
           
      
       }
   }

  public function report(Request $request)
    {
      $validator = Validator::make($request->all(),[
      'user_id' => 'required',
      'post_id' => 'required',
      'msg' => 'nullale',
      'post_title' => 'required',
      'type' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
          $post=Post::where(['id'=>$request->post_id,'status'=>0])->get()->toArray();
          if(!empty($post)){
          $userpost=User::where(['id'=>$post[0]['user_id'],'status'=>0])->get()->toArray();
          $user=User::where(['id'=>$request->user_id,'status'=>0])->get()->toArray();
           if(!empty($userpost) && !empty($user)){
          $data=['userid'=>$request->user_id,'post_id'=>$request->post_id,'msg'=>$request->msg,'post_title'=>$request->post_title,'type'=>$request->type,'post_userid'=>$post[0]['user_id'],'post_username'=>$userpost[0]['name'],'post_useremail'=>$userpost[0]['email'],'user_email'=>$user[0]['email'],'user_name'=>$user[0]['name']];
             $storeData= Report::create($data);
            if(!empty($storeData)){
              if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Data inserted successfully','data'=>$data,'status'=>true));
        }else{
          return json_encode(array('msg'=>'Información registrada con éxito','data'=>$data,'status'=>true));
        }
              
            }else{
              if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Data not inserted','status'=>false));
        }else{
         return json_encode(array('msg'=>'Información sin registrar','status'=>false));
        }
             
            }
        }else{
          if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'User is not valid.','status'=>false));
        }else{
          return json_encode(array('msg'=>'Usuario no válido','status'=>false));
        }
             
            }
        }else{
          if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Post ID is not valid','status'=>false));
        }else{
           return json_encode(array('msg'=>'Usuario no válido','status'=>false));
        }
             
            }
           
      
       }
   }

   

  public function rating(Request $request)
    {
       $request->validate([
              'user_id' => 'required',
              
        ]);

       $getRating = Feedback::where('receiver_id',$request->user_id)->get()->count();
       $rat = ($getRating/ 15) * 5;
      // print_r($rat); die;
         if($getRating != 0){
         return json_encode(array('msg'=>'Data found','Data'=>$rat,'status'=>true));
        }else{
         return json_encode(array('msg'=>'Data not found','status'=>false));
        }

      
    
  }

  public function contactUs(Request $request)
    {
       $page = asset('/contactUs/');
        return json_encode(array('msg'=>'Data found','Data'=>$page,'status'=>true));
       
    }

     public function fileShare(Request $request)
    {
       $validator1 = Validator::make($request->all(), [
        'post_id' => 'required',
        'client_id' => 'required',
        'uuid' => 'required',
        'client_id' => 'required',
        'post_name' => 'required',
        'channel' => 'required',
        'message' => 'nullable',
       'type' => 'required',
       'file_type' => 'required',
        'image' => 'required',
        
                      
      ]);
       if($validator1->fails()){
            return json_encode(array('msg'=>$validator1->errors()->first(),'status'=>false));
        }     //print_r($request->file('image')->getClientOriginalName()); die;
    
     if(!empty($request->image)){
        if($request->file_type == 'image'){
          $validator = Validator::make($request->all(),[
         'image' => 'mimes:jpeg,jpg,png,gif|max:10048',
          ]);
      }elseif($request->file_type == 'video'){
        $validator = Validator::make($request->all(),[
         'image' => 'mimes:mp4,mov,ogg,qt,ogx,oga,ogv,webm | max:20000',
          ]); 
      }elseif($request->file_type == 'audio'){
        $validator = Validator::make($request->all(),[
         'image' => 'nullable',
          ]); 
      }else{
        $validator = Validator::make($request->all(),[
         'image' => 'nullable',
          ]); 
      }
          if($validator->fails()){
            return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
            }else{
            $host = $request->getHttpHost();
      
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('upload/chat'), $imageName);
           
          }
        }
     $data=[ 'post_id' => $request->post_id,
              'client_id' => $request->client_id,
              'uuid' => $request->uuid,
              'post_name' => $request->post_name,
              'message' => $request->message,
              'file_type' => $request->file_type,
              'type' => 'type',
              'image' => $imageName,
              'channel' => $request->channel,
         ];
       //  print_r($imageName); die;
     $data1 = Chat::create($data);
     //$data = Feedback::where('receiver_id',$request->user_id)->get()->toArray();
      if($data){
         if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Data inserted!','data'=>asset('/upload/chat/'.$imageName),'status'=>true));
        }else{
       return json_encode(array('msg'=>'Información registrada con éxito','data'=>asset('/upload/chat/'.$imageName),'status'=>true));
        }
        
      }else{
        if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Data not inserted.','status'=>false));
        }else{
        return json_encode(array('msg'=>'Información sin registrar','status'=>false));
        }
        
      }
 
      
    
  }

      public function alluserdata(Request $request)
    {
      
      $validator = Validator::make($request->all(),[
        'user_id' => 'required',
        
      ]);

       if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
      }else{
        $data = User::find($request->user_id);
		if(!empty($data['image'])){
			$data['image']=asset('upload/profile/'.$data['image']);
		}else{
			$data['image']=asset('upload/profile/default.png');
			
		}
		$data['url']= asset('/user_detail/Suid='.$data->id);
       if(!empty($data)){
       // env('APP_URL').
        return json_encode(array('msg'=>'Data found!','data'=>$data,'status'=>true));
      
     }else{
       return json_encode(array('msg'=>'Data not found!','status'=>false));
     }
    }
  }

   /*    public function favourite_geolocations(Request $request)
    {
        $request->validate([
          'name' => 'required|string',
          'address' => 'required',
          'latitude' => 'required',
          'longitude' => 'required',
          'user_id' => 'required',
          'image' => 'required',
        ]);
      $data=$request->all();
      $getData= Favourite_geolocation::where(['latitude'=>$request->latitude,'longitude'=>$request->longitude,'user_id'=>$request->user_id])->get()->toArray();
        if(!empty($getData)){
          if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'Location already exists.','status'=>false));
        }else{
           return json_encode(array('msg'=>'Ubicación ya existente','status'=>false));
        }
         
        }else{ 
        Favourite_geolocation::create($data);
        if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'Your location has been submitted','status'=>true));
        }else{
            return json_encode(array('msg'=>'La ubicación se ha registrado','status'=>true));
        }
       
      }
      
    }

    public function getFavourite_geolocations(Request $request)
    {
       $request->validate([
              'user_id' => 'required',
        ]);
      $category = Favourite_geolocation::where('user_id',$request->user_id)->get()->toArray();
      if(!empty($category)){
        if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Data found.','data'=>$category,'status'=>true));
        }else{
        return json_encode(array('msg'=>'Información válida','data'=>$category,'status'=>true));
        }
        
      }else{
        if($request->header()['lang'][0] == true){
        return json_encode(array('msg'=>'Data not found.','status'=>false));
        }else{
       return json_encode(array('msg'=>'Información no válida','status'=>false));
        }
         
      }
  }

     public function removeFavourite_geolocations(Request $request)
    {
       $request->validate([
              'id' => 'required',
      ]);
      $category = Favourite_geolocation::where('id',$request->id)->delete();
      if($category){
        if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Location has been removed','status'=>true));
        }else{
           return json_encode(array('msg'=>'La ubicación se ha eliminado','status'=>true));
        }
        
      }else{
        if($request->header()['lang'][0] == true){
        return json_encode(array('msg'=>'Location has not been removed','status'=>false));
        }else{
           return json_encode(array('msg'=>'La ubicación no se ha podido eliminar','status'=>false));
        }
        
      }
      
    }*/
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
   /* public function news(Request $request)
    {
      // User::where_id()->get()->toArray();
      $validator = Validator::make($request->all(),[
      'latitude' => 'required',
      'longitude' => 'required',
      'user_id' => 'required',
      'type' => 'nullable',
      'location_id' => 'nullable',
      'location_name' => 'nullable',

      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
           //return json_encode(array('msg'=>$request->user_id,'status'=>true)); die;
       
        $latitude = $request->latitude;
        $longitude = $request->longitude;
       if($request->page  <= 1){
          $page = 0;
          }else {
          $page = $request->page - 1;
        }
        $limit = 5;
        $offset = ($page * $limit);
      //  $catecory = Post::where('id',$request->category_id)->get()->pluck('name')->toArray();
        $searchTerm = $request->search;
 //print_r($searchTerm); die;
        if($request->type == 'discover'){ 
          
           $Business = Post::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->Where('post','Discover')->where('status',0)->where('type','Discover')->where('latitude',$request->latitude)->where('longitude',$request->longitude)->where('id',$request->location_id)
             ->with('children.userData')->where(function($query) use ($searchTerm) {
             // $query->where('user_name', 'LIKE', '%' . $searchTerm . '%')
              $query->orWhere('post', 'LIKE', '%' . $searchTerm . '%')
              
              ->orWhere('tittle', 'LIKE', '%' . $searchTerm . '%');
             
            })->with('userData')->orderBy('created_at','Desc')->withCount('children')
             //->orderByRaw('ISNULL(distance), distance ASC')
           // ->groupBy("id")
            ->offset($offset)
            ->take($limit)
            ->get()
            ->toArray();
           
        }elseif($request->type == 'sub'){ 
          $locationData=$this->location_search($request->latitude, $request->longitude, $request->location_name,$request->user_id);
          
         $Business = Post::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->where('post','News')->where('status',0)->where('location_id',$locationData['location_id'])->where('latitude',$request->latitude)->where('longitude',$request->longitude)
             ->where(function($query) use ($searchTerm) {
             // $query->where('user_name', 'LIKE', '%' . $searchTerm . '%')
              $query->orWhere('post', 'LIKE', '%' . $searchTerm . '%')
              
              ->orWhere('tittle', 'LIKE', '%' . $searchTerm . '%');
             
            })->with('userData')->orderBy('created_at','Desc')
             //->orderByRaw('ISNULL(distance), distance ASC')
           // ->groupBy("id")
            ->offset($offset)
            ->take($limit)
            ->get()
            ->toArray();
             
               }else{ 
            $Business = Post::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->where('post','News')->where('status',0)->where('type','main')
             ->where(function($query) use ($searchTerm) {
             // $query->where('user_name', 'LIKE', '%' . $searchTerm . '%')
              $query->orWhere('post', 'LIKE', '%' . $searchTerm . '%')
              
              ->orWhere('tittle', 'LIKE', '%' . $searchTerm . '%');
             
            })->with('userData')->orderBy('created_at','Desc')
             //->orderByRaw('ISNULL(distance), distance ASC')
           // ->groupBy("id")
            ->offset($offset)
            ->take($limit)
            ->get()
            ->toArray();
          
         }
        //print_r($Business); die;
        if(!empty($Business)){ 
           $img='https://onlineprojectprogress.com/laravel/qui/upload/image/';
            $url= "https://onlineprojectprogress.com/laravel/qui/share_page";

         $array=[];
          foreach($Business as $value){
          $data = $value;
       
           // $data['children']['image']='https://onlineprojectprogress.com/laravel/qui/upload/image/'.$value['children']['image'];
        //  $data['children']['count'] = array_count_values($value['children']);
          $report=Report::where(['userid'=>$request->user_id,'post_id'=>$value['id'],'status'=>0])->get()->toArray();
          if(!empty($report)){

          }else{ 
             
            $data['totalLike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type']])->where('status','!=',0)->count('post_id');
        

         //$data['like'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>1])->count('post_id');
        $data['dislike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>0])->count('post_id');
           $data['smile'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>1])->count('post_id');
        $data['heart'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>2])->count('post_id');
        $data['Laugh'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>3])->count('post_id');
        $data['shoke'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>4])->count('post_id');
        $data['sad'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>5])->count('post_id');
        $data['angry'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>6])->count('post_id');
          
         $data['totalDislike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type']])->where('status',0)->count('post_id');
          $data['userLike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'user_id'=>$request->user_id])->where('status','!=',0)->count('post_id');
      
            if($data['totalLike'] == 0){
              $data['likeStatus'] = false;

            }else{
              $data['likeStatus'] = false;
            }

             $data['url']= $url.'/'.$value['id'] ;
           // if(!empty($data['image'])){ 
           // $data['image'] = $img.$value['image'];
         // }
              
              // array_push($array,$data);
          }
           array_push($array,$data);
        }
        //return json_encode(array('msg'=>'data found.','data'=>$array,'status'=>true));
        if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'data found.','data'=>$array,'status'=>true));
        }else{
        return json_encode(array('msg'=>'Información válida','data'=>$array,'status'=>true));
        }
        
          }else{
          	//return json_encode(array('msg'=>'data not available','status'=>false));
          	if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'data not available','status'=>false));
        }else{
        return json_encode(array('msg'=>'Información no válida','status'=>false));
        }
          
        }
       } 
        //return response()->json($request->user());
    }*/

    
    



    /* public function category(Request $request)
    {
      $category = Category::all()->toArray();
        return json_encode(array('msg'=>'Data found.','data'=>$category,'status'=>true));
    }*/

  /*  public function storeNews(Request $request){
    
      $validator = Validator::make($request->all(),[
        'post' => 'required',
        'tittle' => 'required',
        'latitude' => 'required',
        'longitude' => 'required',
        'description' => 'required',
        'post_type'  => 'required',
        'image' => 'nullable',
        'user_image' => 'nullable',
        'user_name' => 'nullable',
        // 'user_last_name' => 'required',
        'user_id' => 'required',
        'post_category' => 'nullable',
        'address' => 'nullable',
        'address_second' => 'nullable',
        'type' => 'nullable',
        'location_id' => 'nullable',
        'location_name' => 'required',
        'user_latitude' => 'nullable',
        'user_longitude' => 'nullable',
        'file_type' => 'nullable',
      ]);

      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
        $locationData=$this->location_search($request->latitude, $request->longitude, $request->location_name,$request->user_id);
     
       $data=$request->all();
       if($request->type != 'sub_discover'){
         $data['location_id']=$locationData['location_id'];
       }
     
       if(!empty($request->image)){ 
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('upload/image'), $imageName);
        }else{
           $imageName = '';
        }
         //print_r($data); die;
        $userData=User::where('id',$request->user_id)->where(['status'=>0])->get()->toArray();
       $data['image'] = $imageName;
       
       $getId=Post::create($data);
        $data1=[ 'post_id' => $getId->id,
                'join_id' => $request->user_id,
                'uuid' => $request->user_id,
               'channel' => $getId->id,
         ];
         ChatJoin::create($data1);
         Chat::create(['channel'=>$getId->id,'uuid'=>$request->user_id,'client_id'=>$request->user_id,'post_id'=>$getId->id,'type'=>$request->post_type == 0 ? 'public' : 'private','join_status'=>1,'status'=>0]);
         
       if(!empty($request->type) &&  $request->type == 'sub_discover'){ 
       $joindata=JoinLocation::where(['post_id'=>$request->location_id,'status'=>0])->where('user_id','!=',$request->user_id)->get()->toArray();
       foreach($joindata as $key=>$value){
          if(empty($value['unreadmsg_count']) || $value['unreadmsg_count'] == 0){
            JoinLocation::where(['id'=>$value['id']])->update(['unreadmsg_count'=>1]);
          }else{
            $msgCount= $value['unreadmsg_count'] + 1;
            JoinLocation::where(['id'=>$value['id']])->update(['unreadmsg_count'=>$msgCount]);
          }
       }
     }else{
       $joindata1=JoinLocation::where(['post_id'=>0,'latitude'=>$request->latitude,'longitude'=>$request->longitude,'status'=>0])->where('user_id','!=',$request->user_id)->get()->toArray();
       foreach($joindata1 as $key=>$value){
          if(empty($value['unreadmsg_count']) || $value['unreadmsg_count'] == 0){
            JoinLocation::where(['id'=>$value['id']])->update(['unreadmsg_count'=>1]);
          }else{
            $msgCount= $value['unreadmsg_count'] + 1;
            JoinLocation::where(['id'=>$value['id']])->update(['unreadmsg_count'=>$msgCount]);
          }
       }
     }
            $userData1 = User::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $request->latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $request->longitude . ') ) + sin( radians(' . $request->latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')]);
             if($request->post_category == 'help' || $request->post_category == 'warning'){
              $userData2 = $userData1->where(['other_notification'=>0]);
             }elseif($request->post_type == 1){
               $userData2 = $userData1->where(['private_notification'=>0]);
             }else{
               $userData2 = $userData1->where('status','!=',2);
             }

            $userData =$userData2->having( 'distance', '<=', 10 )->groupBy('id')->get()->toArray(); 
     
       
       
         // print_r($this->pubnub); die;
       $ids=[];
        foreach($userData as $value){
              $notifydata=['title'=>$request->tittle,'description'=>$request->description,'user_name'=>$value['name'],'user_email'=>'','user_id'=>$value['id'],'post_id'=>$getId->id,'type'=>'user'];
            Notification::create($notifydata); 
            if($value['id'] != $request->user_id){
            array_push($ids,$value['id']); 
            } 
          }
          $notifydata=['pn_gcm'=>['data'=>['text'=>$request->tittle,'message'=>$request->description,'uuids'=>$ids]]];
           $result = $this->pubnub->publish()
             ->channel('notify')
              ->message($notifydata)
              ->sync();
         //$url= str_replace("/api/auth/storeNeeded",\Request::fullUrl(),"/share_page");
         
           $needyData=[ 'url'=>  'https://onlineprojectprogress.com/laravel/qui/share_page/'.$getId->id ];
         if($request->header()['lang'][0] == true){
      return json_encode(array('msg'=>'Your post has been created','data'=>$needyData,'status'=>true,'ids'=>$ids));
        }else{
        	return json_encode(array('msg'=>'Se ha creado la publicación','data'=>$needyData,'status'=>true));
        }
          
          
      }
    }

    public function storeNeeded(Request $request){
     
      $validator = Validator::make($request->all(),[
      'post' => 'required',
      'tittle' => 'required',
      'latitude' => 'required',
      'longitude' => 'required',
      'user_latitude' => 'nullable',
      'user_longitude' => 'nullable',
      'description' => 'required',
      //'post_type'  => 'required',
      'needed_price'  => 'required',
      //'image' => 'required|mimes:jpeg,jpg,png|max:2048',
      'image' => 'nullable',
      'user_image' => 'required',
      'user_name' => 'required',
     // 'user_last_name' => 'required',
      'user_id' => 'required',
      'address' => 'nullable',
      'address_second' => 'nullable',
      // 'post_category' => 'nullable',
      'type' => 'nullable',
      'location_id' => 'nullable',
      'location_name' => 'nullable',
       'file_type' => 'nullable',
      
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{

        	if(!empty($request->location_name)){
        $locationData=$this->location_search($request->latitude, $request->longitude, $request->location_name,$request->user_id);
         if(!empty($locationData)){
             $data['location_id']=$locationData['location_id'];
       }
       }
       $data=$request->all();
      //return json_encode(array('msg'=>'Your post has been created successfully.','data'=>$data,'status'=>true)); die;
       if(!empty($request->image)){ 
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
    request()->image->move(public_path('upload/image'), $imageName);
        }else{
          $imageName='';
        }
         $data['image'] = $imageName;
       $getId=Post::create($data);
        $data1=[ 'post_id' => $getId->id,
                'join_id' => $request->user_id,
                'uuid' => $request->user_id,
               'channel' => $getId->id,
         ];
         ChatJoin::create($data1);
         Chat::create(['channel'=>$getId->id,'uuid'=>$request->user_id,'client_id'=>$request->user_id,'post_id'=>$getId->id,'type'=>'public','join_status'=>1,'status'=>0]);
        $getId1=Post::where('id',$request->user_id)->get()->toArray();
         $userData = User::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $request->latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $request->longitude . ') ) + sin( radians(' . $request->latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->where('status','!=',2)->having( 'distance', '<=', 10 )->get()->toArray();
 // print_r($userData); die;
         $ids=[];
        foreach($userData as $value){
       // $notifydata=['pn_gcm'=>['data'=>['text'=>$request->tittle,'message'=>$request->description]]];
        $notifydataIos=['pn_apns'=>['data'=>['text'=>$request->tittle,'message'=>$request->description]]];
       
            if($value['id'] != $request->user_id){
            array_push($ids,$value['id']); 
            }
              $notifydata=['title'=>$request->tittle,'description'=>$request->description,'user_name'=>$value['name'],'user_email'=>'','user_id'=>$value['id'],'post_id'=>$getId->id,'type'=>'user'];
            Notification::create($notifydata);  
          }
          $notifydata1=['pn_gcm'=>['data'=>['text'=>$request->tittle,'message'=>$request->description,'uuids'=>$ids]]];

           $result = $this->pubnub->publish()
             ->channel('notify')
               ->message($notifydata1)
              ->sync();
            
        
       
        // $url= str_replace("/api/auth/storeNeeded","/share_page",\Request::fullUrl());
         
           $needyData=[ 'url'=>  'https://onlineprojectprogress.com/laravel/qui/share_page/'.$getId->id ];
         
          return json_encode(array('msg'=>'Your post has been created successfully.','data'=>$needyData,'status'=>true));
          
      }
    }



      public function storeOffered(Request $request){

      $validator = Validator::make($request->all(),[
      'post' => 'required',
      'tittle' => 'required',
      'latitude' => 'required',
      'longitude' => 'required',
      'description' => 'required',
      //'post_type'  => 'required',
      'image' => 'nullable',
      'user_image' => 'required',
      'user_name' => 'required',
     // 'user_last_name' => 'required',
      'price' => 'nullable',
      'price_type' => 'required',
      'user_id' => 'required',
      'address' => 'nullable',
      'address_second' => 'nullable',
      'type' => 'nullable',
      'location_id' => 'nullable',
      'location_name' => 'required',
       'file_type' => 'nullable',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
       $locationData=$this->location_search($request->latitude, $request->longitude, $request->location_name,$request->user_id);
        
       $data=$request->all();
      $data['location_id']=$locationData['location_id'];
         
       if(!empty($request->image)){ 
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
    request()->image->move(public_path('upload/image'), $imageName);
        }else{
          $imageName ='';
        }
        $userData=User::where('id',$request->user_id)->get()->toArray();
       $data['image'] = $imageName;
         $getId=Post::create($data);
         $data1=[ 'post_id' => $getId->id,
                'join_id' => $request->user_id,
                'uuid' => $request->user_id,
               'channel' => $getId->id,
         ];
         ChatJoin::create($data1);
         Chat::create(['channel'=>$getId->id,'uuid'=>$request->user_id,'client_id'=>$request->user_id,'post_id'=>$getId->id,'type'=>'public','join_status'=>1,'status'=>0]);
         $userData = User::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $request->latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $request->longitude . ') ) + sin( radians(' . $request->latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->where('status','!=',2)->having( 'distance', '<=', 10 )->get()->toArray();
          $ids=[];
        foreach($userData as $value){
        $notifydataIos=['pn_apns'=>['data'=>['text'=>$request->tittle,'message'=>$request->description]]];
       
              if($value['id'] != $request->user_id){
               array_push($ids,$value['id']); 
            } 
              $notifydata=['title'=>$request->tittle,'description'=>$request->description,'user_name'=>$value['name'],'user_email'=>'','user_id'=>$value['id'],'post_id'=>$getId->id,'type'=>'user'];
            Notification::create($notifydata);  
          }
           $notifydata=['pn_gcm'=>['data'=>['text'=>$request->tittle,'message'=>$request->description,'uuids'=>$ids]]];
           $result = $this->pubnub->publish()
              
              ->channel('notifications')
                
               ->message($notifydata)
              ->sync();
       
         
           $needyData=[ 'url'=> 'https://onlineprojectprogress.com/laravel/qui/share_page/'.$getId->id ];
         
          return json_encode(array('msg'=>'Your post has been created successfully.','data'=>$needyData,'status'=>true));
          
          
      }
    }

     public function offered(Request $request)
    {
     
      $validator = Validator::make($request->all(),[
      'latitude' => 'required',
      'longitude' => 'required',
      'user_id' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
        $latitude = $request->latitude;
        $longitude = $request->longitude;
       if($request->page  <= 1){
          $page = 0;
          }else {
          $page = $request->page - 1;
        }
        $limit = 5;
        $offset = ($page * $limit);
     
        $searchTerm = $request->search;
        
            $Business = Post::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->where('post','Offered')->where('status',0)
           
            ->where(function($query) use ($searchTerm) {
             
              $query->orWhere('tittle', 'LIKE', '%' . $searchTerm . '%')
              ->orWhere('post', 'LIKE', '%' . $searchTerm . '%');
             
            })->with('userData')->orderBy('created_at','Desc')
           
            ->offset($offset)
            ->take($limit)
            ->get()
            ->toArray();
          
         
        
        if(!empty($Business)){ 
            $img='https://onlineprojectprogress.com/laravel/qui/upload/image/';
              $url= "https://onlineprojectprogress.com/laravel/qui/share_page";
           
           $array=[];
          foreach($Business as $value){
            $data = $value;
           
        $data['dislike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>0])->count('post_id');
           $data['smile'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>1])->count('post_id');
        $data['heart'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>2])->count('post_id');
        $data['Laugh'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>3])->count('post_id');
        $data['shoke'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>4])->count('post_id');
        $data['sad'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>5])->count('post_id');
        $data['angry'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>6])->count('post_id');
          $data['totalLike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type']])->where('status','!=',0)->count('post_id');
         $data['totalDislike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type']])->where('status',0)->count('post_id');
          $data['userLike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'user_id'=>$request->user_id])->where('status','!=',0)->count('post_id');
          if($data['totalLike'] == 0){
              $data['likeStatus'] = false;

            }else{
              $data['likeStatus'] = false;
            }
             $data['url']= $url.'/'.$value['id'] ;
             if(!empty($data['image'])){ 
            $data['image'] = $value['image'];
          }
               array_push($array,$data);
          }
         
          if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'data found.','data'=>$array,'status'=>true));
        }else{
        	return json_encode(array('msg'=>'Información válida','data'=>$array,'status'=>true));
        }
         
          }else{
          	
          	if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'data not available','status'=>false));
        }else{
        	 return json_encode(array('msg'=>'Información no válida','status'=>false));
        }
         
        }
       } 
       
    }

     public function needed(Request $request)
    {
      $validator = Validator::make($request->all(),[
      'latitude' => 'required',
      'longitude' => 'required',
      'user_id' => 'required',
      //'user_id' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
        $latitude = $request->latitude;
        $longitude = $request->longitude;
       if($request->page  <= 1){
          $page = 0;
          }else {
          $page = $request->page - 1;
        }
        $limit = 5;
        $offset = ($page * $limit);
      //  $catecory = Post::where('id',$request->category_id)->get()->pluck('name')->toArray();
        $searchTerm = $request->search;
        
            $Business = Post::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->where('post','Needed')->where('status',0)->orderBy('created_at','Desc')
           
            ->where(function($query) use ($searchTerm) {
             // $query->where('user_name', 'LIKE', '%' . $searchTerm . '%')
              $query->orWhere('tittle', 'LIKE', '%' . $searchTerm . '%')
              ->orWhere('post', 'LIKE', '%' . $searchTerm . '%');
             
            })->with('userData')->orderBy('created_at','Desc')
            //->orderByRaw('ISNULL(distance), distance ASC')
           // ->groupBy("id")
            ->offset($offset)
            ->take($limit)
            ->get()
            ->toArray();
          
         
        
        if(!empty($Business)){ 
           $img='https://onlineprojectprogress.com/laravel/qui/upload/image/';
           $url= "https://onlineprojectprogress.com/laravel/qui/share_page";
          
           $array=[];
          foreach($Business as $value){
            $data = $value;
            //$data['like'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>1])->count('post_id');
        $data['smile'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>1])->count('post_id');
        $data['heart'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>2])->count('post_id');
        $data['Laugh'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>3])->count('post_id');
        $data['shoke'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>4])->count('post_id');
        $data['sad'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>5])->count('post_id');
        $data['angry'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>6])->count('post_id');
        $data['dislike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>0])->count('post_id');
         
         
         $data['totalLike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type']])->where('status','!=',0)->count('post_id');
         $data['totalDislike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type']])->where('status',0)->count('post_id');
          $data['userLike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'user_id'=>$value['user_id']])->where('status','!=',0)->count('post_id');
          if($data['totalLike'] == 0){
              $data['likeStatus'] = false;

            }else{
              $data['likeStatus'] = false;
            }
              $data['url']= $url.'/'.$value['id'] ;
             if(!empty($data['image'])){ 
            $data['image'] = $value['image'];
          }
               array_push($array,$data);
          }
          // return json_encode(array('msg'=>'data found.','data'=>$array,'status'=>true));
          if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'data found.','data'=>$array,'status'=>true));
        }else{
         return json_encode(array('msg'=>'Información válida','data'=>$array,'status'=>true));
        }
        
          }else{
          	// return json_encode(array('msg'=>'data found.','data'=>$array,'status'=>true));
          	if($request->header()['lang'][0] == true){
            return json_encode(array('msg'=>'data not available','status'=>false));
        }else{
         return json_encode(array('msg'=>'Información no válida','status'=>false));
        }
         
        }
       } 
        //return response()->json($request->user());
    }*/





 /* public function get_reaction(Request $request)
    {
       $request->validate([
             
             // 'user_id' => 'required',
              'post_id' => 'required',
              'post_type' => 'required',
              
      ]);
   $data = [
      'like' => UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>1])->count('post_id'),
       'dislike' => UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>0])->count('post_id'),
        'smile' => UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>3])->count('post_id'),
        'heart' => UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>4])->count('post_id')
      ];
     
      if($data){
         
        return json_encode(array('msg'=>'Data found!','data'=>$data,'status'=>true));
      }else{
        
        return json_encode(array('msg'=>'Data not found.','status'=>false));
      }
 
      
    
  }

   public function updateNews(Request $request){
      $validator = Validator::make($request->all(),[
        'id' => 'required',
     // 'post' => 'nullable',
      'tittle' => 'nullable',
      'latitude' => 'nullable',
      'longitude' => 'nullable',
      'description' => 'nullable',
      'post_type'  => 'nullable',
      //'image' => 'required|mimes:jpeg,jpg,png',
       'image' => 'nullable',
     // 'user_image' => 'required',
     // 'user_name' => 'required',
     // 'user_last_name' => 'required',
     // 'user_id' => 'required',
     // 'post_category' => 'nullable',
     ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
        if(!empty($request->image)){
          $validator = Validator::make($request->all(),[
          'image' => 'mimes:jpeg,jpg,png',
          ]);
          if($validator->fails()){
            return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
            }else{
            $host = $request->getHttpHost();
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('upload/image'), $imageName);
           
          }
        }
        $data = Post::where('id', $request->id)->get()->toArray();
        if(!empty($data)){
          $User = Post::where('id', $request->id)->update([
          //'post' => ($request->post)? $request->post: $data[0]['post'],
          'tittle' => ($request->tittle)? $request->tittle: $data[0]['tittle'],
           'latitude' => ($request->latitude)? $request->latitude: $data[0]['latitude'],
            'longitude' => ($request->longitude)? $request->longitude: $data[0]['longitude'],
          'description' => ($request->description)? $request->description: $data[0]['description'],
            'post_type' => ($request->post_type)? $request->post_type: $data[0]['post_type'],
         
          'image' => ($request->image)? $imageName: $data[0]['image'],
         
          ]);
          $updatedata = Post::where('id', $request->id)->get()->toArray();

        
          if(!empty($updatedata[0]['image'])) { 
             $image1 = explode("://",$updatedata[0]['image']);
            
             if($image1[0] == 'https'|| $image1[0] == 'http') { 
                 $updatedata[0]['image'] = $updatedata[0]['image'];
             }else{
               $updatedata[0]['image'] = 'https://onlineprojectprogress.com/laravel/qui/upload/image/'.$updatedata[0]['image'];
             }
              
         
         }else{
           $updatedata[0]['image'] = null;
         }
         if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Your post has been updated','data'=>$updatedata[0],'status'=>true));
        }else{
         return json_encode(array('msg'=>'La publicación se ha actualizado','data'=>$updatedata[0],'status'=>true));
        }
         
          }else{
          	if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Post does not exist.','status'=>false));
        }else{
        return json_encode(array('msg'=>'La publicación no existe','status'=>false));
        }
          
        }
      }
    }

    public function updateNeeded(Request $request){
      $validator = Validator::make($request->all(),[
        'id' => 'required',
     // 'post' => 'nullable',
      'tittle' => 'nullable',
      'latitude' => 'nullable',
      'longitude' => 'nullable',
      'description' => 'nullable',
      'needed_price'  => 'nullable',
      //'image' => 'required|mimes:jpeg,jpg,png',
       'image' => 'nullable',
     // 'user_image' => 'required',
     // 'user_name' => 'required',
     // 'user_last_name' => 'required',
     // 'user_id' => 'required',
     // 'post_category' => 'nullable',
     ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
        if(!empty($request->image)){
          $validator = Validator::make($request->all(),[
          'image' => 'mimes:jpeg,jpg,png',
          ]);
          if($validator->fails()){
            return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
            }else{
            $host = $request->getHttpHost();
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('upload/image'), $imageName);
           
          }
        }
        $data = Post::where('id', $request->id)->get()->toArray();
        if(!empty($data)){
          $User = Post::where('id', $request->id)->update([
          //'post' => ($request->post)? $request->post: $data[0]['post'],
          'tittle' => ($request->tittle)? $request->tittle: $data[0]['tittle'],
           'latitude' => ($request->latitude)? $request->latitude: $data[0]['latitude'],
            'longitude' => ($request->longitude)? $request->longitude: $data[0]['longitude'],
          'description' => ($request->description)? $request->description: $data[0]['description'],
            'needed_price' => ($request->needed_price)? $request->needed_price: $data[0]['needed_price'],
         
          'image' => ($request->image)? $imageName: $data[0]['image'],
         
          ]);
          $updatedata = Post::where('id', $request->id)->get()->toArray();

        
          if(!empty($updatedata[0]['image'])) { 
             $image1 = explode("://",$updatedata[0]['image']);
            
             if($image1[0] == 'https'|| $image1[0] == 'http') { 
                 $updatedata[0]['image'] = $updatedata[0]['image'];
             }else{
               $updatedata[0]['image'] = 'https://onlineprojectprogress.com/laravel/qui/upload/image/'.$updatedata[0]['image'];
             }
              
         
         }else{
           $updatedata[0]['image'] = null;
         }
          return json_encode(array('msg'=>'Your post has been updated successfully.','data'=>$updatedata[0],'status'=>true));
          }else{
          return json_encode(array('msg'=>'Post  not exist .','status'=>false));
        }
      }
    }

    public function updateOffered(Request $request){
      $validator = Validator::make($request->all(),[
        'id' => 'required',
     // 'post' => 'nullable',
      'tittle' => 'nullable',
      'latitude' => 'nullable',
      'longitude' => 'nullable',
      'description' => 'nullable',
      'price_type'  => 'nullable',
       'price'  => 'nullable',
      //'image' => 'required|mimes:jpeg,jpg,png',
       'image' => 'nullable',
    
     ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
        if(!empty($request->image)){
          $validator = Validator::make($request->all(),[
          'image' => 'mimes:jpeg,jpg,png',
          ]);
          if($validator->fails()){
            return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
            }else{
            $host = $request->getHttpHost();
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('upload/image'), $imageName);
           
          }
        }
        $data = Post::where('id', $request->id)->get()->toArray();
        if(!empty($data)){
          $User = Post::where('id', $request->id)->update([
          //'post' => ($request->post)? $request->post: $data[0]['post'],
          'tittle' => ($request->tittle)? $request->tittle: $data[0]['tittle'],
           'latitude' => ($request->latitude)? $request->latitude: $data[0]['latitude'],
            'longitude' => ($request->longitude)? $request->longitude: $data[0]['longitude'],
          'description' => ($request->description)? $request->description: $data[0]['description'],
            'price_type' => ($request->price_type)? $request->price_type: $data[0]['price_type'],
            'price' => ($request->price)? $request->price: $data[0]['price'],
         
          'image' => ($request->image)? $imageName: $data[0]['image'],
         
          ]);
          $updatedata = Post::where('id', $request->id)->get()->toArray();

        
          if(!empty($updatedata[0]['image'])) { 
             $image1 = explode("://",$updatedata[0]['image']);
            
             if($image1[0] == 'https'|| $image1[0] == 'http') { 
                 $updatedata[0]['image'] = $updatedata[0]['image'];
             }else{
               $updatedata[0]['image'] = 'https://onlineprojectprogress.com/laravel/qui/upload/image/'.$updatedata[0]['image'];
             }
              
         
         }else{
           $updatedata[0]['image'] = null;
         }
          return json_encode(array('msg'=>'Your post has been updated successfully.','data'=>$updatedata[0],'status'=>true));
          }else{
          return json_encode(array('msg'=>'Post  not exist .','status'=>false));
        }
      }
    }

    public function deleteNews(Request $request)
    {
       $request->validate([
             
             // 'user_id' => 'required',
              'id' => 'required',
             // 'post_type' =>'required',
            
              
      ]);

       $data=Post::where('id',$request->id)->delete();
  
      if($data){
         if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Post has been deleted','status'=>true));
        }else{
        return json_encode(array('msg'=>'La publicación se ha eliminado','status'=>true));
        }
        
      }else{
        if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'Something went wrong','status'=>false));
        }else{
         return json_encode(array('msg'=>'Ocurrió un error','status'=>false));
        }
       
      }
    }

       public function deleteNeeded(Request $request)
    {
       $request->validate([
             
             // 'user_id' => 'required',
              'id' => 'required',
             // 'post_type' =>'required',
            
              
      ]);

       $data=Post::where('id',$request->id)->delete();
  
      if($data){
              if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Post has been deleted','status'=>true));
        }else{
        return json_encode(array('msg'=>'La publicación se ha eliminado','status'=>true));
        }
        
      }else{
        if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'Something went wrong','status'=>false));
        }else{
         return json_encode(array('msg'=>'Ocurrió un error','status'=>false));
        }
       
      }
       // return json_encode(array('msg'=>'Post has deleted successfully!','status'=>true));
     
    }

       public function deleteOffered(Request $request)
    {
       $request->validate([
             
             // 'user_id' => 'required',
              'id' => 'required',
             // 'post_type' =>'required',
            
              
      ]);

       $data=Post::where('id',$request->id)->delete();
  
      if($data){
         
        return json_encode(array('msg'=>'Post has deleted successfully!','status'=>true));
      }else{
        
        return json_encode(array('msg'=>'Someting went wrong.','status'=>false));
      }
 
      
    
  }

  //Chat

  public function personalChat(Request $request)
    {
     $validator = Validator::make($request->all(),[
      'uuid' => 'required',
      'channel' => 'required',
      'client_id' => 'required',
      'message' => 'required',
      'post_id' => 'required',
      //'name' => 'nullable',
      'timetoken' => 'required',
      'type' => 'required',
      'chat_type' => 'nullable',
      'client_id'=> 'required',
      'usertype' => 'nullable',
      'mute' => 'required',
      'notification_status' => 'required',

      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
        	//return json_encode(array('msg'=>'dfgdf','status'=>true)); die;
          $user = User::find($request->uuid);
          $post = Post::find($request->post_id);
          $PersonalChat = Chat::where(['uuid'=>$request->uuid,'client_id'=>$request->client_id,'type'=>$request->type])->get()->toArray();
     if(!empty($post)){
     	 
     	
         $postname = $post->tittle;
         $location_id = $post->location_id;
       
     }else{
      $postname = $user['name'];

     } 

   if(!empty($PersonalChat)){
      $token= rand(); 
   }else{
    $token= rand(); 

    }
         try {
          $data=[
              'channel'=> $request->channel,
              'post_name'=> $postname,
              'uuid'=> $request->uuid,
              'message'=> $request->message,
              'client_id'=> $request->client_id,
              'post_id'=>$request->post_id,
              'type'=>$request->type,
              'chat_type'=>$request->chat_type,
              'start_timetoken'=>$request->timetoken,
              'usertype'=>$request->usertype,
              'muteUnmute'=>$request->mute,
              'status'=>$request->notification_status,
            ];

              $msgdata=[
                // 'channel'=> $request->channel,
                   'uuid'=> $request->uuid,
                    'message'=> $request->message,
                    'created_at'=> $request->created_at,
                    'chat_token'=> $token,
                  ];

        //test
      $notifica=Chat::where('channel',$request->channel)->where('muteUnmute',0)->where('status','!=',2)->where('uuid','!=',$request->uuid)->groupBy('uuid')->pluck('uuid')->toArray();
      
      if(!empty($notifica)){
      $notifyarray=implode(",",$notifica);

      //$this->pnconf->setUuid($request->uuid);
      $notifydataIos=['pn_apns'=>['data'=>['text'=>$request->name,'message'=>$request->message]]];
      $notifydata=['pn_gcm'=>['data'=>['text'=>$request->name,'message'=>$request->message,'uuids'=>$notifica]]];
       
        $result = $this->pubnub->publish()
              ->channel('notify')
              ->message($notifydata)
              ->sync();
             //  return json_encode(array('msg'=>$notifydata,'status'=>true)); die;
     

      }
    

        
      Chat::create($data);

       if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Message sent!','status'=>true));
        }else{
        	 return json_encode(array('msg'=>'Mensaje enviado!','status'=>true));
        }
          
      }
      catch(\PubNub\Exceptions\PubNubServerException $e) {
      	 if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Message send successfully!','timetoken'=>$e->getMessage(),'status'=>false));
        }else{
        	 return json_encode(array('msg'=>'Mensaje enviado!','timetoken'=>$e->getMessage(),'status'=>false));
        }
       
         // echo "Error happened while publishing: " . $e->getMessage();
      }
  }
                  
 }


    public function getChat(Request $request)
    {
     $validator = Validator::make($request->all(),[
     // 'uuid' => 'required',
      'channel' => 'required',
     // 'message' => 'required',
      'user_id' => 'required',
      // 'chat_token' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
     // $this->pnconf->setUuid($request->uuid);

          try {

      $result = $this->pubnub->history()
              //->channel($request->channel)
             ->sync();
     //print_r($result); die;
          $user = Chat::where(['uuid'=>$request->user_id])->get()->toArray();
      
           $dataArray=[];
           $i=1;
        foreach($result->getMessages() as   $value){
          $msg=$value->getEntry();
          //print_r($msg['uuid']); die;
          // if($msg['chat_token'] == $request->chat_token){ 
             $data= User::where('id',$msg['uuid'])->get()->toArray();
             $msg1['_id'] = $i; 
            $msg1['chat_token'] = $msg['chat_token'];
             $msg1['text'] = $msg['message'];
             $msg1['createdAt'] = $msg['created_at'];
             $msg1['user'] =[
              '_id' => $data[0]['id'], 
               'name' => $data[0]['name'], 
               'avatar'=> $data[0]['image'],
             ];
              

                array_push($dataArray,$msg1);
                  $i++;
          // }
            
             
       
        
        }
           $dataReverse=  array_reverse($dataArray);
         // echo "Publish worked! Timetoken: " . $result->getTimetoken();
           return json_encode(array('msg'=>'Data found!','data'=>$dataReverse,'status'=>true));
        
      }
      catch(\PubNub\Exceptions\PubNubServerException $e) {
        return json_encode(array('msg'=>'Data not found!','status'=>false));
         // echo "Error happened while publishing: " . $e->getMessage();
      }
  }
                  
  }*/

 

  /*   public function get_publicChannel(Request $request){ 
        try {
         $request->validate([
          'user_id' => 'required',
          'search' => 'nullable',
          'latitude' => 'nullable',
          'longitude' => 'nullable',
          'type' => 'required',
        ]);

          $searchTerm = $request->search;
          $latitude = $request->latitude;
          $longitude = $request->longitude;
          if(!empty($latitude) && !empty($longitude)){
            $latitude = $request->latitude;
          $longitude = $request->longitude;
        }else{
          $latitude = 0;
          $longitude = 0;
        }
         
           
 
           $allData1 = Chat::select(['chats.*','posts.latitude','posts.longitude','posts.tittle',
             DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( posts.latitude ) )
              * cos( radians(  posts.longitude ) - radians(' . $longitude . ') ) 
              + sin( radians(' . $latitude .') ) * sin( radians(posts.latitude) ) )
                AS DECIMAL(10,2)) AS distance')])
            ->join('posts', 'posts.id', '=', 'chats.post_id')->withCount('user')
            ->where('chats.deleteChannel',0)
            //->where('chats.join_status',0)
           ->whereRaw(
              'chats.id in (
                select max(chats.id) from chats where 
                deleteChannel=0 AND 
                (
                  (chats.type = "discover") OR 
                  ((chats.uuid = '.$request->user_id.' OR chats.client_id = '.$request->user_id.' And chats.join_status != 1)) 
                )
               AND (`chats`.`message` LIKE "%'.$searchTerm.'%" OR EXISTS(
                    SELECT
                        *
                    FROM
                        `users`
                    WHERE
                        `chats`.`client_id` = `users`.`id` AND (`name` LIKE "%'.$searchTerm.'%") OR (`post_name` LIKE "%'.$searchTerm.'%")
                ) OR EXISTS(
                    SELECT
                        *
                    FROM
                        `users`
                    WHERE
                        `chats`.`uuid` = `users`.`id` AND (`name` LIKE "%'.$searchTerm.'%") OR (`post_name` LIKE "%'.$searchTerm.'%")
                ))
                group by chats.channel)
              
            ');

        // print_r($allData1); die;
          
           if($request->type == 'Discover'){ 
             if(!empty($request->search)){
               $allData2 = $allData1->with('Postdata.children')->orderByRaw('distance', 'ASC');

             }else{ 
            $allData2 = $allData1->with('Postdata.children')->orderByRaw('distance', 'ASC')->having("distance", "<=",10);
           }
         }else{
           $allData2 = $allData1->with('Postdata.children')->orderBy('created_at','Desc');
         }
         $allData=  $allData2->get()->toArray();
          
           
           $privateChat = Chat::where('deleteChannel',0)->where('post_id',0)
           ->whereRaw(
              'chats.id in (
                select max(chats.id) from chats where 
                deleteChannel=0 AND 
                (
                  (chats.type = "private" AND chats.post_id = "0") AND 
                  ((chats.uuid = '.$request->user_id.' OR chats.client_id = '.$request->user_id.' And chats.join_status != 1)) 
                )
               AND (`chats`.`message` LIKE "%'.$searchTerm.'%" OR `chats`.`post_name` LIKE "%'.$searchTerm.'%"  OR EXISTS(
                    SELECT
                        *
                    FROM
                        `users`
                    WHERE
                        `chats`.`client_id` = `users`.`id` AND (`name` LIKE "%'.$searchTerm.'%" OR `username` LIKE "%'.$searchTerm.'%")
                ) OR EXISTS(
                    SELECT
                        *
                    FROM
                        `users`
                    WHERE
                        `chats`.`uuid` = `users`.`id` AND (`name` LIKE "%'.$searchTerm.'%" OR `username` LIKE "%'.$searchTerm.'%")
                )
            )
                group by chats.channel)
              
            ')
           ->orderBy('id','Desc')
           ->get()->toArray();

            $discover= Chat::select(['chats.*','posts.latitude','posts.longitude','posts.tittle',
             DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( posts.latitude ) )
              * cos( radians(  posts.longitude ) - radians(' . $longitude . ') ) 
              + sin( radians(' . $latitude .') ) * sin( radians(posts.latitude) ) )
                AS DECIMAL(10,2)) AS distance')])
            ->join('posts', 'posts.id', '=', 'chats.post_id')->withCount('user')
            ->where('chats.deleteChannel',0)
            //->where('chats.join_status',0)
           ->whereRaw(
              'chats.id in (
                select max(chats.id) from chats where 
                deleteChannel=0 AND 
                (
                  (chats.type = "discover")
                )
              
                group by chats.channel)
              
            ');
             if(!empty($request->search)){
               $alldiscover = $discover->whereHas('Userdata', function ($query) use ($request){
                   $query->where('username', 'like', '%'.$request->search.'%');
                    })->orWhere('posts.tittle', 'LIKE', '%' . $request->search . '%')->with('Postdata.children')->orderByRaw('distance', 'ASC')->get()->toArray();


             }else{ 
              $alldiscover = $discover->with('Postdata.children')->orderByRaw('distance', 'ASC')->having("distance", "<=",10)->get()->toArray();
           }
           $data =array_merge($privateChat,$allData);
           
          // $unreadmsg1=[];
                      $channels=[];
          foreach($data as $value){

            $unread = ChatJoin::where(['channel'=>$value['channel'],'join_id'=>$request->user_id,'status'=>0])->pluck('join_id')->toArray();

            if(!empty($unread)){
              //$unreadmsg=Chat::where(['channel'=>$value['channel'],'deleteChannel'=>0,'join_status'=>0])->get()->toArray();
              $channel = $value['channel'];
              array_push($channels,$channel);
             // array_push($unreadmsg1,$unreadmsg);
            }

          }
         
         $unreadmsg=Chat::where(['deleteChannel'=>0,'join_status'=>0])->whereIn('channel',$channels)->get()->toArray();
         //print_r($unreadmsg); die;
        if(!empty($unreadmsg)){
          $sum=[];
          $sum2=[];
          $i=1;
          foreach($unreadmsg as $value){
            if(!empty($value['unreadmsg']) ){
              $array= (explode(",",$value['unreadmsg']) );
              
              if (in_array($request->user_id, $array))
              {
                      
              }
              else
              {

               // $sum1['index'] = $i;
                $sum1 = $value['channel'];
               // $location_id = $value['location_id'];
                array_push($sum,$sum1);
               //  array_push($sum2,$location_id);
              //$sum[] = $value['unreadmsg'];
              }
              //return json_encode(array('msg'=>'Data inserted!','status'=>true));
            }else{

             // $sum1['index'] = $i;
              $sum1 = $value['channel'];
             //  $location_id = 0;
              array_push($sum,$sum1);
              // array_push($sum2,$location_id);
            }
           
              $i++;
          }
         
        }else{
         $datamsg['channel'] = 0;
         $datamsg['unreadmsg_count']=0;
        }
      //print_r($sum2); die;
        $mainData2 =[];
        foreach($data as $value){
          $mainData3 = $value;
          if(!empty($sum)){
           if(in_array($value['channel'],$sum)){
            $d=array_intersect($sum,array($value['channel']));
               $mainData3['channel']=$value['channel'];
          $mainData3['unreadmsg_count']=count($d);
          if(!empty($value['location_id'])){ 
          $mainData3['location_id']=$value['location_id'][0];
        }
         //print_r($value['location_id']); die;
           }else{
            $mainData3['channel']=$value['channel'];
          $mainData3['unreadmsg_count']=0;
           }
          
          

        }else{
         $mainData3['channel'] =$value['channel'];
         $mainData3['unreadmsg_count']=0;
        }
         array_push($mainData2,$mainData3);


        }
         
       // print_r($mainData2);
        $data1 = JoinLocation::with('Postdata.children')->where(['user_id'=>$request->user_id,'status'=>0])->whereHas('Join_Userdata', function ($query) use ($searchTerm){
          $query->orWhere('username', 'like', '%'.$searchTerm.'%');
           })->where('location_name', 'like', '%'.$searchTerm.'%')->get()->toArray();

      // print_r($data1); die;
        $data2=array_merge($data1,$mainData2);
        $this->array_sort_by_column($data2, 'created_at');
        
          if(!empty($searchTerm)){    
            $ids=User::where('username', 'like', '%'.$searchTerm.'%')->pluck('id');
              $joinids  = ChatJoin::where('join_id',$request->user_id)->whereIn('uuid',$ids)->pluck('uuid');
              //print_r($joinids); die;
              $user=User::whereNotIn('id',$joinids)->where('username', 'like', '%'.$searchTerm.'%')->where('id','!=',$request->user_id)->get()->toArray();

              if(!empty($user)){ 
                $userData=[];
              foreach($user as $u){
                $u1 = $u;
                $u1['client_id'] = $u['id'];
                $u1['type'] = 'private';
                $u1['user_name'] = $u['name'];
                $u1['user_image'] = $u['image'];
                array_push($userData,$u1);
              }
               }else{$userData='';}
           }else{$userData='';}

           $support = SupportChat::where('client_id',$request->user_id)->orderBy('id','Desc')->first();
           if(!empty($support)){
           $supportChat = ['msg'=>$support['msg'],'date'=>$support['created_at']];
           }

        if($data2 || $userData || $support || $alldiscover){
        return json_encode(array('msg'=>'Data found!','data'=>(!empty($data2)) ? $data2 : 'Data not found','newuser'=>(!empty($userData)) ? $userData : '','support'=>(!empty($support)) ? $supportChat : '','discover'=>$alldiscover,'status'=>true));
        }else{
        return json_encode(array('msg'=>'Data not found.','status'=>false));
        }
      } catch (\Exception $e) {

        return json_encode(array('msg'=>$e->getMessage(),'status'=>false));
        //return $e->getMessage();
    }
   }


   public function notification(Request $request)
    {
      $validator = Validator::make($request->all(),[
      'user_id' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
          if($request->page  <= 1){
          $page = 0;
          }else {
          $page = $request->page - 1;
        }
        $limit = 10;
        $offset = ($page * $limit);
        $searchTerm = $request->search;
        try{ 
          Notification::where('user_id',$request->user_id)->update(['status'=>1]);
         $data = DB::table('notifications')
          ->selectRaw("notifications.*,posts.latitude as latitude,posts.longitude as longitude")
          ->join('posts', 'notifications.post_id', '=', 'posts.id')
          ->where('notifications.user_id',$request->user_id)
          ->where('notifications.status','!=',2)
           ->where(function($query) use ($searchTerm) {
              $query->Where('posts.user_name', 'LIKE', '%' . $searchTerm . '%')
              ->orWhere('posts.tittle', 'LIKE', '%' . $searchTerm . '%')
              ->orWhere('posts.description', 'LIKE', '%' . $searchTerm . '%');
            })->orderBy('notifications.created_at', 'desc')
           ->offset($offset)
            ->take($limit)
            ->get()
            ->toArray();
            if(!empty($data)){
             
              return json_encode(array('msg'=>'Data found','data'=>$data,'status'=>true));
           
            }else{
              return json_encode(array('msg'=>'Data not found','status'=>false));
            }
           } catch (\Exception $e) {

            return json_encode(array('msg'=>$e->getMessage(),'status'=>false));
            //return $e->getMessage();
        }
      
       }

    
        
    }

    public function notificationCount(Request $request){
      $validator = Validator::make($request->all(),[
      'user_id' => 'required',
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
      $noticount= Notification::where(['user_id'=>$request->user_id,'status'=>0])->count();
      return json_encode(array('msg'=>'Data found','data'=>$noticount,'status'=>true));
    }
    }*/



   /* public function support_chat(Request $request)
    {
       $request->validate([
              'user_id' => 'required',
              'user_name' => 'required',
              'user_email' => 'required',
              'channel' => 'required',
              'update' => 'required',
              //'type' => 'required',

                      
      ]);
       $data=[ 'client_id' => $request->user_id,
              //'client_name' => $request->user_name,
              'client_email' => $request->user_email,
              'channel' => $request->channel,
              'msg' => $request->update,
              'type' => 'user',
];
     $data1 = SupportChat::create($data);
      if($data1){
         if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Data inserted!','status'=>true));
        }else{
        	return json_encode(array('msg'=>'Información registrada con éxito','status'=>true));
        }
        
      }else{
         if($request->header()['lang'][0] == true){
        return json_encode(array('msg'=>'Data not inserted.','status'=>false));
        }else{
        	return json_encode(array('msg'=>'Información sin registrar','status'=>false));
        }
        
      }
 
      
    
  }

     public function notificationStatus(Request $request)
    {
       $request->validate([
              'user_id' => 'required',
              'type' => 'required',
              'posttype' => 'required',
        ]);

       if($request->posttype == 'public'){
           if($request->type == 0){
				 $data1 = Chat::where('uuid',$request->user_id)->where('type','public')->update(['status'=>2]);
				User::where('id',$request->user_id)->update(['status'=>2]);
				 if($request->header()['lang'][0] == true){
				 return json_encode(array('msg'=>'Notifications Off','status'=>true));
				}else{
					return json_encode(array('msg'=>'Notificaciones Apagadas','status'=>true));
				}
		    }else{
				 $data1 = Chat::where('uuid',$request->user_id)->where('type','public')->update(['status'=>0]);
				User::where('id',$request->user_id)->update(['status'=>0]);
				if($request->header()['lang'][0] == true){
				return json_encode(array('msg'=>'Notifications On','status'=>true));
				}else{
				return json_encode(array('msg'=>'Notificaciones Encendidas','status'=>true));
				}
			}


       }elseif($request->posttype == 'private'){
            if($request->type == 0){
				 $data1 = Chat::where('uuid',$request->user_id)->where('type','private')->update(['status'=>2]);
				User::where('id',$request->user_id)->update(['private_notification'=>2]);
				 if($request->header()['lang'][0] == true){
				 return json_encode(array('msg'=>'Notifications Off','status'=>true));
				}else{
					return json_encode(array('msg'=>'Notificaciones Apagadas','status'=>true));
				}
		    }else{
				 $data1 = Chat::where('uuid',$request->user_id)->where('type','private')->update(['status'=>0]);
				User::where('id',$request->user_id)->update(['private_notification'=>0]);
				if($request->header()['lang'][0] == true){
				return json_encode(array('msg'=>'Notifications On','status'=>true));
				}else{
				return json_encode(array('msg'=>'Notificaciones Encendidas','status'=>true));
				}
			}


       }else{
             if($request->type == 0){
				 $data1 = Chat::where('uuid',$request->user_id)->update(['status'=>2]);
				User::where('id',$request->user_id)->update(['other_notification'=>2]);
				 if($request->header()['lang'][0] == true){
				 return json_encode(array('msg'=>'Notifications Off','status'=>true));
				}else{
					return json_encode(array('msg'=>'Notificaciones Apagadas','status'=>true));
				}
		    }else{
				 $data1 = Chat::where('uuid',$request->user_id)->update(['status'=>0]);
				User::where('id',$request->user_id)->update(['other_notification'=>0]);
				if($request->header()['lang'][0] == true){
				return json_encode(array('msg'=>'Notifications On','status'=>true));
				}else{
				return json_encode(array('msg'=>'Notificaciones Encendidas','status'=>true));
				}
			}


       }

      
    }

  public function get_notificationStatus(Request $request)
    {
       $request->validate([
              'user_id' => 'required',
              
        ]);
       
        $data1 = User::where('id',$request->user_id)->first();
        if(!empty($data1)){ 
         if($data1->status==2){
         	$status=true;
         }else{
         	$status=false;
         }
          if($data1->private_notification==2){
         	$statusprivate=true;
         }else{
         	$statusprivate=false;
         }

          if($data1->other_notification==2){
         	$other_notification=true;
         }else{
         	$other_notification=false;
         }

        return json_encode(array('msg'=>'Notification Off','public'=>$status,'private'=>$statusprivate,'other'=>$other_notification,'status'=>true));
        }else{
         return json_encode(array('msg'=>'Notification On','Data'=>$data1[0],'status'=>false));
        }
    }*/

  
    /* public function blockUnblock(Request $request)
    {
      
       $request->validate([
              'user_id' => 'required',
              'channel' => 'required',
        ]);
        $data = Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->pluck('status')->take(1)->toArray();
  
       if(!empty($data)){
       if($data[0] == 0){
        $update= Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->update(['status'=>1]);
        //print_r($update); die;
         return json_encode(array('msg'=>'User blocked!','status'=>true));
       }else{
          Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->update(['status'=>0]);
      return json_encode(array('msg'=>'User unblocked!','status'=>true));
       }
     }else{
       return json_encode(array('msg'=>'Data not found!','status'=>false));
     }
    }
  
   public function deleteChannel(Request $request){
       // return json_encode(array('msg'=>$request->user_id,'status'=>true)); die;
       $request->validate([
              'user_id' => 'required',
              'channel' => 'required',
              'location_id'=>'nullable',
              'type'=>'nullable',
        ]);
      if(!empty($request->location_id)){
        if(!empty($request->type) && $request->type == 'discover'){
        $userId= joinLocation::where(['post_id'=>$request->location_id])->pluck('user_id')->first();
       
        if($userId == $request->user_id){ 
          $update1= Post::where(['id'=>$request->location_id])->update(['status'=>1]);
        
          $update2= Post::where(['location_id'=>$request->location_id])->update(['status'=>1]);
          $data = Chat::where(['deleteChannel'=>0,'post_id'=>$request->location_id])->update(['deleteChannel'=>1]);
          $updatee= joinLocation::where(['post_id'=>$request->location_id])->update(['status'=>1]);
         
          $update5= Post::where(['location_id'=>$request->location_id])->get()->toArray();
          foreach($update5 as $key =>$value){
           $data = Chat::where(['deleteChannel'=>0,'post_id'=>$value['id']])->update(['deleteChannel'=>1]);
          ChatJoin::where(['status'=>0,'post_id'=>$value['id']])->update(['status'=>1]); 
            $update3= joinLocation::where(['post_id'=>$value['id']])->update(['status'=>1]);
          }
        return json_encode(array('msg'=>'Channel deleted!!','status'=>true));
       }else{
        return json_encode(array('msg'=>'Sorry! You are not creator of channel','status'=>false));
       }
        }else{
          //$updatechat= Chat::where(['post_id'=>$request->location_id])->update(['status'=>1]);
       
          $update= joinLocation::where(['id'=>$request->location_id])->update(['status'=>1]);
        return json_encode(array('msg'=>'Channel deleted!!','status'=>true));
        }
         
       }else{
          $data = Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel,'deleteChannel'=>0])->count();
          // print_r($data); die;
         if(!empty($data)){
         if($data > 0){
          $update= Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->update(['deleteChannel'=>1]);
          return json_encode(array('msg'=>'Channel deleted!','status'=>true));
         }
        }else{
         return json_encode(array('msg'=>'Data not found!','status'=>false));
       
       }
     }
  }


  public function muteUnmute(Request $request)
    {
       $request->validate([
              'user_id' => 'required',
              'channel' => 'required',
              'mute_unmute' => 'required',
        ]);

       if($request->mute_unmute == true){

         $data = Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->pluck('muteUnmute')->take(1)->toArray();
          $data1 = Chat::where(['uuid'=>$request->user_id])->pluck('status')->take(1)->toArray();
             if(!empty($data)){
       if($data[0] == 0){
          return json_encode(array('msg'=>'Chat umute!','status'=>false,'notification_status'=>$data1[0] ,'chatMute'=>false));
       }else{
          return json_encode(array('msg'=>'Chat mute!!','status'=>true,'notification_status'=>$data1[0],'chatMute'=>true));
       }
     }else{
       return json_encode(array('msg'=>'Data not found!','status'=>false));
     }


       }else{
        $data = Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->pluck('muteUnmute')->take(1)->toArray();
        $data1 = Chat::where(['uuid'=>$request->user_id])->pluck('status')->take(1)->toArray();
       if(!empty($data)){
       if($data[0] == 0){
        $update= Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->update(['muteUnmute'=>1]);
          return json_encode(array('msg'=>'Chat mute!','notification_status'=>$data1[0] ,'status'=>true));
       }else{
          Chat::where(['uuid'=>$request->user_id,'channel'=>$request->channel])->update(['muteUnmute'=>0]);
      return json_encode(array('msg'=>'Chat unmute!!','notification_status'=>$data1[0] ,'status'=>false));
       }
     }else{
       return json_encode(array('msg'=>'Data not found!','status'=>false));
     }
   }
    }

    public function storechannelPost(Request $request)
    {

       $validator = Validator::make($request->all(),[
      'post' => 'required',
      'tittle' => 'required',
      'latitude' => 'required',
      'longitude' => 'required',
      'description' => 'required',
      'post_type'  => 'required',
      //'image' => 'required|mimes:jpeg,jpg,png',
       'image' => 'nullable',
      'user_image' => 'nullable',
      'user_name' => 'nullable',
     // 'user_last_name' => 'required',
      'user_id' => 'required',
      'type' => 'required',
     
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{ 
        $data=$request->all();
        
       if(!empty($request->image)){ 
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('upload/image'), $imageName);
        }else{
          $imageName='';
        }
        $userData=Post::where('id',$request->user_id)->get()->toArray();
      
            
       $data['image'] = $imageName;
       $data['type'] = 'Discover';
       $getId=Post::create($data);
       $data1=[ 'post_id' => $getId->id,
                'join_id' => $request->user_id,
                'uuid' => $request->user_id,
               'channel' => $getId->id,
         ];
         ChatJoin::create($data1);
         Chat::create(['channel'=>$getId->id,'uuid'=>$request->user_id,'client_id'=>$request->user_id,'post_id'=>$getId->id,'type'=>'Discover','join_status'=>1,'status'=>0]);
         $data1=['user_id'=>$request->user_id,'location_name'=>$request->tittle,'latitude'=>$request->latitude,'longitude'=>$request->longitude,'join'=>0,'image'=>$request->imageName,'type'=>1,'post_id'=>$getId->id,'location_type'=>1];
                      $storeData= JoinLocation::create($data1);
    $userData1 = User::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $request->latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $request->longitude . ') ) + sin( radians(' . $request->latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->where('id','!=',$request->user_id)->where(['other_notification'=>0])->having( 'distance', '<=', 10 )->get()->toArray();
            
 
           $ids=[];
        foreach($userData1 as $value){
      
               array_push($ids,$value['id']);
              $notifydata=['title'=>$request->tittle,'description'=>$request->description,'user_name'=>$value['name'],'user_email'=>'','user_id'=>$value['id'],'post_id'=>$getId->id,'type'=>'user'];
            Notification::create($notifydata);  
          }

          $notifydata=['pn_gcm'=>['data'=>['text'=>$request->tittle,'message'=>$request->description,'uuids'=>$ids]]];
           $result = $this->pubnub->publish()
              ->channel('notifications')
              ->message($notifydata)
              ->sync();
       //print_r($getId->id); die;
       $data1=['post_id'=>$getId->id,'client_id'=>$request->user_id,'uuid'=>$request->user_id,'post_name'=>$request->tittle,'channel'=>$getId->id,'image'=>$imageName,'type'=>'Discover'];
       Chat::create($data1);
        // $url= str_replace("/api/auth/storeNeeded","/share_page",\Request::fullUrl());
         
           $needyData=[ 'url'=>  'https://onlineprojectprogress.com/laravel/qui/share_page/'.$getId->id ];
         
          return json_encode(array('msg'=>'Your post has been created successfully.','status'=>true));
          
     } 
    }

    public function unreadmsg(Request $request)
    {
      //return json_encode(array('msg'=>'1','status'=>true)); die;
       $request->validate([
              'user_id' => 'required',
              'channel' => 'nullable',
              'post_id'  => 'required',
              'type'    => 'nullable',
        ]);
       
         $unread = ChatJoin::where(['channel'=>$request->channel,'join_id'=>$request->user_id])->pluck('join_id')->toArray();
         


         $post = Post::find($request->post_id);
         if(!empty($request->type) && $request->type == 'group'){
             JoinLocation::where(['id'=>$request->post_id,'user_id'=>$request->user_id,'status'=>0])->update(['unreadmsg_count'=>0]);
           }
    
       if(!empty($unread)){
       $unreadmsg = Chat::where(['channel'=>$request->channel,'join_status'=>0])->get()->toArray();
     //print_R($unreadmsg); die;
       $update=[];
      foreach($unreadmsg as $value){
        if(!empty($value['unreadmsg']) ){
         $array= (explode(",",$value['unreadmsg']) );
          //print_R($array); die;
       if (in_array($request->user_id, $array))
        {
        
        }
      else
        {
        $data = Chat::where(['id'=>$value['id'],'join_status'=>0])->pluck('unreadmsg')->first();
       
        if(!empty($data)){
        $ok=Chat::where(['id'=>$value['id']])->update(['unreadmsg'=> $data.$request->user_id.',']);
        array_push($update,$ok);
         
        }else{
          //Chat::where(['id'=>$value['id']])->update(['unreadmsg'=>$request->user_id.',']);
        }
         
        }
        //return json_encode(array('msg'=>'Data inserted!','status'=>true));
      }else{
        Chat::where(['id'=>$value['id']])->update(['unreadmsg'=>$request->user_id.',']);
        
      }
       }

       if(!empty($post)){

    
         $location_id = $post->location_id;
         if(!empty($post->location_id)){
        // $joinData=JoinLocation::where('post_id',$location_id)->where('user_id',$request->user_id)->first();
        //  JoinLocation::where('id',$joinData->id)->where('user_id',$request->user_id)->update(['unreadmsg_count'=>0]); 
       
            }else{
              JoinLocation::where('id',$joinData->id)->where('user_id',$request->user_id)->update(['unreadmsg_count'=>0]); 
            }
          }
        
       
       if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Data inserted!','status'=>true));
        }else{
        return json_encode(array('msg'=>'Información registrada con éxito','status'=>true));
        }
       
      }
       
    else{
       return json_encode(array('msg'=>'User not join!','status'=>false));
     }
    }

     public function onlinecount(Request $request)
  {
  	try{ 
         $request->validate([
             // 'uuid' => 'nullable',
              'channel' => 'required',
              'user_id' => 'required',
              'post_id' => 'required',
              'user_status' => 'required',
        ]);

       
       $data = ChatJoin::where(['channel'=>$request->channel,'join_id'=>$request->user_id,'post_id'=>$request->post_id,'status'=>0])->get()->toArray();
      // print_r($data); die;
       if(!empty($data)){
        ChatJoin::where(['channel'=>$request->channel,'join_id'=>$request->user_id,'post_id'=>$request->post_id,'status'=>0])->update([ 'user_status' => $request->user_status]);
         $userStatus = ChatJoin::where(['channel'=>$request->channel,'post_id'=>$request->post_id,'user_status'=>true,'status'=>0])->get()->count();
          $userCount = ChatJoin::where(['channel'=>$request->channel,'post_id'=>$request->post_id,'status'=>0])->get()->count();
     return json_encode(array('msg'=>'Joined!','status'=>true,'userStatus'=>$userStatus,'userCount'=>$userCount));
     }else{
       return json_encode(array('msg'=>'Data not found!','status'=>false,'checkJoin'=>false));
     }
      } catch (\Exception $e) {

       return json_encode(array('msg'=>$e->getMessage(),'status'=>false));
            //return $e->getMessage();
     }
      
  }

    public function chatJoin(Request $request)
    {
       
       $request->validate([
              'uuid' => 'nullable',
              'channel' => 'required',
              'join_id' => 'required',
              'post_id' => 'required',
              'checkJoin' => 'nullable',
        ]);
       if($request->checkJoin == true){
          $data = ChatJoin::where(['channel'=>$request->channel,'join_id'=>$request->join_id,'post_id'=>$request->post_id])->get()->toArray();
      
       if(!empty($data)){
      return json_encode(array('msg'=>'Joined!','status'=>true,'checkJoin'=>true));
     }else{
       return json_encode(array('msg'=>'Data not found!','status'=>false,'checkJoin'=>false));
     }

       }else{
            
        $data = ChatJoin::where(['channel'=>$request->channel,'join_id'=>$request->join_id,'post_id'=>$request->post_id])->get()->toArray();
      
       if(!empty($data)){
      return json_encode(array('msg'=>'Joined!','status'=>true));
     }else{
       $data1=[ 'post_id' => $request->post_id,
                'join_id' => $request->join_id,
                'uuid' => $request->uuid,
               'channel' => $request->channel,
         ];
         ChatJoin::create($data1);
         $postid= Post::find($request->post_id);
         if(!empty($postid)){
           // return json_encode(array('msg'=>'not empty','status'=>true)); die;
            $userData=User::find($request->uuid);
          Chat::create(['channel'=>$request->channel,'uuid'=>$request->join_id,'client_id'=>$request->uuid,'post_id'=>$request->post_id,'post_name'=>$postid->tittle,'type'=>$postid->post_type == 0 ? 'public' : 'private','join_status'=>1,'status'=>0]);
       
        }else{
            //return json_encode(array('msg'=>'empty','status'=>true)); die; 
          $userData=User::find($request->uuid);
        // Chat::create(['channel'=>$request->channel,'uuid'=>$request->join_id,'post_id'=>0,'client_id'=>$request->uuid,'post_name'=>$userData->name,'name'=>$userData->name,'type'=>'private','usertype'=>'usertypeChat','user_username'=>$userData->username,'join_status'=>1,'status'=>0]);
      Chat::create(['channel'=>$request->channel,'uuid'=>$request->join_id,'post_id'=>0,'client_id'=>$request->uuid,'post_name'=>$userData->name,'type'=>'private','usertype'=>'usertypeChat','join_status'=>1,'status'=>0]);
  
     // return json_encode(array('msg'=>'empty','status'=>true)); die;
        }
       return json_encode(array('msg'=>'Joined!','status'=>true));
     }
   }
    }

  public function chatleave(Request $request)
    {
       $request->validate([
              'uuid' => 'nullable',
              'channel' => 'required',
              'join_id' => 'required',
              'post_id' => 'required',
        ]);
        $data = ChatJoin::where(['channel'=>$request->channel,'join_id'=>$request->join_id,'post_id'=>$request->post_id])->get()->toArray();
      
       if(!empty($data)){
       	 ChatJoin::where('id',$data[0]['id'])->update(['status'=>1]);
      return json_encode(array('msg'=>'User leave!','status'=>true));
     }else{
       
       return json_encode(array('msg'=>'User id not exist!','status'=>false));
     }
    }*/

    /* public function notifydelete(Request $request){ 
      $request->validate([
              'channel' => 'required',
            
              
        ]);
          try{ 
              $id= Notification::where('id',$request->channel)->delete();
              
            if($id == 1){ 
                   return json_encode(array('msg'=>'Notification has deleted!','status'=>true));
                 }else{
                   return json_encode(array('msg'=>'Someting went wrong','status'=>false));
                 }
           }catch(\PubNub\Exceptions\PubNubServerException $e) {
        return json_encode(array('msg'=>'Notification not delete','timetoken'=>$e->getMessage(),'status'=>false));
         
      }       
    }*/

   
    
  /*  function location_search($lat, $lang, $location_name,$user_id) {
      try{ 
      $location=JoinLocation::Where(['latitude'=>$lat,'longitude'=>$lang,'location_name'=>$location_name,'location_type'=>0,'post_id'=>0])->first();
      $dis=JoinLocation::Where(['latitude'=>$lat,'longitude'=>$lang,'location_name'=>$location_name,'location_type'=>1])->first();
     
      if(!empty($location)){ 
      //return json_encode(array('msg'=>'Data found','status'=>true,'location_id'=>$location->id,'location_name'=>$location->location_name));
       return $data1=['location_id'=>$location->id,'location_name'=>$location->location_name,'post_id'=>$location->post_id,'location_type'=>$location->location_type];
     }else{
      if(empty($dis)){
      $data=JoinLocation::create(['latitude'=>$lat,'longitude'=>$lang,'location_name'=>$location_name,'user_id'=>$user_id]);
       return $data1=['location_id'=>$data->id,'location_name'=>$data->location_name,'post_id'=>$location->post_id,'location_type'=>$location->location_type];
    }
      // return json_encode(array('msg'=>'Data found','location_id'=>$data->id,'location_name'=>$data->location_name,'status'=>true));
     }
    
     }catch(\Exception $e){
      return json_encode(array('msg'=>$e->getMessage(),'status'=>false));
     }
    }


    function array_sort_by_column(&$array, $column, $direction = SORT_DESC) {
      $reference_array = array();
      foreach($array as $key => $row) {
        $reference_array[$key] = $row[$column];
      }
      
      array_multisort($reference_array, $direction, $array);
    }*/

/*         public function change_reaction(Request $request)
    {
       $request->validate([
              //'id' => 'nullable',
              'user_id' => 'required',
              'post_id' => 'required',
              'status' => 'required',
              'post_type' => 'required',
      ]);
   
      $category = UserReaction::where(['user_id'=>$request->user_id,'post_id'=>$request->post_id])->get()->toArray();
     // print_r($category); die;
      if(!empty($category)){
         $status = UserReaction::where(['user_id'=>$request->user_id,'post_id'=>$request->post_id,'status'=>$request->status])->get()->toArray();
         if(!empty($status)){ 
        
         $category = UserReaction::where(['user_id'=>$request->user_id,'post_id'=>$request->post_id,'status'=>$request->status])->update(['status'=>0]);
                     
        }else{

           UserReaction::where(['user_id'=>$request->user_id,'post_id'=>$request->post_id])->update(['status'=>$request->status]);
        }
          $data['totalLike'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type])->where('status','!=',0)->count('post_id');
             $data['totalDislike'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type])->where('status',0)->count('post_id');
          $data['userLike'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'user_id'=>$request->user_id])->where('status','!=',0)->count('post_id');
           $data['smile'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>1])->count('post_id');
        $data['heart'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>2])->count('post_id');
        $data['Laugh'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>3])->count('post_id');
        $data['shoke'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>4])->count('post_id');
        $data['sad'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>5])->count('post_id');
        $data['angry'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>6])->count('post_id');
           $data['status'] =$request->status;
           $user=User::where('id',$request->user_id)->get()->toArray();
           $data['user'] =$user[0]; 
        if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'User reaction has changed','data'=>$data,'status'=>true,'likeStatus'=>false));
        }else{
        	return json_encode(array('msg'=>'Has cambiado la Reacción','data'=>$data,'status'=>true,'likeStatus'=>false));
        }
        
      }else{
        
       $category = UserReaction::create(['user_id'=>$request->user_id,'post_id'=>$request->post_id,'status'=>$request->status,'post_type'=>$request->post_type]);
            $data['totalLike'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type])->where('status','!=',0)->count('post_id');
             $data['totalDislike'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type])->where('status',0)->count('post_id');
          $data['userLike'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'user_id'=>$request->user_id])->where('status','!=',0)->count('post_id');
           $data['smile'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>1])->count('post_id');
        $data['heart'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>2])->count('post_id');
        $data['Laugh'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>3])->count('post_id');
        $data['shoke'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>4])->count('post_id');
        $data['sad'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>5])->count('post_id');
        $data['angry'] = UserReaction::where(['post_id'=>$request->post_id,'post_type'=>$request->post_type,'status'=>6])->count('post_id');
           $data['status'] =$request->status;
            $user=User::where('id',$request->user_id)->first();
            $postData=Post::where('id',$request->post_id)->first();
            $usernotify=User::where(['id'=>$postData->user_id,'other_notification'=>0])->first();
            if(!empty($usernotify)){
            $notificationData=['title'=>'Reacted on'. $postData->tittle.' post.','description'=>$user->name.' reacted on your post.','user_name'=>$user->name,'user_email'=>'','user_id'=>$postData->user_id,'post_id'=>$request->post_id,'type'=>'user'];
            Notification::create($notificationData); 
		    $notifydata=['pn_gcm'=>['data'=>['text'=>'Hi','message'=>$user->name.' reacred on your post.','uuids'=>[$postData->user_id]]]];
		    $notifydataIos=['pn_apns'=>['data'=>['text'=>'Hi','message'=>$user->name.' reacred on your post.','uuids'=>[$postData->user_id]]]];
		  
		      $result = $this->pubnub->publish()
              ->channel('notify')
              ->message($notifydata)
              ->sync();
              }
            
           $data['user'] =$user; 
       if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'User reaction has submitted','status'=>true,'data'=>$data,'likeStatus'=>false));
        }else{
        	return json_encode(array('msg'=>'Has agregado una Reacción','status'=>true,'data'=>$data,'likeStatus'=>false));
        }
        
      }
  
}*/

    



   
}
