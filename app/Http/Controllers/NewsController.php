<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Notifications\forgotPassword;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Model\passwordReset;
use App\User;
use App\Admin;
use App\Model\UserReaction;
use App\Model\Report;
use App\Model\Notification;
use App\Model\JoinLocation;

use Illuminate\Support\Facades\Validator;
use Session;
use App\Model\Category;
use App\Model\Feedback;
use App\Model\Favourite_geolocation;
use App\Model\Post;
use App\Model\ChatJoin;
use App\Model\SupportChat;
use App\Model\Chat;
use DB;
use App\Model\Help;
use App\Channel\GetPublichannel;
use PubNub\PubNub;
use PubNub\Enums\PNStatusCategory;
use PubNub\Callbacks\SubscribeCallback;
use PubNub\PNConfiguration;
use PubNub\Exceptions\PubNubException;
use Monolog\Handler\ErrorLogHandler;

class NewsController extends Controller
{

    protected $pubnub;
    protected $pnconf;


    public function __construct()
    {
      //$this->middleware('api_token');
        $this->pnconf = new PNConfiguration();
        $this->pnconf->setSubscribeKey(env('SubscribeKey'));
        $this->pnconf->setPublishKey(env('PublishKey'));
        $this->pnconf->setSecretKey(env('SecretKey'));
        $this->pnconf->setSecure(false);
        $this->pubnub = new PubNub($this->pnconf);
        //$this->pubnub->ssl => true;
    }
   public function news(Request $request)
    {
      $validator = Validator::make($request->all(),[
      'latitude' => 'required',
      'longitude' => 'required',
      'user_id' => 'required',
      'type' => 'nullable',
      'location_id' => 'nullable',
      'location_name' => 'nullable',

      ]);
      if($validator->fails()){
           return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
            $latitude = $request->latitude;
            $longitude = $request->longitude;
               if($request->page  <= 1){
                  $page = 0;
                  }else {
                  $page = $request->page - 1;
                }
                $limit = 5;
                $offset = ($page * $limit);
                $searchTerm = $request->search;
                if($request->type == 'discover'){ 
                    $Business = Post::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->Where('post','Discover')->where('status',0)->where('type','Discover')->where('latitude',$request->latitude)->where('longitude',$request->longitude)->where('id',$request->location_id)
                    ->with('children.userData')->where(function($query) use ($searchTerm) {
                    $query->orWhere('post', 'LIKE', '%' . $searchTerm . '%')
                    ->orWhere('tittle', 'LIKE', '%' . $searchTerm . '%');
                    })->with('userData')->orderBy('created_at','Desc')->withCount('children')
                    ->offset($offset)
                    ->take($limit)
                    ->get()
                    ->toArray();
               
                 }elseif($request->type == 'sub'){ 
                    $locationData=$this->location_search($request->latitude, $request->longitude, $request->location_name,$request->user_id);

                    $Business = Post::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->where('post','News')->where('status',0)->where('location_id',$locationData['location_id'])->where('latitude',$request->latitude)->where('longitude',$request->longitude)
                    ->where(function($query) use ($searchTerm) {
                    $query->orWhere('post', 'LIKE', '%' . $searchTerm . '%')
                    ->orWhere('tittle', 'LIKE', '%' . $searchTerm . '%');
                    })->with('userData')->orderBy('created_at','Desc')
                    ->offset($offset)
                    ->take($limit)
                    ->get()
                    ->toArray();
                 
                }else{ 
                    $Business = Post::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')])->where('post','News')->where('status',0)->where('type','main')
                    ->where(function($query) use ($searchTerm) {
                    $query->orWhere('post', 'LIKE', '%' . $searchTerm . '%')
                    ->orWhere('tittle', 'LIKE', '%' . $searchTerm . '%');
                    })->with('userData')->orderBy('created_at','Desc')
                    ->offset($offset)
                    ->take($limit)
                    ->get()
                    ->toArray();
              
              }
               if(!empty($Business)){ 
                  $img=asset('/upload/image/');
                  $url= asset('/share_page/');
                  $array=[];
                  foreach($Business as $value){
                    $data = $value;
                    $report=Report::where(['userid'=>$request->user_id,'post_id'=>$value['id'],'status'=>0])->get()->toArray();
                    if(!empty($report)){

                    }else{ 
                      $data['totalLike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type']])->where('status','!=',0)->count('post_id');
                      $data['dislike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>0])->count('post_id');
                      $data['smile'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>1])->count('post_id');
                      $data['heart'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>2])->count('post_id');
                      $data['Laugh'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>3])->count('post_id');
                      $data['shoke'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>4])->count('post_id');
                      $data['sad'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>5])->count('post_id');
                      $data['angry'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'status'=>6])->count('post_id');
                      $data['totalDislike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type']])->where('status',0)->count('post_id');
                      $data['userLike'] = UserReaction::where(['post_id'=>$value['id'],'post_type'=>$value['post_type'],'user_id'=>$request->user_id])->where('status','!=',0)->count('post_id');

                      if($data['totalLike'] == 0){
                         $data['likeStatus'] = false;
                       }else{
                         $data['likeStatus'] = false;
                      }

                     $data['url']= $url.'/'.$value['id'] ;
                  }
                     array_push($array,$data);
                 }
                if($request->header()['lang'][0] == true){
                   return json_encode(array('msg'=>'data found.','data'=>$array,'status'=>true));
                }else{
                    return json_encode(array('msg'=>'Información válida','data'=>$array,'status'=>true));
                }
            
              }else{
                 if($request->header()['lang'][0] == true){
                    return json_encode(array('msg'=>'data not available','status'=>false));
                  }else{
                     return json_encode(array('msg'=>'Información no válida','status'=>false));
                 }
              
          }
        } 
       
    }

    public function storeNews(Request $request){
     // return json_encode(array('msg'=>$request->all(),'status'=>true)); die;
      $validator = Validator::make($request->all(),[
        'post' => 'required',
        'tittle' => 'required',
        'latitude' => 'required',
        'longitude' => 'required',
        'description' => 'required',
        'post_type'  => 'required',
        'image' => 'nullable',
        'file' => 'nullable',
        'user_image' => 'nullable',
        'user_name' => 'nullable',
        // 'user_last_name' => 'required',
        'user_id' => 'required',
        'post_category' => 'nullable',
        'address' => 'nullable',
        'address_second' => 'nullable',
        'type' => 'nullable',
        'location_id' => 'nullable',
        'location_name' => 'required',
        'user_latitude' => 'nullable',
        'user_longitude' => 'nullable',
        'file_type' => 'nullable',
      ]);

      if($validator->fails()){
           return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
          $locationData=$this->location_search($request->latitude, $request->longitude, $request->location_name,$request->user_id);
          $data=$request->all();
          if($request->type != 'sub_discover'){
             $data['location_id']=$locationData['location_id'];
          }
     
          if(!empty($request->image)){ 
              $imageName = time().'.'.request()->image->getClientOriginalExtension();
              request()->image->move(public_path('upload/image'), $imageName);
          }else if(!empty($request->file)){ 
              $imageName = time().'.'.request()->file->getClientOriginalExtension();
              request()->file->move(public_path('upload/image'), $imageName);
            
          }else{
			  $imageName='';
		  }
			  
			  
              $userData=User::where('id',$request->user_id)->where(['status'=>0])->get()->toArray();
              $data['image'] = $imageName;
              $getId=Post::create($data);
              $data1=[ 'post_id' => $getId->id,
                    'join_id' => $request->user_id,
                    'uuid' => $request->user_id,
                   'channel' => $getId->id,
              ];
               ChatJoin::create($data1);
               Chat::create(['channel'=>$getId->id,'uuid'=>$request->user_id,'client_id'=>$request->user_id,'post_id'=>$getId->id,'type'=>$request->post_type == 0 ? 'public' : 'private','join_status'=>1,'status'=>0]);
         
            if(!empty($request->type) &&  $request->type == 'sub_discover'){ 
                $joindata=JoinLocation::where(['post_id'=>$request->location_id,'status'=>0])->where('user_id','!=',$request->user_id)->get()->toArray();
                foreach($joindata as $key=>$value){
                    if(empty($value['unreadmsg_count']) || $value['unreadmsg_count'] == 0){
                       JoinLocation::where(['id'=>$value['id']])->update(['unreadmsg_count'=>1]);
                    }else{
                      $msgCount= $value['unreadmsg_count'] + 1;
                      JoinLocation::where(['id'=>$value['id']])->update(['unreadmsg_count'=>$msgCount]);
                    }
                }
            }else{
              $joindata1=JoinLocation::where(['post_id'=>0,'latitude'=>$request->latitude,'longitude'=>$request->longitude,'status'=>0])->where('user_id','!=',$request->user_id)->get()->toArray();
              foreach($joindata1 as $key=>$value){
                  if(empty($value['unreadmsg_count']) || $value['unreadmsg_count'] == 0){
                     JoinLocation::where(['id'=>$value['id']])->update(['unreadmsg_count'=>1]);
                  }else{
                      $msgCount= $value['unreadmsg_count'] + 1;
                      JoinLocation::where(['id'=>$value['id']])->update(['unreadmsg_count'=>$msgCount]);
                  }
              }
           }
            $userData1 = User::select(['*', DB::RAW('CAST( 6371 * acos( cos( radians(' . $request->latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $request->longitude . ') ) + sin( radians(' . $request->latitude .') ) * sin( radians(latitude) ) )  AS DECIMAL(10,2)) AS distance')]);
             if($request->post_category == 'help' || $request->post_category == 'warning'){
              $userData2 = $userData1->where(['other_notification'=>0]);
             }elseif($request->post_type == 1){
               $userData2 = $userData1->where(['private_notification'=>0]);
             }else{
               $userData2 = $userData1->where('status','!=',2);
             }
            $userData =$userData2->having( 'distance', '<=', 10 )->groupBy('id')->get()->toArray(); 
            $ids=[];
            foreach($userData as $value){
                if($value['id'] != $request->user_id){
              $notifydata=['title'=>$request->tittle,'description'=>$request->description,'user_name'=>$value['name'],'user_email'=>'','user_id'=>$value['id'],'post_id'=>$getId->id,'type'=>'user'];
              Notification::create($notifydata); 
              if($value['id'] != $request->user_id){
                array_push($ids,$value['id']); 
              } 
            }
            }
            $notifydata=['pn_gcm'=>['data'=>['popular'=>$request->tittle,'message'=>$request->description,'uuids'=>$ids]]];
            $result = $this->pubnub->publish()
            ->channel('notify')
            ->message($notifydata)
            ->sync();
            $needyData=[ 'url'=> asset('/share_page/').$getId->id ];
            if($request->header()['lang'][0] == true){
               return json_encode(array('msg'=>'Your post has been created','data'=>$needyData,'status'=>true,'ids'=>$ids));
            }else{
                return json_encode(array('msg'=>'Se ha creado la publicación','data'=>$needyData,'status'=>true));
            }
        }
    }

     public function updateNews(Request $request){
      $validator = Validator::make($request->all(),[
        'id' => 'required',
        'tittle' => 'nullable',
        'latitude' => 'nullable',
        'longitude' => 'nullable',
        'description' => 'nullable',
        'post_type'  => 'nullable',
        'image' => 'nullable',
     
      ]);
      if($validator->fails()){
        return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
        }else{
        if(!empty($request->image)){
          $validator = Validator::make($request->all(),[
          'image' => 'mimes:jpeg,jpg,png',
          ]);
          if($validator->fails()){
            return json_encode(array('msg'=>$validator->errors()->first(),'status'=>false));
            }else{
            $host = $request->getHttpHost();
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('upload/image'), $imageName);
           
          }
        }
        $data = Post::where('id', $request->id)->get()->toArray();
        if(!empty($data)){
          $User = Post::where('id', $request->id)->update([
              'tittle' => ($request->tittle)? $request->tittle: $data[0]['tittle'],
              'latitude' => ($request->latitude)? $request->latitude: $data[0]['latitude'],
              'longitude' => ($request->longitude)? $request->longitude: $data[0]['longitude'],
              'description' => ($request->description)? $request->description: $data[0]['description'],
              'post_type' => ($request->post_type)? $request->post_type: $data[0]['post_type'],
              'image' => ($request->image)? $imageName: $data[0]['image'],
          ]);
          $updatedata = Post::where('id', $request->id)->get()->toArray();
          if(!empty($updatedata[0]['image'])) { 
             $image1 = explode("://",$updatedata[0]['image']);
            if($image1[0] == 'https'|| $image1[0] == 'http') { 
                 $updatedata[0]['image'] = $updatedata[0]['image'];
             }else{
               $updatedata[0]['image'] =asset('/upload/image/').$updatedata[0]['image'];
             }
              
         
           }else{
             $updatedata[0]['image'] = null;
         }
         if($request->header()['lang'][0] == true){
          return json_encode(array('msg'=>'Your post has been updated','data'=>$updatedata[0],'status'=>true));
        }else{
         return json_encode(array('msg'=>'La publicación se ha actualizado','data'=>$updatedata[0],'status'=>true));
        }
         
          }else{
            if($request->header()['lang'][0] == true){
         return json_encode(array('msg'=>'Post does not exist.','status'=>false));
        }else{
        return json_encode(array('msg'=>'La publicación no existe','status'=>false));
        }
          
        }
      }
    }

     public function deleteNews(Request $request)
    {
       $request->validate([
        'id' => 'required',
       ]);
         $data=Post::where('id',$request->id)->delete();
       if($data){
          if($request->header()['lang'][0] == true){
            return json_encode(array('msg'=>'Post has been deleted','status'=>true));
          }else{
             return json_encode(array('msg'=>'La publicación se ha eliminado','status'=>true));
          }
        
      }else{
        if($request->header()['lang'][0] == true){
           return json_encode(array('msg'=>'Something went wrong','status'=>false));
        }else{
          return json_encode(array('msg'=>'Ocurrió un error','status'=>false));
        }
       
      }
    }

     function location_search($lat, $lang, $location_name,$user_id) {
        try{ 
          $location=JoinLocation::Where(['latitude'=>$lat,'longitude'=>$lang,'location_name'=>$location_name,'location_type'=>0,'post_id'=>0])->first();
          $dis=JoinLocation::Where(['latitude'=>$lat,'longitude'=>$lang,'location_name'=>$location_name,'location_type'=>1])->first();

          if(!empty($location)){ 
             return $data1=['location_id'=>$location->id,'location_name'=>$location->location_name,'post_id'=>$location->post_id,'location_type'=>$location->location_type];
          }else{
              if(empty($dis)){
                 $data=JoinLocation::create(['latitude'=>$lat,'longitude'=>$lang,'location_name'=>$location_name,'user_id'=>$user_id]);
              return $data1=['location_id'=>$data->id,'location_name'=>$data->location_name,'post_id'=>$location-> post_id,'location_type'=>$location->location_type];
              }
              // return json_encode(array('msg'=>'Data found','location_id'=>$data->id,'location_name'=>$data->location_name,'status'=>true));
          }

        }catch(\Exception $e){
           return json_encode(array('msg'=>$e->getMessage(),'status'=>false));
        }
    }
   
}
