<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Model\passwordReset;
use App\Notifications\Contactus;
use App\Model\Post;
use App\Model\Feedback;
use App\Model\Cms;
use App\Model\Admin;
use App\Model\JoinLocation;
use Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function forgotPassword($token,$email){
            $data2= passwordReset::where(['email'=>$email,'status'=>0,'token'=>$token])->get()->toArray();
            if(!empty($data2)){
                
                return view('forgotPassword',['email' => $email]);
                }else{
                return view('forgot_linkExpire');
            }
        }
        
        public function passwordupdate(Request $request){
            $validator = Validator::make($request->all(),[

            //'email' => 'required',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|same:password',   
            ]);
            if($validator->fails()){
                return redirect()->back()
                ->withErrors($validator)
                ->withInput();
                
            }
            $pass = sha1($request->password);
            passwordReset::where('email',$request->email)->update(['status'=>1]);
            $data=User::where('email',$request->email)->update(['password'=>$pass]);
            if($data){
                return redirect()->route('passwordRest_thanks');
                }else{
                //echo "hii"; die;
                return redirect()->back()->with('message', 'Something wrong.');
                
            }
            
        }
        
        public function passwordRest_thanks(){
            return view('passwordRest_thanks');

        }

         public function share_page($id){
            $data = Post::where('id',$id)->get()->toArray();
           
           return view('sharePage',['data' => $data]);
        }

         public function share_Location($id){
            $data = JoinLocation::where('id',$id)->get()->toArray();
           
           return view('share_Location',['data' => $data]);
        }

         public function user_detail($id){
              $ids=  explode("Suid",$id);
			//print_r($ids); die;
            $receiverdata = User::where('id',$ids[0])->get()->toArray();

			$senderdata = User::where('id',$ids[1])->get()->toArray();
             //print_r($senderdata); die;
			$feedback = Feedback::where('receiver_id',$ids[0])->where('sender_id',$ids[1])->get()->toArray();
			// print_R($feedback); die;
           
           return view('userFeedback/userFeedback',['receiverdata' => $receiverdata,'senderdata'=>$senderdata,'feedback'=>$feedback]);
        }

        public function userFeedback(Request $request){
            //print_r($request->receiver_id); die;
            $validator = Validator::make($request->all(),[
                'sender_id' => 'required',
                'receiver_id' => 'required',
                'comment' => 'required',
                'receiver_name' => 'nullable', 
                'receiver_email' => 'required',  
                'receiver_address' => 'required', 
                'sender_name' => 'nullable', 
                'sender_email' => 'required',  
                'sender_address' => 'required', 
                'starOne' => 'nullable',
                'starTwo' => 'nullable',
                'starThree' => 'nullable',
                'starFour' => 'nullable',
                'starFive' => 'nullable',
            ]);
            if($validator->fails()){
				

				//return redirect()->back()->with('message', $validator);
               
                
            }
             $Feedback= Feedback::where(['receiver_id'=>$request->receiver_id,'sender_id'=>$request->sender_id])->get()->toArray();
          // print_R($Feedback); die;
		  if(!empty($request->starFive)){ 
					$star=$request->starFive;
					}elseif(!empty($request->starFour)){
						$star=$request->starFour;
					}elseif(!empty($request->starThree)){
						$star=$request->starThree;
					}elseif(!empty($request->starTwo)){
						$star=$request->starTwo;
					}elseif(!empty($request->starOne)){
						$star=$request->starOne;
					}else{
						return redirect()->back()->with('message1', 'All fields are required.');
					}
				
            if(!empty($Feedback)){
                $data2= Feedback::where('id', $Feedback[0]['id'])->update(['rating'=>$star,'sender_name'=>$request->sender_name,'sender_email'=>$request->sender_email,'sender_address'=>$request->sender_address,'comment'=>$request->comment,'receiver_id'=>$request->receiver_id,'sender_id'=>$request->sender_id,'receiver_name'=>$request->receiver_name,'receiver_email'=>$request->receiver_email,'receiver_address'=>$request->receiver_address]);
                 
               return redirect()->back()->with('message', 'Feedback has changed successfully.');
                }else{
					
                    $data2= Feedback::create(['rating'=>$star,'sender_name'=>$request->sender_name,'sender_email'=>$request->sender_email,'sender_email'=>$request->sender_address,'comment'=>$request->comment,'receiver_id'=>$request->receiver_id,'sender_id'=>$request->sender_id,'receiver_name'=>$request->receiver_name,'receiver_email'=>$request->receiver_email,'receiver_address'=>$request->receiver_address]);
              return redirect()->back()->with('message', 'Feedback has submitted successfully.');
            }
        }

        public function privacy(Request $request){
             
            $cms = Cms::where('page_uses','Privcay-Policy')->get()->toArray();
           return view('cms/Privcay_Policy',['cms'=>$cms]);
        }

        public function about_us(Request $request){
             
            $cms = Cms::where('page_uses','About The QUI')->get()->toArray();
            //print_r($cms); die;
           return view('cms/about_us',['cms'=>$cms]);
        }

        public function contactUs(Request $request){
           
           return view('contactUs');
        }

        public function contactPost(Request $request){
    
      if($request->isMethod('post')){
      
        $validator = Validator::make($request->all(), [
        'name' => 'required|max:50',
        'email' => 'required',
        'subject' => 'required',
        'message' => 'required|max:250',
        // 'g-recaptcha-response' => 'required',
        ]);
        
        $msg = [
        'message' => 'Something wents wrong!!!',
        'alert-type' => 'error'
        ];
        if($validator->fails()){
          return redirect()->intended(route('contact'))
          ->withErrors($validator)
          ->withInput()
          ->with($msg);
          
        }
        $admin = Admin::all()->toArray();
        $email = $admin[0]['email'];
        //$email = "himani@braintechnosys.com";
        $name = $request->name;
        $user_email=$request->email;
        $user_subject= $request->subject;
        $msg=$request->message;
        $data = array('name'=>$name,'user_email'=>$user_email,'user_subject'=>$user_subject,'msg'=>$msg);
          Mail::send('emails.Contactus',$data, function ($message) use($name,$user_email,$user_subject,$msg,$email) {
     // $message->from('littlemonga294@gmail.com');
      $message->to($email);
      $message->subject('QUI');
    });
        //$email->notify(new Contactus($name,$user_email,$user_subject,$message)); 
        return redirect()->intended(route('contact'))->with('messagecon', 'Mail has been sent successfully!!');
      }
    }



}
