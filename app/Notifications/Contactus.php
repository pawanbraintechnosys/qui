<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Str;
use App\Model\Admin;


class Contactus extends Notification
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$user_subject,$message)
    {
        $this->name = $name;
        $this->email = $email;
        $this->user_subject = $user_subject;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
     public function via($notifiable)
    {
        return ['mail'];
    }

   public function toMail($notifiable)
    {
            return (new MailMessage)
                       //->from('himani@braintechnosys.com')
                    //->line('You are receiving this email because we received a password reset request for your account.')

                     ->greeting('Hi '.$notifiable->first_name.',')
                    ->line("We received a request to reset your password for your QUI account. We're here to help!")
                    //->line($notifiable->email.'. We are here to help!')
                    ->line('Simply click on the button to set a new password:')
                   ->action('Set A New Password',url('/forgotPassword/'.$token.'/'.$notifiable->email))
                      ->line("If you did'nt ask to change your password, don't worry! Your password is still safe and you can delete this email.")
                       ->line("Cheers,");

    }

    public function build()
    {
        return $this->subject('Contact Us')
                    ->markdown('emails.Contactus')
                    ->with(
                            array(
                                'name' => $this->name,
								'email' => $this->email,
								'user_subject' => $this->user_subject,
								'message' => $this->message
                            )
                        );
    }
}
