<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ChannelSeviceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->bind('Channel', function () {
        // Pass the application name
        return new \App\Channel\Channel();
    });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
