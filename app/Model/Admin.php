<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPassword as ResetPasswordNotification;
//use Kyslik\ColumnSortable\Sortable;
    class Admin extends Authenticatable
    {
        use Notifiable;
        protected $table = 'admins';
        protected $guard = 'admin';
		
	//use Sortable;
	//public $sortable = ['name','email'];

        protected $fillable = [
            'name', 'email', 'password','task','status'
        ];

        protected $hidden = [
            'password', 'remember_token',
        ];

        public function sendPasswordResetNotification($token) {
                $this->notify(new ResetPasswordNotification($token));
        }
    }