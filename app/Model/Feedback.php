<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Feedback extends Model{
 use Sortable;
	 
	public $sortable = ['sender_name','sender_email','rating','created_at']; 
	

    protected $fillable = [

       'sender_id','receiver_id','comment','rating'
    ];

    protected $appends = ['sender_name','sender_email','sender_address','receiver_name','receiver_address','receiver_email'];

    public function getreceiverNameAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','receiver_id')->first('name');
            if(!empty($data)){ 
		   return $data['name'];
            	
		   }else{
		   	return null;
		   }
		   
		}

		public function getreceiverEmailAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','receiver_id')->first('email');
            if(!empty($data)){ 
		   return $data['email'];
            	
		   }else{
		   	return null;
		   }
		   
		}

		public function getreceiverAddressAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','receiver_id')->first('location');
            if(!empty($data)){ 
		   return $data['location'];
            	
		   }else{
		   	return null;
		   }
		   
		}
		//sender

		 public function getsenderNameAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','sender_id')->first('name');
            if(!empty($data)){ 
		   return $data['name'];
            	
		   }else{
		   	return null;
		   }
		   
		}

		 public function getsenderEmailAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','sender_id')->first('email');
            if(!empty($data)){ 
		   return $data['email'];
            	
		   }else{
		   	return null;
		   }
		   
		}

		 public function getsenderAddressAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','sender_id')->first('location');
            if(!empty($data)){ 
		   return $data['location'];
            	
		   }else{
		   	return null;
		   }
		   
		}
}
