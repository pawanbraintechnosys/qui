<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;


class Chat extends Model
{
	  use Sortable;
	
	public $sortable = ['post_name'];
    protected $fillable = [

       'name','image','usertype','client_id','uuid','user_name','user_image','channel','message','status','created_at','post_id','start_timetoken','end_timetoken','type','file_type','image','deleteChannel','muteUnmute','chat_type','user_username','join_status'
    ];



    public function Postdata(){
		return $this->belongsTo('App\Model\Post','post_id')->where('status',0);
	}



	
	
	 public function Clientdata(){
		return $this->belongsTo('App\Model\User','client_id');
	}
	

	
	public function user(){
		return $this->belongsTo('App\Model\ChatJoin','channel')->where('status',0)->where('chats.type','public')->distinct();
	}
	
	public function Userdata(){
		return $this->belongsTo('App\Model\User','uuid');
	}
	
	protected $appends = ['user_image','image','name','user_username','url','post_type','location','chatuser_id','chatmsg','chatdataTime',
	'user_name','location_id','post_name'];
	
	  /*public function getCreatedAtAttribute()
		{
			if(!empty($this->post_id)){ 
		     return Post::where('location_id',$this->post_id)->pluck('created_at')->first();
		  }
		  
		   
		} */

		 public function getpostNameAttribute()
		{
			$data = $this->belongsTo('App\Model\Post','post_id')->where('status',0)->first('tittle');
            if(!empty($data)){ 
		   return $data['tittle'];
            	
		   }else{
		   	return null;
		   }
			//return $this->belongsTo('App\Model\Post','post_id')->where('status',0)->pluck('tittle');
		 
		   
		}

		 public function getLocationIdAttribute()
		{
			return $this->belongsTo('App\Model\Post','post_id')->where('status',0)->pluck('location_id');
		 
		   
		}

	 public function getchatdataTimeAttribute()
		{
		   return $this->created_at ;
		   
		}
	 
	  public function getchatuserIdAttribute()
		{
		   return $this->client_id ;
		   
		}

		public function getuserNameAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','uuid')->first('name');
            if(!empty( $data)){ 
		   return $data['name'];
		   }else{
		   	return null;
		   }
		   
		}

		public function getuserImageAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','uuid')->first('image');
            if(!empty($data) && $data['image']!=null){ 
		   //return asset('upload/image/' .$data['image']);
            	$url = parse_url($data['image'], PHP_URL_SCHEME);
            	if(!empty($url)){
            		return $data['image'];
            	}else{
            		return asset('upload/image/' .$data['image']);
            	}
            	
		   }else{
		   	return null;
		   }
		   
		}

		public function getuserUsernameAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','uuid')->first('username');
            if(!empty( $data)){ 
		   return $data['username'];
		   }else{
		   	return null;
		   }
		   
		}

		public function getNameAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','client_id')->first('name');
            if(!empty( $data)){ 
		   return $data['name'];
		   }else{
		   	return null;
		   }
		   
		}
		
		/*public function getMessageAttribute()
		{
		   if($this->file_type == 'post'){
		       $data = $this->belongsTo('App\Model\Post','message');
		     // $post =  App\Model\Post::find($this->message);
		      return $post;
		   }
		   return $this->file_type;
		   
		   
		}*/

		
		
		public function getchatmsgAttribute()
		{
		   return $this->message;
		   
		}
		
	

				public function geturlAttribute()
		{
		  
		        return asset('/share_page/' .$this->post_id);
		   
		}
		
			public function getPostTypeAttribute()
		{
		  if($this->type == 'public') {
		       return 0 ;
		  } else {
		      return 1;
		  }
		   
		}
		
			public function getlocationAttribute()
		{
		   return false ;
		   
		}
		
		
		 public function Image()
		{
		    if ($this->image) {
		        return asset('upload/image/' . $this->image);
		    } else {
		        return null;
		    }
		} 

		/*public function getimageAttribute()
		{
		   $data = $this->belongsTo('App\Model\User','client_id')->first('image');
		   
            if(!empty($data) && $data['image']!=null){ 
		   //return asset('upload/image/' .$data['image']);
            	$url = parse_url($data['image'], PHP_URL_SCHEME);
            	if(!empty($url)){
            		return $data['image'];
            	}else{
            		return asset('upload/image/' .$data['image']);
            	}
            	
		   }else{
		   	return null;
		   }
		   
		} */

		 public function getImageAttribute($value)
		{
		    if ($value) {
		        return asset('upload/image/' . $value);
		    } else {
		        return null;
		    }
		} 
		

		
    		/* public function unread($uid) {
    		     
    		     
             $data = $this->hasMany('App\Model\ChatJoin','channel')->where('chat_joins.status',0)->where('chat_joins.join_id',$id)->pluck('join_id');
                if(!empty($data)){
              $unreadmsg= $this->hasMany('App\Model\Chat','channel')->where('deleteChannel',0)->pluck('id');
             
              }
              print_r($unreadmsg); die; 
        } */
    
        // results in a "problem", se examples below
       /* public function available_videos() {
            $this->ChatJoin()
            return $this->belongsTo('App\Model\Chat','channel');
            //return $this->videos()->where('available','=', 1);
        }*/
}


