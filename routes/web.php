<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

       Route::post('/userFeedback', 'HomeController@userFeedback')->name('userFeedback');

		Route::get('/forgotPassword/{token}/{email}', 'HomeController@forgotPassword')->name('forgotPassword');
		Route::post('/passwordupdate', 'HomeController@passwordupdate')->name('passwordupdate');

		Route::get('/passwordRest_thanks', 'HomeController@passwordRest_thanks')->name('passwordRest_thanks');

		//Auth::routes();

		Route::get('/home', 'HomeController@index')->name('home');
		//Route::prefix('admin')->group(function () {


		Route::group(['middleware' => ['web','auth:admin']], function() {
        //cms
		Route::get('cms', 'Admin\CmsController@index')->name('admin.cms');
		Route::get('cms/add', 'Admin\CmsController@add')->name('admin.addCms');
		Route::post('cms/add', 'Admin\CmsController@store')->name('admin.storeCms');
		Route::get('cms/edit/{id}', 'Admin\CmsController@edit')->name('admin.editCms');
		Route::put('cms/update/{id}', 'Admin\CmsController@update')->name('admin.updateCms');
		//tracking
		Route::get('track/{id}', 'Admin\LiveTrackingController@index')->name('admin.track');
        //report
        Route::post('reportChange_status', 'Admin\ReportController@reportChange_status')->name('admin.reportChange_status');
        Route::post('reportDelete', 'Admin\ReportController@reportDelete')->name('admin.reportDelete');
        Route::get('report', 'Admin\ReportController@report')->name('admin.report');
        Route::get('spam', 'Admin\ReportController@spam')->name('admin.spam');
        Route::get('Postview/{id}', 'Admin\AdminController@Postview')->name('admin.Postview');
		//chat
		Route::get('support', 'Admin\ChatController@support')->name('admin.support');
		Route::post('supportPost', 'Admin\ChatController@supportPost')->name('admin.supportPost');
		Route::get('chat', 'Admin\ChatController@index')->name('admin.chat');
		Route::get('support/chat/{id}', 'Admin\ChatController@support_chat')->name('admin.support_chat');
		
		Route::get('chat/list/{id}', 'Admin\ChatController@list')->name('admin.chatList');
		Route::post('chat/delete', 'Admin\ChatController@delete')->name('admin.chat_delete');

		
		Route::delete('cms/destroy/{id}', 'Admin\CmsController@destroy')->name('admin.deleteCms');
		Route::post('ckeditor/upload', 'Admin\CkeditorController@upload')->name('admin.ckeditor.upload');
		Route::post('fileSave', 'Admin\ChatController@fileSave')->name('admin.fileSave');


		Route::get('feedback', 'Admin\AdminController@feedback')->name('admin.feedback');
		Route::get('admin/feedback_edit/{id}', 'Admin\AdminController@feedback_edit')->name('admin.feedback_edit');
		Route::put('feedbackUpdate/{id}', 'Admin\AdminController@feedbackUpdate')->name('admin.feedbackUpdate');
		Route::post('feedbackDelete', 'Admin\AdminController@feedbackDelete')->name('admin.feedbackDelete');
		Route::get('feedback/view/{id}', 'Admin\AdminController@feedbackView')->name('admin.feedback.view');
		Route::post('notificationStatus', 'Admin\AdminController@notificationStatus')->name('admin.notificationStatus');


		Route::get('dashboard', 'Admin\AdminController@index')->name('admin.dashboard');
		//categories
		Route::get('category', 'Admin\AdminController@category')->name('admin.category');

		Route::get('category/add', 'Admin\AdminController@categoryAdd')->name('admin.categoryAdd');
		Route::post('Post_categoryAdd', 'Admin\AdminController@Post_categoryAdd')->name('admin.Post_categoryAdd');
		Route::get('category/edit/{id}', 'Admin\CategoryController@edit')->name('admin.edit.category');
		Route::put('category/update/{id}', 'Admin\CategoryController@update')->name('admin.category.update');
		Route::post('categoryDelete', 'Admin\CategoryController@categoryDelete')->name('admin.categoryDelete');

		Route::get('news', 'Admin\AdminController@news')->name('admin.news');
		Route::get('help', 'Admin\AdminController@help')->name('admin.help');
		Route::get('warning', 'Admin\AdminController@warning')->name('admin.warning');
		Route::get('offered', 'Admin\AdminController@offered')->name('admin.offered');
		Route::get('needed', 'Admin\AdminController@needed')->name('admin.needed');
		Route::get('newest/{id}', 'Admin\AdminController@newest')->name('admin.newest');
		Route::get('popular/{id}', 'Admin\AdminController@popular')->name('admin.popular');
		Route::get('recommend/{id}', 'Admin\AdminController@recommend')->name('admin.recommend');
		Route::get('recommend_more/{id}/{uid}', 'Admin\AdminController@recommend_more')->name('admin.recommend_more');



		Route::get('news/editPost/{id}', 'Admin\AdminController@editPost')->name('admin.editPost_news');
		Route::get('warning/editPost/{id}', 'Admin\AdminController@editPost')->name('admin.editPost_warning');
		Route::get('help/editPost/{id}', 'Admin\AdminController@editPost')->name('admin.editPost_help');
		Route::get('offered/editPost/{id}', 'Admin\AdminController@editPost')->name('admin.editPost_offered');
		Route::get('needed/editPost/{id}', 'Admin\AdminController@editPost')->name('admin.editPost_needed');

		Route::get('news/Postview/{id}', 'Admin\AdminController@Postview')->name('admin.Postview_news');
		Route::get('warning/Postview/{id}', 'Admin\AdminController@Postview')->name('admin.Postview_warning');
		Route::get('help/Postview/{id}', 'Admin\AdminController@Postview')->name('admin.Postview_help');
		Route::get('offered/Postview/{id}', 'Admin\AdminController@Postview')->name('admin.Postview_offered');
		Route::get('needed/Postview/{id}', 'Admin\AdminController@Postview')->name('admin.Postview_needed');
		Route::put('postUpdate/{id}', 'Admin\AdminController@postUpdate')->name('admin.postUpdate');
		Route::get('news/post_userDetails/{id}', 'Admin\AdminController@post_userDetails')->name('admin.post_userDetails_news');
		Route::get('offered/post_userDetails/{id}', 'Admin\AdminController@post_userDetails')->name('admin.post_userDetails_offered');
		Route::get('needed/post_userDetails/{id}', 'Admin\AdminController@post_userDetails')->name('admin.post_userDetails_needed');
		Route::post('postChange_status', 'Admin\AdminController@postChange_status')->name('admin.postChange_status');
		Route::post('recommendChange', 'Admin\AdminController@recommendChange')->name('admin.recommendChange');


		Route::post('postDelete', 'Admin\AdminController@postDelete')->name('admin.postDelete');

		Route::get('changepassword', 'Admin\Auth\ResetPasswordController@index')->name('admin.auth.changepassword');
		Route::post('changepasswordrequest', 'Admin\Auth\ResetPasswordController@changePassword')->name('admin.auth.changepasswordrequest');
		Route::post('logout', 'Admin\Auth\LoginController@logout')->name('admin.auth.logout');
		//add push notification
		Route::get('pushnotification', 'Admin\PushNotificationController@index')->name('admin.pushnotification');

		Route::get('pushnotification/add', 'Admin\PushNotificationController@Add')->name('admin.pushnotificationAdd');
		Route::post('Post_pushnotificationAdd', 'Admin\PushNotificationController@store')->name('admin.Post_pushnotificationAdd');
		//Route::get('category/edit/{id}', 'Admin\CategoryController@edit')->name('admin.edit.category');
		//Route::put('category/update/{id}', 'Admin\CategoryController@update')->name('admin.category.update');
		Route::post('pushnotificationDelete', 'Admin\PushNotificationController@Delete')->name('admin.pushnotificationDelete');
		Route::get('GeofencingConfigurations/{id}', 'Admin\GeofencingConfigurationsController@index')->name('admin.GeofencingConfigurations');
		Route::post('GeofencingConfigurations_Post/{id}', 'Admin\GeofencingConfigurationsController@GeofencingConfigurations_Post')->name('admin.GeofencingConfigurations_Post');

  });

	// User Module
	Route::get('user', 'Admin\UsersController@user')->name('admin.user');
	Route::get('active/user', 'Admin\UsersController@active')->name('admin.active');
	Route::get('inactive/user', 'Admin\UsersController@inactive')->name('admin.inactive');
	Route::get('user/create', 'Admin\UsersController@create')->name('admin.user.create');
	Route::post('user/store', 'Admin\UsersController@store')->name('admin.user.store');
	Route::get('user/edit/{id}', 'Admin\UsersController@edit')->name('admin.user.edit');
	Route::put('user/update/{id}', 'Admin\UsersController@update')->name('admin.user.update');
	Route::post('userDestroy', 'Admin\UsersController@userDestroy')->name('admin.userDestroy');
	Route::post('userDisable', 'Admin\UsersController@userDisable')->name('admin.userDisable');
	Route::post('user_status', 'Admin\UsersController@user_status')->name('admin.user_status');
	Route::get('disabled', 'Admin\UsersController@disabled')->name('admin.disabled');
    //subadmin
	Route::get('/subadmin_list', 'Admin\SubadminController@subadmin_list')->name('admin.subadmin_list');
	
	
	Route::get('/edit_subadmin/{id}', 'Admin\SubadminController@edit_subadmin')->name('admin.edit_subadmin');
	Route::post('/subadmin_updatemodule', 'Admin\SubadminController@subadmin_updatemodule')->name('admin.subadmin_updatemodule');
	
	Route::delete('delete_subadmin', 'Admin\SubadminController@delete_subadmin')->name('admin.delete_subadmin');
	
	Route::get('changePassword_subadmin/{id}', 'Admin\SubadminController@updatePassword_subadmin')->name('admin.updatePassword_subadmin');
	Route::post('update_password', 'Admin\SubadminController@update_password')->name('admin.update_password_subadmin');
		
	//Route::get('/admin', 'Admin\AdminController@login');
	Route::get('admin/register', 'Admin\AdminController@create')->name('admin.register');
	Route::post('admin_register', 'Admin\AdminController@store')->name('admin.register.store');
	Route::get('/', 'Admin\Auth\LoginController@login')->name('admin.auth.login');
	Route::get('admin', 'Admin\Auth\LoginController@login')->name('admin.auth.login');
	Route::post('admin_login', 'Admin\Auth\LoginController@loginAdmin')->name('admin.auth.loginAdmin');
	/*forgot password token*/
	Route::get('admin/forgotpassword', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.auth.forgotpassword');
	Route::post('forgotpasswordrequest','Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.auth.forgotpasswordrequest');
	Route::get('password/reset/{token}','Admin\Auth\ResetPasswordController@showResetForm')->name('admin.auth.password.reset.token');
	Route::post('password/reset', 'Admin\Auth\ResetPasswordController@reset')->name('admin.password.update');
	Route::get('share_page/{id}', 'HomeController@share_page');
	Route::get('share_Location/{id}', 'HomeController@share_Location');
	Route::get('user_detail/{id}', 'HomeController@user_detail');
	Route::get('privacy-policy', 'HomeController@privacy');
	Route::get('about_us', 'HomeController@about_us');
	Route::get('contactUs', 'HomeController@contactUs')->name('contact');
	Route::post('contactPost', 'HomeController@contactPost')->name('admin.contactPost');

//});
